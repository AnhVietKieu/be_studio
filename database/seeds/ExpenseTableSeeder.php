<?php

use Illuminate\Database\Seeder;

class ExpenseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('expense')->truncate();
        DB::table('expense')->insert([
            'expense_code'=> 'PC10/19_0001',
            'expense_type'=> 1,
            'reason'=> 'Khách hàng chê váy xấu',
            'date_excute'=> '2019-08-08',
            'date_accounting'=> '2019-08-09',
            'money'=>500,
            'person_charge'=> 1,
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
