<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('roles')->truncate();
        DB::table('roles')->insert([
            //=================USER============================
            [
                'group' => 'TÀI KHOẢN',
                'name' => 'LIST_USER',
                'permissions' => 'Danh sách',
            ],
            [
                'group' => 'TÀI KHOẢN',
                'name' => 'CREATE_USER',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'TÀI KHOẢN',
                'name' => 'EDIT_USER',
                'permissions' => 'Chỉnh sửa',
            ],
            [
                'group' => 'TÀI KHOẢN',
                'name' => 'LOCK_USER',
                'permissions' => 'Khóa và mở',
            ],
            //=================CUSTOMER============================
            [
                'group' => 'KHÁCH HÀNG',
                'name' => 'LIST_CUSTOMER',
                'permissions' => 'Danh sách',
            ],
            [
                'group' => 'KHÁCH HÀNG',
                'name' => 'CREATE_CUSTOMER',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'KHÁCH HÀNG',
                'name' => 'EDIT_CUSTOMER',
                'permissions' => 'Chỉnh sửa',
            ],
            [
                'group' => 'KHÁCH HÀNG',
                'name' => 'SEND_EMAIL_CUSTOMER',
                'permissions' => 'Gửi email',
            ],
            //=================SUPPLIER============================
            [
                'group' => 'NHÀ CUNG CẤP',
                'name' => 'LIST_SUPPLIER',
                'permissions' => 'Danh sách',
            ],
            [
                'group' => 'NHÀ CUNG CẤP',
                'name' => 'CREATE_SUPPLIER',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'NHÀ CUNG CẤP',
                'name' => 'EDIT_SUPPLIER',
                'permissions' => 'Chỉnh sửa',
            ],
            [
                'group' => 'NHÀ CUNG CẤP',
                'name' => 'SEND_EMAIL_SUPPLIER',
                'permissions' => 'Gửi email',
            ],
            //=================CATEGORY============================
            [
                'group' => 'LOẠI DỊCH VỤ',
                'name' => 'LIST_CATEGORY',
                'permissions' => 'Danh sách',
            ],
            [
                'group' => 'LOẠI DỊCH VỤ',
                'name' => 'CREATE_CATEGORY',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'LOẠI DỊCH VỤ',
                'name' => 'EDIT_CATEGORY',
                'permissions' => 'Chỉnh sửa',
            ],
            //=================COMBO============================
            [
                'group' => 'COMBO',
                'name' => 'LIST_COMBO',
                'permissions' => 'Danh sách',
            ],
            [
                'group' => 'COMBO',
                'name' => 'CREATE_COMBO',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'COMBO',
                'name' => 'EDIT_COMBO',
                'permissions' => 'Chỉnh sửa',
            ],
            //=================SERVICE============================
            [
                'group' => 'DỊCH VỤ',
                'name' => 'LIST_SERVICE',
                'permissions' => 'Danh sách',
            ],
            [
                'group' => 'DỊCH VỤ',
                'name' => 'CREATE_SERVICE',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'DỊCH VỤ',
                'name' => 'EDIT_SERVICE',
                'permissions' => 'Chỉnh sửa',
            ],
            //=================INCURRED============================
            [
                'group' => 'PHÍ PHÁT SINH',
                'name' => 'LIST_INCURRED',
                'permissions' => 'Danh sách',
            ],
            [
                'group' => 'PHÍ PHÁT SINH',
                'name' => 'CREATE_INCURRED',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'PHÍ PHÁT SINH',
                'name' => 'EDIT_INCURRED',
                'permissions' => 'Chỉnh sửa',
            ],
            //=====================RECEIPTS========================
            [
                'group' => 'PHIẾU THU',
                'name'  => 'CREATE_BILL_RECEIPT',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'PHIẾU THU',
                'name'  => 'APPROVE_BILL_RECEIPT',
                'permissions' => 'Duyệt phiếu thu',
            ],
            [
                'group' => 'PHIẾU THU',
                'name'  => 'LIST_PAYMENT_SLIP',
                'permissions' => 'Danh sách phiếu thu',
            ],
            //=====================VOUCHERS========================
            [
                'group' => 'PHIẾU CHI',
                'name'  => 'CREATE_BILL_VOUCHER',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'PHIẾU CHI',
                'name'  => 'APPROVE_BILL_VOUCHER',
                'permissions' => 'Duyệt phiếu chi',
            ],
            [
                'group' => 'PHIẾU CHI',
                'name'  => 'LIST_BILL',
                'permissions' => 'Danh sách phiếu chi',
            ],
            //=====================DELETE==========================
            [
                'group' => 'DELETE',
                'name' => 'DELETE_USER',
                'permissions' => 'Xóa tài khoản',
            ],
            [
                'group' => 'DELETE',
                'name' => 'DELETE_CUSTOMER',
                'permissions' => 'Xóa khách hàng',
            ],
            [
                'group' => 'DELETE',
                'name' => 'DELETE_SUPPLIER',
                'permissions' => 'Xóa nhà cung cấp',
            ],
            [
                'group' => 'DELETE',
                'name' => 'DELETE_CATEGORY',
                'permissions' => 'Xóa loại dịch vụ',
            ],
            [
                'group' => 'DELETE',
                'name' => 'DELETE_COMBO',
                'permissions' => 'Xóa combo',
            ],
            [
                'group' => 'DELETE',
                'name' => 'DELETE_SERVICE',
                'permissions' => 'Xóa dịch vụ',
            ],
            [
                'group' => 'DELETE',
                'name' => 'DELETE_INCURRED',
                'permissions' => 'Xóa phí phát sinh',
            ],
            [
                'group' => 'DELETE',
                'name'  => 'DELETE_ORDER',
                'permissions' => 'Xóa đơn hàng',
            ],
            [
                'group' => 'DELETE',
                'name'  => 'DELETE_SALARY',
                'permissions' => 'Xóa bảng lương',
            ],
            //===================ORDER===============================
            [
                'group' => 'ĐƠN HÀNG',
                'name'  => 'LIST_ORDER',
                'permissions' => 'Danh sách',
            ],
            [
                'group' => 'ĐƠN HÀNG',
                'name'  => 'CREATE_ORDER',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'ĐƠN HÀNG',
                'name'  => 'EDIT_ORDER',
                'permissions' => 'Chỉnh sửa',
            ],
            [
                'group' => 'ĐƠN HÀNG',
                'name'  => 'UPDATE_STATUS_ORDER',
                'permissions' => 'Cập nhật trạng thái',
            ],
            [
                'group' => 'ĐƠN HÀNG',
                'name'  => 'ASSIGN_JOB',
                'permissions' => 'Giao việc',
            ],
            [
                'group' => 'ĐƠN HÀNG',
                'name'  => 'APPROVE_JOB',
                'permissions' => 'Nhận việc hoặc từ chối',
            ],
            [
                'group' => 'ĐƠN HÀNG',
                'name'  => 'ROLLBACK_ORDER',
                'permissions' => 'Hoàn duyệt',
            ],
            //=====================SALARY==========================================
            [
                'group' => 'LƯƠNG',
                'name'  => 'CREATE_SALARY',
                'permissions' => 'Tạo mới',
            ],
            [
                'group' => 'LƯƠNG',
                'name'  => 'LIST_SALARY',
                'permissions' => 'Danh sách',
            ],
            [
                'group' => 'LƯƠNG',
                'name'  => 'EDIT_SALARY',
                'permissions' => 'Chỉnh sửa',
            ],
            //=====================CALENDAR========================
            [
                'group' => 'LỊCH CÔNG VIỆC',
                'name'  => 'VIEW_CALENDAR_PHOTO_MAKEUP',
                'permissions' => 'Xem lịch photo và makeup',
            ],
            [
                'group' => 'LỊCH CÔNG VIỆC',
                'name'  => 'VIEW_CALENDAR_PHOTOSHOP',
                'permissions' => 'Xem lịch photoshop',
            ],
            //=====================Report ========================
            [
                'group' => 'BÁO CÁO',
                'name'  => 'VIEW_REPORT',
                'permissions' => 'Xem báo cáo',
            ],
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
