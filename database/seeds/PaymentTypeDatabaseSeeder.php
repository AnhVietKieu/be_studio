<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentTypeDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('payment_type')->truncate();
        DB::table('payment_type')->insert([
        [
            'user_id'=>'1',
            'name'=>'Thanh toán làm 3 đợt',
            'parent_id'=>'0',
            'money'=>'0',
            'payment_type'=>'0',
            'payment_expiry'=>'3',
            'payment_expiry_type'=>'0',
            'payment_expiry_by'=>'1'
        ],
        [
            'user_id'=>'1',
            'name'=>'Thanh toán tiền cọc lần 1',
            'parent_id'=>'1',
            'money'=>'40',
            'payment_type'=>'0',
            'payment_expiry'=>'3',
            'payment_expiry_type'=>'0',
            'payment_expiry_by'=>'1'
        ],
        [
            'user_id'=>'1',
            'name'=>'Thanh toán tiền cọc lần 2',
            'parent_id'=>'1',
            'money'=>'40',
            'payment_type'=>'0',
            'payment_expiry'=>'3',
            'payment_expiry_type'=>'0',
            'payment_expiry_by'=>'1'
        ],
        [
            'user_id'=>'1',
            'name'=>'Thanh toán tiền cọc lần 3',
            'parent_id'=>'1',
            'money'=>'20',
            'payment_type'=>'0',
            'payment_expiry'=>'3',
            'payment_expiry_type'=>'0',
            'payment_expiry_by'=>'1'
        ],

        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
