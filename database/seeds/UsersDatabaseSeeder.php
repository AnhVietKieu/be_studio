<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'name'=>'Dương Mạnh Hoan',
                'email'=>'hoandm@gmail.com',
                'password'=>bcrypt('admin123'),
                'department_id'=>'1',
                'phone_number' => '',
            ],
            [
                'name'=>'Dương Văn Lò',
                'email'=>'thuy@gmail.com',
                'password'=>bcrypt('admin123'),
                'department_id'=>'1',
                'phone_number' => '',
            ],
            [
                'name'=>'Nguyễn Thu Huyền',
                'email'=>'huyennguyen@gmail.com',
                'password'=>bcrypt('admin123'),
                'department_id'=>'2',
                'phone_number' => '',
            ],
            [
                'name'=>'Nguyễn Thúy Phương',
                'email'=>'thuyphuong@gmail.com',
                'password'=>bcrypt('admin123'),
                'department_id'=>'2',
                'phone_number' => '',
            ],
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
