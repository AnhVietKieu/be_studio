<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('department')->truncate();
        DB::table('department')->insert([
            [
                'name' => 'ADMIN',
                'description' => 'quản trị cao nhất',
            ],

            [
                'name' => 'SALE',
                'description' => 'nhân viên bán hàng',
            ],

            [
                'name' => 'PHOTOGRAPHER',
                'description' => 'nhiếp ảnh',
            ],

            [
                'name' => 'MAKEUP',
                'description' => 'trang điểm',
            ],

            [
                'name' => 'PHOTOSHOP',
                'description' => 'chỉnh sửa ảnh',
            ],

            [
                'name' => 'IN',
                'description' => 'In ảnh'
            ]
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
