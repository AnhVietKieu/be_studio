<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxSaleAndTimeShiftToCombo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('combo', function (Blueprint $table) {
            $table->string('time_shift','30')->nullable()->comment('thời gian 1 ca');
            $table->decimal('max_sale', 14, 2)->nullable()->comment('giá max giảm giá');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('combo', function (Blueprint $table) {
            //
        });
    }
}
