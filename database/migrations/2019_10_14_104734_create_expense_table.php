<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('expense_code', 50)->comment('Mã phiếu chi');
            $table->bigInteger('expense_type')->comment('1: tiền mặt,2: chuyển khoản');
            $table->string('reason', 250)->comment('Lý do chi');
            $table->datetime('date_excute')->comment('Ngày chi');
            $table->datetime('date_accounting')->comment('Ngày hạch toán');
            $table->decimal('money', 12, 2)->comment('Số tiền');
            $table->bigInteger('person_charge')->comment('Người nhận');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense');
    }
}
