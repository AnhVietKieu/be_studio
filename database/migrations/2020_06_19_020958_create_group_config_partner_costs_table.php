<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupConfigPartnerCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_config_partner_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('partner_id')->comment('ID của partner');
            $table->date('excu_date')->comment('Ngày áp dụng');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_config_partner_costs');
    }
}
