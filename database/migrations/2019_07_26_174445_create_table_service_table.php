<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('name', 255)->comment('Tên dịch vụ');
            $table->string('code', 11)->comment('Mã dịch vụ'); 
            $table->integer('category_id')->comment('Mã danh mục');
            $table->decimal('cost_price', 12, 2)->default(0)->comment('Giá nhập vào');
            $table->decimal('sale_price', 12, 2)->default(0)->comment('Giá bán ra');
            $table->decimal('minus_price', 12, 2)->default(0)->comment('Giá trừ đi khi ra khỏi combo');
            $table->string('description', 500)->comment('Mô tả về dịch vụ');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service');
    }
}
