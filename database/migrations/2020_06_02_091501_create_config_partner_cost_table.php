<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigPartnerCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_partner_cost', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('partner_id')->comment('id của partner');
            $table->integer('combo_id')->comment('id của gói dịch vụ combo');
            $table->float('money_of_combo','12','2')->comment('giá của combo');
            $table->float('half_day','12','2')->comment('giá ngày công của nửa ngày');
            $table->float('full_day','12','2')->comment('giá ngày công của một ngày');
            $table->float('morning','12','2')->comment('giá ngày công của buổi sáng');
            $table->float('evening','12','2')->comment('giá ngày công của buổi tối');
            $table->float('afterparty','12','2')->comment('giá phát sinh thêm giờ');
            $table->float('out_of_province_half_day','12','2')->comment('giá nhân công đi tỉnh nửa ngày');
            $table->float('out_of_province_full_day','12','2')->comment('giá nhân công đi tỉnh cả ngày');
            $table->date('excu_date')->nullable()->comment('Ngày áp dụng');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_partner_cost');
    }
}
