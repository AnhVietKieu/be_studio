<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderIncurredTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_incurred', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('incurred_id')->comment('id chi phí phát sinh');
            $table->integer('order_id')->comment('id order');
            $table->datetime('date')->comment('ngày chi phí');
            $table->integer('create_by')->comment('người tạo');
            $table->integer('payer')->comment('người chi');
            $table->decimal('money',12,2)->comment('số tiền');
            $table->string('note',100)->nullable()->comment('ghi chú');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_incurred');
    }
}
