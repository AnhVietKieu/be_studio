<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankCodeToPaymentHistoryDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_history_detail', function (Blueprint $table) {
            $table->string('code_bank', 100)->nullable()->comment('ngân hàng chuyển khoản');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_history_detail', function (Blueprint $table) {
            //
        });
    }
}
