<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_job', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_id')->comment('id đơn hàng');
            $table->integer('combo_id')->comment('id combo');
            $table->integer('service_id')->comment('id dịch vụ');
            $table->integer('user_id')->nullable()->comment('ngày được assign');
            $table->string('location', 500)->comment('địa điểm');
            $table->dateTime('start_date_plan')->comment('time dự kiến thực hiện');
            $table->dateTime('end_date_plan')->comment('time dự kiến kết thúc');
            $table->dateTime('start_date')->comment('ngày bắt đầu thực tế');
            $table->dateTime('end_date')->comment('ngày bắt đầu kết thúc');
            $table->tinyInteger('status')->default(0)
                ->comment('0: chờ xác nhận, 1 Đồng ý, 2 từ chối, 3 đang thực hiện, 4 kết thúc');
            $table->string('url_file', 500)->nullable()->comment('đường dẫn ảnh tin nhắn');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_job');
    }
}
