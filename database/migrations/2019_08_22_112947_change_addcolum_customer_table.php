<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAddcolumCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer', function (Blueprint $table) {
            $table->string('contact_name' ,500)->nullable()->comment('Tên người liên hệ');
            $table->string('contact_mobile_phone' ,15)->nullable()->comment('Số điện thoại người liên hệ');
            $table->string('contact_email' ,255)->nullable()->comment('Email người liên hệ');
            $table->string('contact_address' ,300)->nullable()->comment('Địa chỉ người liên hệ');
            $table->string('mobile_phone' ,15)->comment('Số điện thoại')->change();
            $table->string('email_address' ,255)->nullable()->comment('Địa chỉ Email')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
