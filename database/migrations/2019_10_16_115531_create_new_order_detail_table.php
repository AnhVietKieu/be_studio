<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_service_id')->comment('id đơn hàng');
            $table->dateTime('date')->comment('ngày bắt đầu');
            $table->string('name', 500)->comment('vd: ngày ăn hỏi');
            $table->string('shift', 1000)->nullable()->comment('ca chụp khách chọn');
            $table->string('time', 255)->nullable()->comment('tổng thời gian chụp khách chọn');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_detail');
    }
}
