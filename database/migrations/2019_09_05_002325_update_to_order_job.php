<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateToOrderJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_job', function (Blueprint $table) {
            $table->dropColumn('combo_id');
            $table->string('location', 500)->nullable()->comment('địa điểm')->change();
            $table->dateTime('start_date')->nullable()->comment('ngày bắt đầu thực tế')->change();
            $table->dateTime('end_date')->nullable()->comment('ngày bắt đầu kết thúc')->change();
            $table->integer('process')->default(0)->comment('% hoàn thành công việc');
            $table->integer('create_by')->comment('id người giao việc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_job', function (Blueprint $table) {
            //
        });
    }
}
