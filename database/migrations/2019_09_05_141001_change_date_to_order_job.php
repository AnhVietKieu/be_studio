<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDateToOrderJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_job', function (Blueprint $table) {
            $table->dateTime('start_date_plan')->nullable()->comment('time dự kiến thực hiện')->change();
            $table->dateTime('end_date_plan')->nullable()->comment('time dự kiến kết thúc')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_job', function (Blueprint $table) {
            //
        });
    }
}
