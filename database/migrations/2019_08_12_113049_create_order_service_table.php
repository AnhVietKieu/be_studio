<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_service', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_id')->comment('id order');
            $table->integer('combo_id')->comment('id combo');
            $table->integer('service_id')->comment('id dịch vụ');
            $table->string('type', 8)->nullable()->comment('ADD: thêm vào combo ; MINUS: bỏ ra ngoài combo');
            $table->decimal('price', 12, 2)->nullable()->comment('giá bán hoặc giá bỏ đi khỏi combo');
            $table->decimal('sale_off', 5, 2)->nullable()->comment('giảm giá % service');
            $table->integer('number')->default(1)->comment('số lượng service');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_service');
    }
}
