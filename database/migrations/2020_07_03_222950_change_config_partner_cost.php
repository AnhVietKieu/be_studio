<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeConfigPartnerCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_partner_cost', function($table) {
            $table->dropColumn('half_day');
            $table->dropColumn('full_day');
            $table->dropColumn('morning');
            $table->dropColumn('evening');
            $table->dropColumn('afterparty');
            $table->dropColumn('out_of_province_half_day');
            $table->dropColumn('out_of_province_full_day');
            $table->dropColumn('excu_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
