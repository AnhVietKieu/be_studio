<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->comment('id ngưởi gửi');
            $table->integer('customer_id')->comment('id khách hàng');
            $table->integer('user_send_id')->comment('id nhân viên nhận mail');
            $table->integer('order_id')->comment('id đơn hàng');
            $table->string('email_to', 255)->comment('gửi tới mail');
            $table->string('header', 500)->comment('tiêu đề mail');
            $table->string('body', 5000)->comment('Nội dung');
            $table->tinyInteger('type')->comment('0: mail mẫu, 1: mail gửi');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email');
    }
}
