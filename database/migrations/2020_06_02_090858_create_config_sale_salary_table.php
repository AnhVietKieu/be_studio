<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigSaleSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_sale_salary', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->comment('id của user sale');
            $table->decimal('basic_salary',12,2)->comment('lương cơ bản');
            $table->string('percentage_of_sales',150)->comment('giải doanh số tính hoa hồng cho nhân viên sale');
            $table->string('takecare',150)->comment('giải doanh số được tính thưởng khi được chỉ định làm hợp đồng cho nhân viên sale');
            $table->string('sale_seasonal',150)->comment('Doanh số tối thiểu theo mùa cho nhân viên sale');
            $table->integer('combo_id')->nullable()->comment('id của gói sản phẩm combo tính lương cho photoshop');
            $table->integer('service_id')->nullable()->comment('id của dịch vụ tính lương cho photoshop');
            $table->string('cate_path',5)->nullable()->comment('đường dẫn của combo cha và combo con');
            $table->float('bonus_money')->comment('số tiền nhận được theo combo hoặc dịch vụ tính lương cho photoshop');
            $table->integer('type')->comment('kiểu nhân viên (sale, photoshop)');
            $table->date('excu_date')->comment('Ngày áp dụng');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_sale_salary');
    }
}
