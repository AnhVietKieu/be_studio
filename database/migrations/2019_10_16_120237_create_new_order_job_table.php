<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewOrderJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_job', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_id')->comment('id đơn hàng');
            $table->integer('combo_id')->nullable()->comment('id combo');
            $table->integer('service_id')->nullable()->comment('id dịch vụ');
            $table->integer('order_detail_id')->comment('id combo service');
            $table->integer('department_id')->comment('id phòng ban');
            $table->integer('user_author_id')->comment('người giao việc');
            $table->integer('recipient_id')->nullable()->comment('người nhận việc');
            $table->integer('supplier_id')->nullable()->comment('id nhà cung cấp nhận việc');
            $table->string('name_job', '500')->comment('Tên công việc');
            $table->string('location', 1000)->nullable()->comment('địa điểm');
            $table->dateTime('start_date_plan')->nullable()->comment('ngày giờ bắt đầu dự kiến');
            $table->dateTime('end_date_plan')->nullable()->comment('ngày giờ kết thúc dự kiến');
            $table->dateTime('start_date')->nullable()->comment('ngày giờ bắt đầu thực tế');
            $table->dateTime('end_date')->nullable()->comment('ngày giờ kết thúc thực tế');
            $table->tinyInteger('status')->default(0)
                ->comment('0: chờ xác nhận, 1 Đồng ý, 2 từ chối, 3 đang thực hiện, 4 kết thúc');
            $table->string('url_file', 500)->nullable()->comment('đường dẫn ảnh bằng chứng');
            $table->string('link', 500)->nullable()->comment('link ảnh gốc gửi khách');
            $table->string('note', 500)->nullable()->comment('ghi chú');
            $table->integer('process')->default(0)->comment('% hoàn thành công việc');
            $table->dateTime('confirm_date')->nullable()->comment('ngày nhận việc');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_job');
    }
}
