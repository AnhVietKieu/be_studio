<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_service', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('sale_off');
            $table->dropColumn('number');
            $table->dropColumn('service_id');
            $table->dropColumn('type');
            $table->string('service_ids', 500)->comment('mã service trong combo');
            $table->string('add_services', 500)->comment('mã service thêm trong combo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_service', function (Blueprint $table) {
            //
        });
    }
}
