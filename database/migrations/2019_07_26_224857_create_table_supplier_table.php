<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('name',500)->comment('Tên nhà cung cấp');
            $table->string('code',255)->comment('Mã nhà cung cấp');
            $table->string('address',300)->comment('Địa chỉ giao dịch');
            $table->string('mobile_phone',15)->unique()->comment('Số điện thoại');
            $table->string('email_address',255)->nullable()->comment('Địa chỉ email');
            $table->string('tax_code',255)->nullable()->comment('Mã số thuế');
            $table->string('bank_code',255)->nullable()->comment('Tài khoản ngân hàng');
            $table->text('description')->nullable()->comment('Mô tả chung về khách hàng');
            $table->string('field_work')->nullable()->comment('Lĩnh vực kinh doanh');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier');
    }
}
