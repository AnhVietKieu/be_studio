<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('code', 55)->comment('mã đơn hàng');
            $table->integer('customer_id')->comment('id khách hàng');
            $table->string('shooting_location', 300)->comment('địa điểm chụp');
            $table->tinyInteger('status')->default(0)->comment('trạng thái đơn hàng');
            $table->decimal('total_price', 12, 2)->comment('giá đơn hàng');
            $table->string('note', 500)->nullable()->comment('ghi chú');
            $table->tinyInteger('payment_method')->default(1)->comment('0: tiền mặt; 1: ngân hàng');
            $table->tinyInteger('payment_type_id')->default(1)->comment('id cách thức thanh toán');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
