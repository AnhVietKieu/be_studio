<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldConfigForPhotographer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category', function (Blueprint $table) {
            $table->text('photographer_salary_field_config')
                ->nullable()
//                ->default('
//                [{
//                    "id":"half_day",
//                    "name":"Nửa ngày",
//                    "display":true
//                },
//                {
//                    "id":"full_day",
//                    "name":"Cả ngày",
//                    "display":true
//                },
//                {
//                    "id":"morning",
//                    "name":"Sáng",
//                    "display":true
//                },
//                {
//                    "id":"evening",
//                    "name":"Tối",
//                    "display":true
//                },
//                {
//                    "id":"out_of_province_half_day",
//                    "name":"Nửa ngày",
//                    "display":true
//                },
//                {
//                    "id":"out_of_province_full_day",
//                    "name":"Cả ngày",
//                    "display":true
//                }
//                ]')
                ->comment('Trường lưu cấu hình các buổi cho config lương của photographer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
