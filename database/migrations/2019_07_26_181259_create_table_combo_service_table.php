<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableComboServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combo_service', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('service_id')->comment('id dịch vụ');
            $table->integer('combo_id')->comment('id combo');
            $table->integer('number')->comment('số lượng dịch vụ trong combo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combo_service');
    }
}
