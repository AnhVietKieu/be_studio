<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBasicsalaryToGrouopConfigPartnerCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_config_partner_costs', function (Blueprint $table) {
            $table->integer('basic_salary')->default(0)->comment('Lương cơ bản');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('grouop_config_partner_code', function (Blueprint $table) {
            //
        });
    }
}
