<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('department_id')->comment('id phòng ban');
            $table->string('avatar', 500)->nullable()->comment('url ảnh đại diện');
            $table->decimal('basic_salary', 12, 2)->nullable()->comment('lương cơ bản');
            $table->decimal('percentage_of_sales', 12, 2)->nullable()->comment('lương dịch vụ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
