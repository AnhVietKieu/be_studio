<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllowanceLunchAndTelephone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_sale_salary', function (Blueprint $table) {
            $table->integer('allowance_lunch')->default(0)->comment('Phụ cấp ăn trưa');
            $table->integer('allowance_telephone')->default(0)->comment('Phụ cấp điện thoại');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
