<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTimeToOrderService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_service', function (Blueprint $table) {
            $table->dateTime('start_time')->nullable()->comment('tgian bắt đầu order')->change();
            $table->dateTime('end_time')->nullable()->comment('tgian kết thúc order')->change();
            $table->integer('combo_category_id')->nullable()->comment('category_id của combo trong order')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_service', function (Blueprint $table) {
            //
        });
    }
}
