<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('parent_id')->comment('id cha loại tt');
            $table->string('name', 255)->comment('tên loại thanh toán');
            $table->decimal('money', 12, 2)->comment('số tiền hoặc % tt đợt');
            $table->tinyInteger('payment_type')->default(0)->comment('0: %; 1: tiền');
            $table->tinyInteger('payment_expiry')->nullable()->comment('số ngày có thể quá hạn');
            $table->integer('payment_expiry_type')->default(0)->comment('0: ngày; 1: tháng');
            $table->integer('payment_expiry_by')->nullable()->comment('mốc sự kiện thanh toán');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_type');
    }
}
