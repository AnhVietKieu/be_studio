<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHistoryPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_history_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('order_id')->comment('id đơn hàng');
            $table->decimal('payment_expected', 12, 2)->comment('số tiền tt dự kiến');
            $table->decimal('payment_money', 12, 2)->nullable()->default(0)
                ->comment('số tiền tt thực tế');
            $table->dateTime('payment_date_plan')->comment('ngày dự kiến tt');
            $table->dateTime('payment_date_expected')->nullable()->comment('ngày thực tế tt');
            $table->string('note')->nullable()->comment('ghi chú');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_history_payment');
    }
}
