<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable()->comment('Mã nhân viên');
            $table->integer('supplie_id')->nullable()->comment('Mã id công tác viên');
            $table->integer('department_id')->comment('Mã văn phòng');
            $table->decimal('basic_salary',12,2)->comment('Lương cơ bản');
            $table->float('work_day',12,2)->comment('Số ngày làm việc của nhân viên');
            $table->float('allowance',12,2)->comment('phụ cấp');
            $table->decimal('tax_salary',12,2)->comment('Lương trừ thuế');
            $table->date('start_date')->comment('ngày bắt đầu tính lương');
            $table->date('end_date')->comment('ngày kết thúc tính lương');
            $table->string('note')->comment('ghi chú');
            $table->integer('status')->comment('trang thai 0:thanh toán 1: chưa thanh toán');
            $table->decimal('total',14,2)->comment('tong cong luong');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
