<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAllownullFieldConfigPartnerCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_partner_cost', function (Blueprint $table) {
            $table->float('money_of_combo','12','2')
                ->nullable()
//                ->default(0)
                ->comment('giá của combo')
                ->change();
            $table->float('half_day','12','2')
                ->nullable()
//                ->default(0)
                ->comment('giá ngày công của nửa ngày')
                ->change();
            $table->float('full_day','12','2')
                ->nullable()
//                ->default(0)
                ->comment('giá ngày công của một ngày')
                ->change();
            $table->float('morning','12','2')
                ->nullable()
//                ->default(0)
                ->comment('giá ngày công của buổi sáng')
                ->change();
            $table->float('evening','12','2')
                ->nullable()
//                ->default(0)
                ->comment('giá ngày công của buổi tối')
                ->change();
            $table->float('afterparty','12','2')
                ->nullable()
//                ->default(0)
                ->comment('giá phát sinh thêm giờ')
                ->change();
            $table->float('out_of_province_half_day','12','2')
                ->nullable()
//                ->default(0)
                ->comment('giá nhân công đi tỉnh nửa ngày')
                ->change();
            $table->float('out_of_province_full_day','12','2')
                ->nullable()
//                ->default(0)
                ->comment('giá nhân công đi tỉnh cả ngày')
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
