<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableComboTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('name', 255)->comment('Tên combo dịch vụ');
            $table->string('code', 11)->comment('Mã combo dịch vụ');
            $table->integer('category_id')->comment('Mã danh mục combo ');
            $table->decimal('price', 12, 2)->default(0)->comment('Giá combo');
            $table->string('time_limit', 500)->comment('Giới hạn thời gian của combo');
            $table->string('description', 500)->comment('Mô tả về combo dịch vụ');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combo');
    }
}
