<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('name', 500)->comment('tên doanh nghiệp');
            $table->string('code', 255)->comment('mã khách hàng');
            $table->string('address', 300)->comment('Địa chỉ giao dịch');
            $table->string('mobile_phone', 15)->unique()->comment('số điện thoại');
            $table->string('email_address',255)->nullable()->comment('địa chỉ email');
            $table->string('tax_code',255)->nullable()->comment('mã số thuế');
            $table->string('bank_code',255)->nullable()->comment('tài khoản ngân hàng');
            $table->text('description')->nullable()->comment('mô tả chung vè khách hàng');
            $table->string('field_work',255)->nullable()->comment('lĩnh vực kinh doanh');
            $table->datetime('married_date')->nullable()->comment('ngày cưới');
            $table->tinyInteger('childrens')->default(0)->nullable()->comment('số con');
            $table->datetime('date_of_birth')->nullable()->comment('ngày sinh');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
