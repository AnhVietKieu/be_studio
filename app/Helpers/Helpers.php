<?php

namespace App\Helpers;


class Helpers
{
    public static function padLeft($id, $length, $strPad)
    {
        return str_pad($id, $length, $strPad, STR_PAD_LEFT);
    }

    // group by mảng 2 chiều
    public static function groupArray($arr, $group, $preserveGroupKey = false, $preserveSubArrays = false){
        $temp = array();
        foreach($arr as $key => $value) {
            $groupValue = $value[$group];
            if(!$preserveGroupKey)
            {
                unset($arr[$key][$group]);
            }
            if(!array_key_exists($groupValue, $temp)) {
                $temp[$groupValue] = array();
            }

            if(!$preserveSubArrays){
                $data = count($arr[$key]) == 1? array_pop($arr[$key]) : $arr[$key];
            } else {
                $data = $arr[$key];
            }
            $temp[$groupValue][] = $data;
        }
        return $temp;
    }
    // lấy 1 hoặc 2 thành phần mảng con vào mảng bố trong mảng 2 chiều
    // thành phần đầu là lấy name trong bảng con
    public static function addColumnArray($arr, $columnAdd1, $getColumn2 = '', $columnUnset, $columnAdd2 = '' ){
        foreach($arr as $key => $value) {
            $arr[$key][$columnAdd1] = '';
            $arr[$key][$columnAdd1] = $value[$columnUnset]['name'];
            if($getColumn2){
                $arr[$key][$columnAdd2] = '';
                $arr[$key][$columnAdd2] = $value[$columnUnset][$getColumn2];
            }
            unset($arr[$key][$columnUnset]);
        }
        return $arr;
    }

    public static function superUnique($array,$key)
    {
        $temp_array = [];
        foreach ($array as &$v) {
            if (!isset($temp_array[$v[$key]]))
                $temp_array[$v[$key]] =& $v;
        }
        $array = array_values($temp_array);
        return $array;

    }
}
