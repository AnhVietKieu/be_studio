<?php

namespace App\Console\Commands;

use App\Models\ConfigSaleSalary;
use App\Models\GroupConfigPartnerCost;
use App\Models\OrderJob;
use App\Models\Orders;
use App\Models\PayRoll;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Console\Command;

class UpdateSalary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:salary';

    private $pay_percentage_of_sales = 0;

    private  $ratio_percentage_of_sales = 0;

        // % giải doanh số được tính thưởng khi được chỉ định làm hợp đồng cho nhân viên sale
    private  $pay_takecare = 0;

        //số tiền nhận được theo combo hoặc dịch vụ tính lương cho photoshop
    private $pay_bonus_money = 0;

    private $ratio_takecare = 0;

        // %Doanh số tối thiểu theo mùa cho nhân viên sale
    private $pay_sale_seasonal = 0;

    private $ratio_sale_seasonal = 0;

        //số tiền nhận được theo combo hoặc dịch vụ tính lương cho photoshop

    private $allowance= 0;

    private $reward =0;

        // tong gia tri combo
    private $total_combo = 0;

        // tong gia tri service
    private $total_service = 0;

        //giá của combo
    private $money_of_combo = 0;

        //giá ngày công của nửa ngày
    private $half_day = 0;

        //giá ngày công của một ngày
    private $full_day = 0;

        //giá ngày công của buổi sáng
    private $morning = 0;

        //giá ngày công của buổi tối
    private $evening = 0;

        //giá phát sinh thêm giờ
    private $afterparty = 0;

    private $work_day_contract =0;

    private $work_day_sale = 0;

        //giá nhân công đi tỉnh nửa ngày
    private $out_of_province_half_day =0 ;

        //giá nhân công đi tỉnh cả ngày
    private $out_of_province_full_day = 0;

    private $work_day=0;

    private $total= 0;
        //doanh thu
    private $revenue =0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $time = date('yy-m-d H:m:s',time());

        $start_date_pay = date('yy-m-5 H:m:s',time());

        $end_date_pay = date('yy-m-d H:m:s',strtotime($start_date_pay . "+30 day"));

        $start_date_order = OrderJob::where('status','4')->min('start_date');


        $start_date_begin = date('yy-m-5 H:m:s',strtotime($start_date_order));


        // chay tu $start_date_begin(nhgay nho nhat trong order_job) -> $end_date_pay (5 cuar thang ke)

        for($i=strtotime($start_date_begin);$i<strtotime($end_date_pay);$i=$i+2592000)
        {

            $start_date_pay_roll = date('yy-m-d H:m:s',$i);
            $end_date_pay_roll = date('yy-m-d H:m:s',$i+2592000);

            $order_job =OrderJob::where('status','4')->where('start_date','>=',$start_date_pay_roll)
                ->where('end_date','<',$end_date_pay_roll);

            $user_all = User::with('Department')->get();

            $supplie_all = Supplier::all();

            foreach ($user_all as $user)
            {
                // lương cơ bản$pay_basic_salary =0;

                //% giải doanh số tính hoa hồng cho nhân viên sale
                $pay_percentage_of_sales = 0;

                $ratio_percentage_of_sales = 0;

                // % giải doanh số được tính thưởng khi được chỉ định làm hợp đồng cho nhân viên sale
                $pay_takecare = 0;

                //số tiền nhận được theo combo hoặc dịch  vụ tính lương cho photoshop
                $pay_bonus_money = 0;

                $ratio_takecare = 0;

                // %Doanh số tối thiểu theo mùa cho nhân viên sale
                $pay_sale_seasonal = 0;

                $ratio_sale_seasonal = 0;

                //số tiền nhận được theo combo hoặc dịch vụ tính lương cho photoshop
                $pay_bonus_money =0;

                $allowance= 0;
                $reward =0;

                // tong gia tri combo
                $total_combo = 0;

                // tong gia tri service
                $total_service = 0;

                //giá của combo
                $money_of_combo = 0;

                //giá ngày công của nửa ngày
                $half_day = 0;

                //giá ngày công của một ngày
                $full_day = 0;

                //giá ngày công của buổi sáng
                $morning = 0;

                //giá ngày công của buổi tối
                $evening = 0;

                //giá phát sinh thêm giờ
                $afterparty = 0;

                $work_day_contract =0;

                $work_day_sale = 0;

                //giá nhân công đi tỉnh nửa ngày
                $out_of_province_half_day =0 ;

                //giá nhân công đi tỉnh cả ngày
                $out_of_province_full_day = 0;

                $work_day=0;

                $total= 0;
                //doanh thu
                $revenue =0;
                // id cua user
                $user_model = $user['id'];
                switch ($user['department']['name']){
                    case 'PHOTOSHOP':

                        $payroll_salary =  PayRoll::where('user_id',$user_model)->where('department_id',$user['department']['id'])->where('start_date','<=',$start_date_pay_roll)->where('end_date','<=',$end_date_pay_roll)->first();
                        if(isset($payroll_salary)){

                            $config_sale_salary= ConfigSaleSalary::where('id',$payroll_salary['config_id'])->where('user_id', $user_model)->where('type',1)->where('excu_date','<',$time)->first();

                        }else{
                            $config_sale_salary= ConfigSaleSalary::where('user_id', $user_model)->where('type',1)->where('excu_date','<',$time)->first();

                        }

                        $order_job_photo = $order_job->where('department_id',$user['department']['id'])->where('recipient_id',$user_model)->get();

                        if(isset($order_job_photo)&&count($order_job_photo)>=1) {
                            foreach ($order_job_photo as $key) {

                                $combo_id = $key['combo_id'];
                                $service_id = $key['service_id'];
                                $department_id = $key['department_id'];


                                if(isset($config_sale_salary))
                                {
                                    $basic_photoshop = $config_sale_salary['basic_salary'];

                                    $combo = json_decode($config_sale_salary['combo'],true);
                                    $service = json_decode($config_sale_salary['service'],true);

                                    foreach($combo as $key_combo)
                                    {
                                        if($key_combo['id']==$combo_id){
                                            $total_combo +=$key_combo['total'];
                                        }

                                    }
                                    //tinh tong service
                                    foreach($service as $key_service)
                                    {
                                        if($key_service['id']==$service_id){
                                            $total_service +=$key_service['total'];
                                        }
                                    }



                                    $allowance=$total_combo+$total_service;

                                    $total=$allowance+$basic_photoshop;
                                }
                                $work_day = PayRoll::revenue($start_date_pay,$end_date_pay,$user_model,'','',$user['department']['id']);


                                $arr = array(
                                    'user_id'=>$user_model,
                                    'basic_salary'=>isset($basic_photoshop)?$basic_photoshop:'0',
                                    'department_id'=>$user['department']['id'],
                                    'work_day'=>$work_day['work_day']==0?'0':$work_day['work_day'],
                                    'allowance'=>$allowance,
                                    'start_date'=>$start_date_pay_roll,
                                    'end_date'=>$end_date_pay_roll,
                                    'note'=>'Lương của Photoshop',
                                    'tax_salary'=>'0',
                                    'status' =>'1',
                                    'total'=>$total,
                                    'config_id'=>$config_sale_salary['id']
                                );


                                if(isset($payroll_salary)){

                                    $arr_up= array(
                                        'user_id'=>$user_model,
                                        'basic_salary'=>isset($basic_photoshop)?$basic_photoshop:'0',
                                        'department_id'=>$user['department']['id'],
                                        'work_day'=>$work_day['work_day']==0?'0':$work_day['work_day'],
                                        'allowance'=>$allowance,
                                        'note'=>'Lương của photoshop',
                                        'tax_salary'=>'0',
                                        'status' =>'1',
                                        'total'=>$total,
                                        'config_id'=>$config_sale_salary['id']
                                    );


                                    $payroll_salary->update($arr_up);

                                }else{
                                    PayRoll::create($arr);
                                }

                            }
                        }

                        break;
                    case 'SALE':

                        $payroll_salary =  PayRoll::where('user_id',$user_model)->where('department_id',$user['department']['id'])->where('start_date','<=',$start_date_pay_roll)->where('end_date','<=',$end_date_pay_roll)->first();

                        if(isset($payroll_salary)){
                            $config_sale_salary= ConfigSaleSalary::where('id',$payroll_salary['config_id'])->where('user_id',$user_model)->where('type',0)->where('excu_date','<',$time)->first();
                        }else{
                            $config_sale_salary= ConfigSaleSalary::where('user_id',$user_model)->where('type',0)->where('excu_date','<',$time)->first();
                        }

                        $pay_basic_salary= $config_sale_salary['basic_salary'];

                        $order_contract =Orders::where('user_id',$user_model)->where('customer_id',$user_model)->where('status','1')
                            ->where('created_at','>=',$start_date_pay_roll)
                            ->where('created_at','<',$end_date_pay_roll)->get();

                        // contract

                        foreach ($order_contract as $item1)
                        {
                            $user_order = $item1['user_id'];
                            $customer_order = $item1['customer_id'];
                            $total_price = $item1['total_price'];

                            if(isset($config_sale_salary))
                            {

                                $percentage_of_sales = json_decode($config_sale_salary['percentage_of_sales'],true);

                                $sale_contract = PayRoll::revenue_sale($start_date_pay_roll,$end_date_pay_roll,$user_order,$customer_order,'CONTRACT');


                                foreach ($percentage_of_sales as $key1){

                                    if($key1['from']<$sale_contract['revenue'] &&$key1['to']>$sale_contract['revenue'])
                                    {
                                        $ratio_percentage_of_sales = $key1['total'];
                                    }

                                }
                                $allowance+=$sale_contract['revenue']*$ratio_percentage_of_sales/100;
                                $work_day_contract += $sale_contract['work_day'];


                            }


                        }

                        $order_sale = Orders::where('user_id','!=',$user_model)->where('customer_id',$user_model)->where('status','1')
                            ->where('created_at','>=',$start_date_pay_roll)
                            ->where('created_at','<',$end_date_pay_roll)->get();


                        //sale
                        if(isset($order_sale)&&count($order_sale)>=1){
                            foreach ($order_sale as $item2)
                            {
                                $takecare = json_decode($config_sale_salary['takecare'],true);
                                $sale_seasonal = json_decode($config_sale_salary['sale_seasonal'],true);

                                $sale = PayRoll::revenue_sale($start_date_pay_roll,$end_date_pay_roll,$user_order,$customer_order,'NORMAL');

                                if(isset($takecare)&&count($takecare)>=1)
                                {
                                    foreach ($takecare as $key2) {

                                        if($key2['from']<$sale['revenue'] &&$key2['to']>$sale['revenue'])
                                        {
                                            $ratio_takecare = $key2['total'];
                                        }

                                    }
                                }

                                $allowance+=$sale['revenue']*$ratio_takecare/100;
                                $work_day_sale+=$sale['work_day'];
                            }
                        }

                        $total+= $allowance+$pay_basic_salary;

                        $work_day=($work_day_contract+$work_day_sale);

                        $arr = array(
                            'user_id'=>$user_model,
                            'basic_salary'=>isset($pay_basic_salary)?$pay_basic_salary:'0',
                            'department_id'=>$user['department']['id'],
                            'work_day'=>$work_day,
                            'allowance'=>$allowance,
                            'start_date'=>$start_date_pay_roll,
                            'end_date'=>$end_date_pay_roll,
                            'note'=>'Lương của Sale',
                            'tax_salary'=>'0',
                            'status' =>'1',
                            'total'=>$total,
                            'config_id'=>$config_sale_salary['id']
                        );

                        if(isset($payroll_salary)){
                            $arr_up= array(
                                'user_id'=>$user_model,
                                'basic_salary'=>isset($pay_basic_salary)?$pay_basic_salary:'0',
                                'department_id'=>$user['department']['id'],
                                'work_day'=>$work_day,
                                'allowance'=>$allowance,
                                'note'=>'Lương của Sale',
                                'tax_salary'=>'0',
                                'status' =>'1',
                                'total'=>$total,
                                'config_id'=>$config_sale_salary['id']
                            );
                            $payroll_salary->update($arr_up);
                        }else{
                            PayRoll::create($arr);
                        }
                        break;

                }

            }
            // supplier

            foreach ($supplie_all as $supplier)
            {
                $payroll_partner =  PayRoll::where('supplie_id',$supplier->id)->where('department_id',3)->where('start_date','<=',$start_date_pay_roll)->where('end_date','<=',$end_date_pay_roll)->first();

                        if(isset($payroll_partner))
                        {
                            $config_partner_cost = GroupConfigPartnerCost::where('id',$payroll_partner['config_id'])->where('partner_id',$user_model)->where('excu_date','<',$time)->orderby('excu_date','desc')->with('ConfigPartnerCost')->first();

                        }else{
                            $config_partner_cost = GroupConfigPartnerCost::where('partner_id',$supplier->id)->where('excu_date','<',$time)->orderby('excu_date','desc')->with('ConfigPartnerCost')->first();

                        }

                        $order_job_photographer = $order_job->where('department_id',3)->where('supplier_id',$supplier->id)->get();


                        if(isset($order_job_photographer)&&count($order_job_photographer)>=1) {
                            foreach ($order_job_photographer as $key) {
                                // id combo
                                $combo_id = $key['combo_id'];
                                //id service
                                $service_id = $key['service_id'];


                                if(isset($config_partner_cost['config_partner_cost'])){
                                    foreach ($config_partner_cost['config_partner_cost'] as $key)
                                    {
                                        if($key['combo_id']==$combo_id)
                                        {
                                            $basic_partner = $key['basic_salary'];
                                            $money_of_combo += $key['money_of_combo'];
                                            $half_day+=$key['half_day'];
                                            $full_day += $key['full_day'];
                                            $morning +=$key['morning'];
                                            $evening+=$key['evening'];
                                            $afterparty+=$key['afterparty'];
                                            $out_of_province_half_day+=$key['out_of_province_half_day'];
                                            $out_of_province_full_day+$key['out_of_province_full_day'];
                                        }
                                    }
                                }

                                $total =  isset($basic_partner)+$money_of_combo+$half_day+$full_day+$morning+$evening+$afterparty+$out_of_province_full_day+$out_of_province_half_day;


                                $work_day = PayRoll::revenue($start_date_pay,$end_date_pay,'','',$user_model,$user['department']['id']);

                                $arr = array(
                                    'supplie_id'=>$supplier->id,
                                    'basic_salary'=>isset($basic_partner)?$basic_partner:'0',
                                    'department_id'=>$user['department']['id'],
                                    'work_day'=>$work_day['work_day']==0?'0':$work_day['work_day'],
                                    'allowance'=>'0',
                                    'start_date'=>$start_date_pay_roll,
                                    'end_date'=>$end_date_pay_roll,
                                    'note'=>'Lương của photographer',
                                    'tax_salary'=>'0',
                                    'reward'=>'0',
                                    'status' =>'1',
                                    'total'=>$total,
                                    'config_id'=>$config_partner_cost['id']
                                );


                                if(isset($payroll_partner)){
                                    $arr_up =array(
                                        'supplie_id'=>$supplier->id,
                                        'basic_salary'=>isset($basic_partner)?$basic_partner:'0',
                                        'department_id'=>$user['department']['id'],
                                        'work_day'=>$work_day['work_day']==0?'0':$work_day['work_day'],
                                        'allowance'=>0,
                                        'note'=>'Lương của photographer',
                                        'tax_salary'=>'0',
                                        'reward'=>'0',
                                        'status' =>'1',
                                        'total'=>$total,
                                        'config_id'=>$config_partner_cost['id']
                                    );

                                    $payroll_partner->update($arr_up);
                                }else{
                                    PayRoll::create($arr);
                                }

                            }
                        }
            }


        }
    }
}
