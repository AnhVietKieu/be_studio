<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Roles;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
     public function boot()
    {
            $dataRoles = Roles::select('name', 'permissions')->get();
            $scopes = [];
            foreach ($dataRoles as $value) {
                $scopes[$value->name] = $value->permissions;
            }

            $this->registerPolicies();
            Passport::routes(function ($router) {
                $router->forAccessTokens();
                $router->forPersonalAccessTokens();
                $router->forTransientTokens();
            });
            Passport::personalAccessTokensExpireIn(now()->addDays(1));
            Passport::refreshTokensExpireIn(now()->addDays(30));
            Passport::pruneRevokedTokens();
            Passport::tokensCan($scopes);
    }

}
