<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\OrderRepository;
use App\Repositories\Api\OrderJobRepository;

class DashboardController extends Controller
{
    protected $orderRepository;

    protected $orderJobRepository;

    public function __construct(OrderRepository $orderRepository, OrderJobRepository $orderJobRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->orderJobRepository = $orderJobRepository;
    }

    public function index(Request $request)
    {
        $data = $this->orderRepository->getOrderForDashboard($request);
        $data['jobs'] = $this->orderJobRepository->getJobFollowCurrentUser($request);

        return $this->responseJsonListOrDetail($data);
    }
}
