<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\OrderJobRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderJobController extends Controller
{
    protected $orderJobRepository;

    public function __construct(orderJobRepository $orderJobRepository)
    {
        $this->orderJobRepository = $orderJobRepository;
    }

    /**
     * danh sách công việc theo ngày
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listJobs(Request $request)
    {
        $job = $this->orderJobRepository->getListJob($request);

        return $this->responseJsonListOrDetail($job);
    }

    /**
     * return list jobs of month
     * @param Request $request
     * @param $month
     */
    public function listJobOfMonth(Request $request,$month){
        $job = $this->orderJobRepository->getListJobOfMonth($month);
        return $this->responseJsonListOrDetail($job);
    }

    /**
     * chi tiết công việc
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view($id)
    {
        $job = $this->orderJobRepository->detailJob($id);

        return $this->responseJsonListOrDetail($job);
    }

    /**
     * tạo mới công việc
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($id)
    {
        $job = $this->orderJobRepository->saveData($id);

        return $this->responseJson($job, 'Tạo mới thành công');
    }

    /**
     * cập nhật công việc
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $job = $this->orderJobRepository->updateData($request);

        return $this->responseJson($job, 'Cập nhật thành công');
    }

    /**
     * assign job
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assign(Request $request)
    {
        $validateResult=$this->orderJobRepository->validateInput('assign',$request);
        if($validateResult['error'])
        {
            return $this->responseJson($validateResult, 'Giao việc thất bại');
        }

        $job = $this->orderJobRepository->assign($request);
        return $this->responseJson($job, 'Giao việc thành công.');
    }

    public function index(Request $request)
    {
        $job = $this->orderJobRepository->getListJob($request);

        return $this->responseJsonListOrDetail($job);
    }

    public function list($id)
    {
        $job = false;
        DB::beginTransaction();
        try {
            //$job = $this->orderJobRepository->listByOrder($id);
            $job = $this->orderJobRepository->detailJob($id);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJsonListOrDetail($job);
    }

    public function listNumbers(Request $request)
    {
        $job = $this->orderJobRepository->getListNumber($request);

        return $this->responseJsonListOrDetail($job);
    }

}
