<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\PaymentTypeRepository;

class PaymentTypeController extends Controller
{
    protected $paymentTypeRepository;

    public function __construct(paymentTypeRepository $paymentTypeRepository)
    {
        $this->paymentTypeRepository = $paymentTypeRepository;
    }

    public function index(Request $request)
    {
        $payment = false;

        $payment = $this->paymentTypeRepository->getList($request);

        return $this->responseJsonListOrDetail($payment);
    }

}
