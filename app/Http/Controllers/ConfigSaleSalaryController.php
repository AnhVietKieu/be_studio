<?php

namespace App\Http\Controllers;

use App\Models\ConfigPartnerCost;
use App\Models\ConfigSaleSalary;
use App\Models\OrderJob;
use App\Models\User;
use App\Repositories\Api\ConfigSaleSalaryRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ConfigSaleSalaryController extends Controller
{
    //
    protected $configSaleSalaryRepository;

    public function __construct(ConfigSaleSalaryRepository $configSaleSalaryRepository)
    {
        $this->configSaleSalaryRepository = $configSaleSalaryRepository;
    }

    public function index(Request $request)
    {

        $data = $this->configSaleSalaryRepository->getList($request);

        return $this->responseJsonListOrDetail($data);

    }

    public function show($id)
    {
        $data = $this->configSaleSalaryRepository->getDetail($id);

        return $this->responseJsonListOrDetail($data);
    }

    public function create(Request $request)
    {
        if (!$this->configSaleSalaryRepository->validateInputData($request)) {
            $error = [
                'error' => true,
                'messages' => 'Không thể tạo mới bản ghi trùng lặp thời gian'
            ];
            return $this->responseJson($error, 'Lỗi tạo mới');
        }
        $config_sale = $this->configSaleSalaryRepository->saveData($request);
        return $this->responseJson($config_sale, 'Tạo mới thành công');
    }

    public function update(Request $request, $id)
    {
        $config_sale = false;

        DB::beginTransaction();
        try {
            $config_sale = $this->configSaleSalaryRepository->updateData($request, $id);

            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($config_sale, 'Cập nhật thành công');
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->configSaleSalaryRepository->deleteData($ids);
        }
        return $this->responseJsonDelete($result);
    }


    public function getConfigFieldPhotoshop()
    {
        $result = $this->configSaleSalaryRepository->getConfigFieldForPhotoshop();
        return $this->responseJsonListOrDetail($result);
    }
}
