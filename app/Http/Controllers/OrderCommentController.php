<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\OrderCommentRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderCommentController extends Controller
{
    protected $orderCommentRepository;

    public function __construct(orderCommentRepository $orderCommentRepository)
    {
        $this->orderCommentRepository = $orderCommentRepository;
    }

    public function index(Request $request)
    {

        $order = false;

        $order = $this->orderCommentRepository->getList($request);

        return $this->responseJsonListOrDetail($order);
    }

    public function create(Request $request)
    {
        $orderComment = false;
        DB::beginTransaction();
        try {
            $orderComment = $this->orderCommentRepository->saveData($request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($orderComment, 'Tạo mới thành công');
    }

    public function view(Request $request, $id)
    {
        $orderComment = $this->orderCommentRepository->getListByOrderId($request, $id);

        return $this->responseJsonListOrDetail($orderComment);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $orderComment = false;

        $orderComment = $this->orderCommentRepository->deleteOrder($id);

        return $this->responseJsonDelete($orderComment);
    }

    public function update(Request $request, $id)
    {
        $orderComment = false;
        DB::beginTransaction();
        try {
            $orderComment = $this->orderCommentRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($orderComment, 'Cập nhật thành công');
    }
}
