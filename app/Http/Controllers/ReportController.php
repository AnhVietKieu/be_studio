<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\OrderRepository;
use App\Repositories\Api\ExpenseRepository;
use App\Repositories\Api\CustomerRepository;

class ReportController extends Controller
{
    protected $orderRepository;

    protected $expenseRepository;

    protected $customerRepository;


    public function __construct(OrderRepository $orderRepository, CustomerRepository $customerRepository, ExpenseRepository $expenseRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->customerRepository = $customerRepository;
        $this->expenseRepository = $expenseRepository;
    }

    public function index(Request $request)
    {
        $search = json_decode($request->get('search'));
        $data['customers'] = $this->customerRepository->getDataForReport($request);
        $data['orders'] = $this->orderRepository->getOrdersAllMonthFollowSale($search);
        $data['revenue'] = $this->orderRepository->getRevenueAllMonthFollowSale($search);
        $data['profit'] = $this->orderRepository->getProfitAllMonth();
        $data['cost'] = $this->expenseRepository->getCostAllMonth();

        return $this->responseJsonListOrDetail($data);
    }
}
