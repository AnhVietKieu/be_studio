<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\UserRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $user = $this->userRepository->getList($request);

        return $this->responseJsonListOrDetail($user);
    }

    public function updateUser(Request $request)
    {
        $user = $this->userRepository->updateUser($request);

        return $user;
    }

    public function lockUser(Request $request, $id)
    {
        $user = false;
        DB::beginTransaction();
        try {
            $user = $this->userRepository->lockUsers($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($user, 'Cập nhật thành công');
    }

    public function editUser(Request $request, $id)
    {
        $user = false;
        DB::beginTransaction();
        try {
            $user = $this->userRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {

            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($user, 'Cập nhật thành công');
    }

    public function listDepartment(Request $request)
    {
        $user = $this->userRepository->listDepartment($request);
        return $this->responseJsonListOrDetail($user);
    }

    public function listSelect(Request $request)
    {
        $user = $this->userRepository->getListSelect($request);
        return $this->responseJsonListOrDetail($user);
    }

    public function getDetail($id)
    {
        $user = $this->userRepository->getUserDetail($id);

        return $this->responseJsonListOrDetail($user);
    }

    public function listParam(Request $request)
    {
        $user = $this->userRepository->listUser($request);

        return $this->responseJsonListOrDetail($user);
    }

}
