<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\PaymentHistoryRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PaymentHistoryController extends Controller
{
    protected $paymentHistoryRepository;

    public function __construct(PaymentHistoryRepository $paymentHistoryRepository)
    {
        $this->paymentHistoryRepository = $paymentHistoryRepository;
    }

    public function create(Request $request)
    {
        $paymentHistory = false;
        DB::beginTransaction();
        try {
            $paymentHistory = $this->paymentHistoryRepository->saveData($request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($paymentHistory, 'Tạo mới thành công');
    }

    public function history(Request $request, $id)
    {
        $paymentHistory = false;

        $paymentHistory = $this->paymentHistoryRepository->getHistory($request, $id);

        return $this->responseJsonListOrDetail($paymentHistory);
    }
    public function listByOrder($order_id)
    {
        $paymentByOrder = false;

        $paymentByOrder = $this->paymentHistoryRepository->getPaymentByOrder($order_id);

        return $this->responseJsonListOrDetail($paymentByOrder);
    }

    public function getPayment($order_id)
    {
        $paymentByOrder = false;

        $paymentByOrder = $this->paymentHistoryRepository->getPaymentHistory($order_id);

        return $this->responseJsonListOrDetail($paymentByOrder);
    }

    public function update(Request $request, $id)
    {
        $paymentHistory = false;
        DB::beginTransaction();
        try {
            $paymentHistory = $this->paymentHistoryRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($paymentHistory, 'Cập nhật thành công');
    }
    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->paymentHistoryRepository->deletePayment($ids);
        }
        return $this->responseJsonDelete($result);
    }
}
