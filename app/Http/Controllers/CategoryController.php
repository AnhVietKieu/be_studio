<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\CategoryRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

     public function index(Request $request)
    {
        $category = $this->categoryRepository->getList($request);

        return $this->responseJsonListOrDetail($category);
    }

    public function create(Request $request)
    {
        $category = false;

        DB::beginTransaction();
        try {
            $category = $this->categoryRepository->saveData($request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($category, 'Tạo mới thành công');
    }

    public function view(Request $request, $id)
    {
        $cars = $this->categoryRepository->getDetail($request, $id);

        return $this->responseJsonListOrDetail($cars);
    }

    public function listSelect()
    {
        $data = $this->categoryRepository->getListSelect();

        return $this->responseJsonListOrDetail($data);
    }

    public function listCombo(Request $request)
    {
        $data = $this->categoryRepository->getListCombo($request);

        return $this->responseJsonListOrDetail($data);
    }

    public function getCombo($id)
    {
        $data = $this->categoryRepository->getComboByCategory($id);

        return $this->responseJsonListOrDetail($data);
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->categoryRepository->deleteCategory($ids);
        }

        return $this->responseJsonDelete($result);
    }

    public function update(Request $request, $id)
    {
        $category = false;
        DB::beginTransaction();
        try {
            $category = $this->categoryRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($category, 'Cập nhật thành công');
    }


}
