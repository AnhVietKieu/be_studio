<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\RoleRepository;

class RoleController extends Controller
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function index(Request $request)
    {
        $roles = $this->roleRepository->getList($request);

        return $this->responseJsonListOrDetail($roles);
    }
}
