<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\OrderIncurredRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderIncurredController extends Controller
{
    protected $orderIncurredRepository;

    public function __construct(OrderIncurredRepository $orderIncurredRepository)
    {
        $this->orderIncurredRepository = $orderIncurredRepository;
    }

    public function view(Request $request, $id)
    {
        $orderIncurred = $this->orderIncurredRepository->getDetail($request, $id);

        return $this->responseJsonListOrDetail($orderIncurred);
    }

    public function create(Request $request)
    {
        $orderIncurred = false;

        DB::beginTransaction();
        try {
            $orderIncurred = $this->orderIncurredRepository->saveData($request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($orderIncurred, 'T?o m?i thành công');
    }

    public function listOrderIncurred(Request $request, $id)
    {
        $orderIncurred = $this->orderIncurredRepository->getDetailIncurred($request, $id);

        return $this->responseJsonListOrDetail($orderIncurred);

    }

    public function listSelect()
    {
        $data = $this->orderIncurredRepository->getListSelect();

        return $this->responseJsonListOrDetail($data);
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->orderIncurredRepository->deleteOrderIncurred($ids);
        }

        return $this->responseJsonDelete($result);    }

    public function update(Request $request, $id)
    {
        $orderIncurred = false;
        DB::beginTransaction();
        try {
            $orderIncurred = $this->orderIncurredRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }
        return $this->responseJson($orderIncurred, 'Cập nhật thành công');
    }
}
