<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public $successStatus = 200;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'c_password' => 'required|same:password',
                'department_id' => 'required',
                'phone_number' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'error' => $validator->errors()
                ], 400);
        }

        $input = $request->all();
        $input['percentage_of_sales'] = json_encode($input['percentage_of_sales'], 1);
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
//        $success['access_token'] = $user->createToken('MyApp')->accessToken;
//        $success['data'] = $user;
        $user->roles()->attach($request->roles);

        return response()->json(
            [
                'success' => 'Tạo mới thành công',
            ],
            $this->successStatus
        );
    }

    /**
     * Login user and create token
     *
     * @param Request $request
     * -  [string] email
     * -  [string] password
     * -  [boolean] remember_me
     * - [string] access_token
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $scopes = $user->roles->pluck('name')->all();
        $tokenResult = $user->createToken('Personal Access Token', $scopes);
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'success' => [
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
//                    'data' => $tokenResult->token,
                ]
            ]
        )->header('Authorization', $tokenResult->accessToken);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ], 200);
    }

    /**
     * Get the authenticated User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {

        $data = $request->user();
        $data->percentage_of_sales = json_decode($data->percentage_of_sales, true);
        $data['scopes'] = $data->roles->pluck('name')->all();
        unset($data['roles']);

        return response()->json($data);
    }
}
