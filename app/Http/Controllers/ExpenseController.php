<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\ExpenseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ExpenseController extends Controller
{
    const PAGE_LIMIT = 20;
    protected $expenseRepository;

    public function __construct(ExpenseRepository $expenseRepository)
    {

        $this->expenseRepository = $expenseRepository;
    }
    public function index(Request $request){
        $expense = $this->expenseRepository->getList($request);

        return $this->responseJsonListOrDetail($expense);
    }

    public function create(Request $request){

        $expense = false;

        DB::beginTransaction();
        try {
            $expense = $this->expenseRepository->saveData($request);

            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($expense, 'Tạo mới thành công');
    }

    public function update(Request $request, $id){
        $expense = false;
        DB::beginTransaction();
        try {
            $expense = $this->expenseRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($expense, 'Cập nhật thành công');
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->expenseRepository->deleteExpense($ids);
        }
        return $this->responseJsonDelete($result);
    }

    public function listSelect()
    {
        $expense = $this->expenseRepository->getPaginate(self::PAGE_LIMIT);

        return $this->responseJsonListOrDetail($expense);
    }

    public function view(Request $request, $id)
    {
        $expense = $this->expenseRepository->getDetail($request, $id);

        return $this->responseJsonListOrDetail($expense);
    }

}
