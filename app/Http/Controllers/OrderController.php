<?php

namespace App\Http\Controllers;

use App\Repositories\Api\OrderJobRepository;
use Illuminate\Http\Request;
use App\Repositories\Api\OrderRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    protected $orderRepository;

    public function __construct(orderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function index(Request $request)
    {
        $order = $this->orderRepository->getList($request);

        return $this->responseJsonListOrDetail($order);
    }

    public function view(Request $request, $id)
    {
        $order = $this->orderRepository->getDetail($request, $id);

        return $this->responseJsonListOrDetail($order);
    }

    public function listSelect(Request $request)
    {
        $order = $this->orderRepository->getListSelect($request);

        return $this->responseJsonListOrDetail($order);
    }

    public function create(Request $request)
    {
        $order = false;

        DB::beginTransaction();
        try {
            $order = $this->orderRepository->saveData($request);

            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($order, 'Tạo mới thành công');
    }

    public function update(Request $request, $id)
    {
        $order = false;

        DB::beginTransaction();
        try {
            $order = $this->orderRepository->updateData($request, $id);

            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($order, 'Cập nhật thành công');
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->orderRepository->deleteOrders($ids);
        }
       return $this->responseJsonDelete($result);
    }

    //thêm ngày chụp
    public function addDatePhoto(Request $request)
    {
        $orderDetail = false;
        //validate state order job
        if(!$this->orderRepository->canChangeDateSchedule($request)){
            $error = [
                'error' => true,
                'messages' => 'Order đã được triển khai, không thể cập nhật.'
            ];
            return $this->responseJson($error, 'Lỗi cập nhật');
        }


        DB::beginTransaction();
        try {
            $orderDetail = $this->orderRepository->saveDatePhoto($request);
            //update orderjob
            OrderJobRepository::saveData($orderDetail->order_service->order->id);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($orderDetail, 'Thêm ngày chụp ảnh thành công');
    }

    // timeline dự kiến
    public function expected($id)
    {
        $timeline = $this->orderRepository->getTimeline($id);

        return $this->responseJsonListOrDetail($timeline);
    }

    // lấy thông tin cần để sửa hóa đơn
    public function listEdit($id)
    {
        $order = $this->orderRepository->getListEdit($id);

        return $this->responseJsonListOrDetail($order);
    }

    public function listNumber()
    {
        $order = $this->orderRepository->getListNumber();

        return $this->responseJsonListOrDetail($order);
    }
}
