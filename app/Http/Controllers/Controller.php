<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="L5 OpenApi",
     *      description="L5 Swagger OpenApi description",
     *      @OA\Contact(
     *          email="darius@matulionis.lt"
     *      ),
     *     @OA\License(
     *         name="Apache 2.0",
     *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *     )
     * )
     */
    protected $successStatus = 200;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function responseJson($store, $message)
    {
        if ((isset($store['error']) && $store['error'] == true) | !$store) {
            return response()->json([
                'message' => isset($store['messages']) ? $store['messages'] : 'error',
            ], 400);
        }

        return response()->json([
            'message' => $message,
            'success' => $store,
        ], $this->successStatus);
    }

    public function responseJsonListOrDetail($data)
    {
        if (is_string($data)) {
            return response()->json([
                'message' => $data,
            ], 401);
        }
        return response()->json([
            'data' => $data,
        ], $this->successStatus);
    }

    public function responseJsonDelete($result, $message = "Hóa đơn đã có bảng kê")
    {
        if (!empty($result) && $result != 'error') {
            return response()->json([
                'message' => " {$result} ",
            ], $this->successStatus);
        } elseif ($result == 'error') {
            return response()->json([
                'message' => $message,
            ], 416);
        } else {
            return response()->json([
                'message' => "Xóa thất bại",
            ], 401);
        }
    }

}
