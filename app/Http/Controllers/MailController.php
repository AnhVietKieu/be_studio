<?php

namespace App\Http\Controllers;

use App\Models\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Mail\SendMail;

class MailController extends Controller
{
    protected $model;

    public function __construct(Email $model)
    {
        $this->model = $model;
    }

    public function getLists(Request $request)
    {
        if(!empty($request->user_send_id)){
            $id = $request->user_send_id;
        }else{
            $id = $request->customer_id;
        }
        $lists = $this->model
            ->where('order_id', $request->order_id)
            ->where('customer_id', $id)
            ->where('type', '!=', 0)
            ->orWhere('user_send_id', $id)
            ->get();

        return $this->responseJsonListOrDetail($lists);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $email = $this->model;
        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel
            $email->fill($data)->save();
            $objSend = new \stdClass();
            $objSend->email = $data['email'];
            $objSend->bodyContent = $data['body'];
            $objSend->header = $data['header'];
            Mail::to($objSend->email)->send(new SendMail($objSend));
            DB::commit();
            return $email;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return false;
        }
    }
}
