<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\ComboServiceRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ComboServiceController extends Controller
{
    protected $comboServiceRepository;

    public function __construct(ComboServiceRepository $comboServiceRepository)
    {
        $this->comboServiceRepository = $comboServiceRepository;
    }

     public function index(Request $request)
    {
        $comboService = $this->comboServiceRepository->getList($request);

        return $this->responseJsonListOrDetail($comboService);
    }

    public function create(Request $request)
    {
        $comboService = false;
        DB::beginTransaction();
        try {
            $comboService = $this->comboServiceRepository->saveData($request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($comboService, 'Tạo mới thành công');
    }

    public function view(Request $request, $id)
    {
        $comboService = $this->comboServiceRepository->getDetail($request, $id);

        return $this->responseJsonListOrDetail($comboService);
    }

    public function listSelect()
    {
        $comboService = $this->comboServiceRepository->getListSelect();

        return $this->responseJsonListOrDetail($comboService);
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->comboServiceRepository->deleteComboService($ids);
        }

        return $this->responseJsonDelete($result);
    }

    public function update(Request $request, $id)
    {
        $comboService = false;
        DB::beginTransaction();
        try {
            $comboService = $this->comboServiceRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($comboService, 'Cập nhật thành công');
    }
}
