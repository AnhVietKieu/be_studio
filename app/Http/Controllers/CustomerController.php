<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\CustomerRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller
{
    protected $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function index(Request $request)
    {
        $customer = $this->customerRepository->getList($request);

        return $this->responseJsonListOrDetail($customer);
    }

    public function listSelect()
    {
        $data = $this->customerRepository->getListSelect();

        return $this->responseJsonListOrDetail($data);
    }

    public function listSearch(Request $request)
    {
        $data = $this->customerRepository->getListSearch($request);

        return $this->responseJsonListOrDetail($data);
    }

    public function view(Request $request, $id)
    {
        $customer = $this->customerRepository->getDetail($request, $id);

        return $this->responseJsonListOrDetail($customer);
    }

    public function create(Request $request)
    {
        $customer = false;
        $customer = $this->customerRepository->saveData($request);
        return $this->responseJson($customer, 'Tạo mới thành công');
    }

    public function update(Request $request, $id)
    {
        $customer = false;
        DB::beginTransaction();
        try {
            $customer = $this->customerRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($customer, 'Cập nhật thành công');
    }

    public function order($id)
    {
        $customer = $this->customerRepository->getListOrder($id);

        return $this->responseJsonListOrDetail($customer);
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->customerRepository->deleteCustomer($ids);
        }
        return $this->responseJsonDelete($result);
    }

}
