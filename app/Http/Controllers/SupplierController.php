<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\SupplierRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SupplierController extends Controller
{
    protected $supplierRepository;

    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

     public function index(Request $request)
    {
        $supplier = $this->supplierRepository->getList($request);

        return $this->responseJsonListOrDetail($supplier);
    }

    public function view(Request $request, $id)
    {
        $supplier = $this->supplierRepository->getDetail($request, $id);

        return $this->responseJsonListOrDetail($supplier);
    }

    public function listSelect()
    {
        $data = $this->supplierRepository->getListSelect();

        return $this->responseJsonListOrDetail($data);
    }

    public function listSearch(Request $request)
    {
        $data = $this->supplierRepository->getListSearch($request);
        return $this->responseJsonListOrDetail($data);
    }

    /**
     * @OA\Get(
     *      path="/api/supplier/list/groupFieldWork",
     *      tags={"Supplier"},
     *      description="List Supplier Group by FieldWork",
     *      security={
     *          {"passport": {}},
     *      },
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */


    /**
     * return list supplier group by field_work
     */
    public function listSupplierByWorkField(){
        $data = $this->supplierRepository->getListSupplierByWorkField();
        return $this->responseJsonListOrDetail($data);
    }

    public function listSearchResponseFormatLikeUser(Request $request)
    {
        $request['non_paging']=true;
        $data = $this->supplierRepository->getListSearch($request);
        $result=[
            [
                'user'=>$data
            ]
        ];
        return $this->responseJsonListOrDetail($result);
    }

    public function create(Request $request)
    {
        $supplier = false;
        DB::beginTransaction();
        try {
            $supplier = $this->supplierRepository->saveData($request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($supplier, 'Tạo mới thành công');
    }

    public function update(Request $request, $id)
    {
        $supplier = false;
        DB::beginTransaction();
        try {
            $supplier = $this->supplierRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($supplier, 'Cập nhật thành công');
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->supplierRepository->deleteSupplier($ids);
        }

        return  $this->responseJsonDelete($result, 'Xe đã có hóa đơn');
    }

    public function listJob($id)
    {
        $supplier = $this->supplierRepository->getListJob($id);

        return $this->responseJsonListOrDetail($supplier);
    }
}
