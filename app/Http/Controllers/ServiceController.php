<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\ServiceRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ServiceController extends Controller
{
    protected $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

     public function index(Request $request)
    {
        $service = $this->serviceRepository->getList($request);

        return $this->responseJsonListOrDetail($service);
    }

    public function create(Request $request)
    {
        $service = false;
        DB::beginTransaction();
        try {
            $service = $this->serviceRepository->saveData($request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($service, 'Tạo mới thành công');
    }

    public function view(Request $request, $id)
    {
        $service = $this->serviceRepository->getDetail($request, $id);

        return $this->responseJsonListOrDetail($service);
    }

    public function listSelect()
    {
        $service = $this->serviceRepository->getListSelect();

        return $this->responseJsonListOrDetail($service);
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->serviceRepository->deleteComboService($ids);
        }

        return $this->responseJsonDelete($result);
    }

    public function update(Request $request, $id)
    {
        $service = false;
        DB::beginTransaction();
        try {
            $service = $this->serviceRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($service, 'Cập nhật thành công');
    }
}
