<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\ComboRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ComboController extends Controller
{
    protected $comboRepository;

    public function __construct(ComboRepository $comboRepository)
    {
        $this->comboRepository = $comboRepository;
    }

     public function index(Request $request)
    {
        $combo = $this->comboRepository->getList($request);

        return $this->responseJsonListOrDetail($combo);
    }

    public function create(Request $request)
    {
        $combo = false;

        DB::beginTransaction();
        try {
            $combo = $this->comboRepository->saveData($request);

            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($combo, 'Tạo mới thành công');
    }

    public function view($id)
    {
        $combo = $this->comboRepository->getDetail($id);

        return $this->responseJsonListOrDetail($combo);
    }

    public function listSelect()
    {
        $data = $this->comboRepository->getListSelect();

        return $this->responseJsonListOrDetail($data);
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->comboRepository->deleteCombo($ids);
        }

        return $this->responseJsonDelete($result);
    }

    public function update(Request $request, $id)
    {
        $combo = false;
        DB::beginTransaction();
        try {
            $combo = $this->comboRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($combo, 'Cập nhật thành công');
    }

    public function comboOrder($id)
    {
        $combo = false;
        DB::beginTransaction();
        try {
            $combo = $this->comboRepository->getDetail($id, 1);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJsonListOrDetail($combo);
    }
}
