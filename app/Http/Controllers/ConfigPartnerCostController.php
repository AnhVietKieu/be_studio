<?php

namespace App\Http\Controllers;

use App\Models\GroupConfigPartnerCost;
use App\Repositories\Api\ConfigPartnerCostRepository;
use App\Repositories\Api\GroupConfigPartnerCostRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ConfigPartnerCostController extends Controller
{
    protected $configPartnerCostRepository;
    protected $groupConfigPartnerCostRepository;

    public function __construct(
        ConfigPartnerCostRepository $configPartnerCostRepository,
        GroupConfigPartnerCostRepository $groupConfigPartnerCostRepository
    )
    {
        $this->configPartnerCostRepository = $configPartnerCostRepository;
        $this->groupConfigPartnerCostRepository = $groupConfigPartnerCostRepository;
    }

    public function index(Request $request)
    {
        $data = $this->groupConfigPartnerCostRepository->getList($request);
        return $this->responseJsonListOrDetail($data);
    }

    public function show($id)
    {
        $data = $this->groupConfigPartnerCostRepository->findOne($id);

        return $this->responseJsonListOrDetail($data);
    }

    public function create(Request $request)
    {
        if (!$this->groupConfigPartnerCostRepository->validateInputData($request)) {
            $error = [
                'error' => true,
                'messages' => 'Không thể tạo mới bản ghi trùng lặp thời gian'
            ];
            return $this->responseJson($error, 'Lỗi cập nhật');
        }
            $config_sale = $this->groupConfigPartnerCostRepository->saveData($request);
        return $this->responseJson($config_sale, 'Tạo mới thành công');
    }

    public function update(Request $request, $id)
    {
        $config_sale = false;

        DB::beginTransaction();
        try {
            $config_sale = $this->groupConfigPartnerCostRepository->updateData($request, $id);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($config_sale, 'Cập nhật thành công');
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->groupConfigPartnerCostRepository->deleteData($ids);
        }
        return $this->responseJsonDelete($result);
    }


    public function getConfigFieldPhotographer()
    {
        $result = $this->configPartnerCostRepository->getConfigFieldPhotographer();
        return $this->responseJsonListOrDetail($result);
    }
}
