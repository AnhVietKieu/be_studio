<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Notifications\PasswordResetSuccess;

class PasswordResetController extends Controller
{
     /**
     * Reset password
     * @param Request $request
     * - [string] email
     * -  [string] password
     * -  [string] password_confirmation
     * -  [string] token
     * @return array|string user object
     */
    public function reset(Request $request)
    {
        $user = User::find($request->id);
        DB::beginTransaction();
        try {
            $newPassword = str_random(8);
            $user->update([
                'password' => bcrypt($newPassword),
            ]);
            if ($user){
                $user->notify(new PasswordResetSuccess($newPassword));
            }

            DB::commit();
            return response()->json([
                    'success' => 'Thành công'
                ]
            );
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Đổi password thất bại',
            ];
        }
    }

    function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'oldPassword' => 'required',
                'newPassword' => 'required',
                'c_newPassword' => 'required|same:newPassword',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'error' => $validator->errors()
                ], 400);
        }

        $data = $request->all();
        $user = Auth::guard('api')->user();

        //Changing the password only if is different of null
        if (
            isset($data['oldPassword'])
            && !empty($data['oldPassword'])
            && $data['oldPassword'] !== ""
            && $data['oldPassword'] !=='undefined'
            ) {
            //checking the old password first
            $check  = Auth::guard('web')->attempt([
                'email' => $user->email,
                'password' => $data['oldPassword']
            ]);

            if (
                $check && isset($data['newPassword'])
                && !empty($data['newPassword'])
                && $data['newPassword'] !== ""
                && $data['newPassword'] !=='undefined') {
                $user->password = bcrypt($data['newPassword']);

                //Changing the type
                $user->save();

                return response()->json([
                        'success' => 'Đổi password thành công'
                    ]
                ); //sending the new token
            }
            else {
                return "Wrong password information";
            }
        }
        return "Wrong password information";
    }
}