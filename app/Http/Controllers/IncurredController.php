<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Api\IncurredRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IncurredController extends Controller
{
    protected $incurredRepository;

    public function __construct(IncurredRepository $incurredRepository)
    {
        $this->incurredRepository = $incurredRepository;
    }

     public function index(Request $request)
    {
        $incurred = $this->incurredRepository->getList($request);

        return $this->responseJsonListOrDetail($incurred);
    }

    public function create(Request $request)
    {
        $incurred = false;
        DB::beginTransaction();
        try {
            $incurred = $this->incurredRepository->saveData($request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($incurred, 'Tạo mới thành công');
    }

    public function view(Request $request, $id)
    {
        $incurred = $this->incurredRepository->getDetail($request, $id);

        return $this->responseJsonListOrDetail($incurred);
    }

    public function listSelect()
    {
        $data = $this->incurredRepository->getListSelect();

        return $this->responseJsonListOrDetail($data);
    }

    public function delete(Request $request)
    {
        $ids = $request->ids;
        $result = false;
        if (count($ids) > 0) {
            $result = $this->incurredRepository->deleteIncurred($ids);
        }

        return $this->responseJsonDelete($result);
    }

    public function update(Request $request, $id)
    {
        $incurred = false;
        DB::beginTransaction();
        try {
            $incurred = $this->incurredRepository->updateData($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
        }

        return $this->responseJson($incurred, 'Cập nhật thành công');
    }
}
