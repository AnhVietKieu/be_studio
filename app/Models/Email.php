<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'email';

    protected $fillable = ['user_id', 'customer_id', 'user_send_id', 'header', 'body', 'type'];

    public $timestamps = true;

    public function customer(){
        return $this->belongsTo(Customer::class);
    }
}
