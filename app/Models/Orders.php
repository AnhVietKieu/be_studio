<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'orders';

    protected $fillable = [
        'code',
        'user_id',
        'customer_id',
        'shooting_location',
        'status',
        'total_price',
        'note',
        'payment_method',
        'payment_type_id',
        'vat',
        'sale_off',
        'created_at',
        'updated_at',
        'take_care_id'
    ];

    public $timestamps = true;

    public function paymentType(){
        return $this->hasMany(PaymentType::class, 'parent_id', 'payment_type_id');
    }

    public function orderHistoryPayment(){
        return $this->hasMany(OrderHistoryPayment::class, 'order_id');
    }
    public function paymentHistoryDetail(){
        return $this->hasMany(PaymentHistoryDetail::class, 'order_id' );
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function takeCareUser(){
        return $this->belongsTo(User::class, 'take_care_id', 'id');
    }

    public function orderServices(){
        return $this->hasMany(OrdersService::class, 'order_id', 'id');
    }
     public function orderIncurred(){
        return $this->hasMany(OrderIncurred::class, 'order_id', 'id');
    }

}
