<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'order_detail';

    protected $fillable = [
        'order_service_id',
        'date',
        'name',
        'shift',
        'time'
    ];
    protected $casts = [
        'date' => 'datetime',
    ];

    public $timestamps = true;

    public function order_service(){
        return $this->belongsTo(OrdersService::class, 'order_service_id', 'id');
    }
}
