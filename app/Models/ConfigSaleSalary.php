<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfigSaleSalary extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'config_sale_salary';

    protected $fillable = [
        'id',
        'user_id',
        'basic_salary',
        'percentage_of_sales',
        'takecare',
        'sale_seasonal',
        'combo',
        'service',
        'cate_path',
        'bonus_money',
        'type',
        'excu_date',
        'allowance_lunch',
        'allowance_telephone'
    ];


    protected $casts =[
        'basic_salary'=>'float'
    ];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
