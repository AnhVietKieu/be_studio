<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'roles';

    protected $fillable = ['group', 'name', 'permissions'];

    public $timestamps = true;

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
