<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incurred extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'incurred';

    protected $fillable = ['user_id', 'name', 'description'];

    public $timestamps = true;

    public function Incurred()
    {
        return $this->hasmany(Incurred::class);
    }
}
