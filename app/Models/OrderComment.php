<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderComment extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'order_comment';

    protected $fillable = [
        'user_id',
        'order_id',
        'customer_id',
        'content_detail',
        'created_at',
    ];

    public $timestamps = true;

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function order(){
        return $this->belongsTo(Orders::class, 'order_id', 'id');
    }
}
