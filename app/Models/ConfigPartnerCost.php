<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfigPartnerCost extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'config_partner_cost';

    protected $fillable = [
        'id',
        'combo_id',
        'money_of_combo',
    ];

    public $timestamps = true;

    public function combo()
    {
        return $this->belongsTo(Combo::class,'combo_id','id');
    }

    public function group(){
        return $this->belongsTo(GroupConfigPartnerCost::class,'group_config_id','id');
    }
}
