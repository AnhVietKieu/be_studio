<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersService extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'order_service';

    protected $fillable = [
        'order_id',
        'combo_id',
        'service_ids',
        'add_services',
        'total_price',
        'sale_off',
        'combo_category_id',
    ];

    public $timestamps = true;

    public function order(){
        return $this->belongsTo(Orders::class, 'order_id', 'id');
    }

    public function combo(){
        return $this->belongsTo(Combo::class, 'combo_id', 'id');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'combo_category_id', 'id');
    }

    public function order_details(){
        return $this->hasMany(OrderDetail::class, 'order_service_id', 'id');
    }
}
