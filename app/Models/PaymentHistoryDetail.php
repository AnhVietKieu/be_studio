<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentHistoryDetail extends Model
{
    use SoftDeletes;
    protected $table = 'payment_history_detail';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'order_id',
        'payment_money',
        'date_payment',
        'note',
        'code_bank'
    ];

    public $timestamps = true;

    public function createBy(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
