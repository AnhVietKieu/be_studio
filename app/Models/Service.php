<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'service';

    protected $fillable = [
        'user_id',
        'name',
        'code',
        'category_id',
        'cost_price',
        'sale_price',
        'minus_price',
        'description',
        'department_id'
    ];

    public $timestamps = true;

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function department(){
        return $this->belongsTo(Department::class,'department_id');
    }

}
