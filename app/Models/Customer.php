<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $table = 'customer';

    protected $fillable = [
	    'user_id',
	    'name',
        'code',
        'address',
        'mobile_phone',
        'email_address',
        'tax_code',
        'bank_code',
        'description',
        'field_work',
        'married_date',
        'childrens',
        'date_of_birth',
        'contact_name',
        'contact_mobile_phone',
        'contact_email',
        'contact_address',
        'level',
        'CMT',
        'contact_CMT',
        'source'
    ];

    public $timestamps = true;

}
