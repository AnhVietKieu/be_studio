<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'category';

    protected $fillable = ['user_id', 'name', 'type', 'description','create_at','photographer_salary_field_config','enable_for_photographer_salary_config'];

    protected $casts=[
        'photographer_salary_field_config'=>'json'
    ];

	public $timestamps = true;

    public function combos(){
        return $this->hasMany(Combo::class, 'category_id', 'id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class,'user_id','id');
    }

    public function services()
    {
        return $this->hasMany(Service::class,'category_id', 'id');
    }
}
