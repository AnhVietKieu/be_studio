<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderHistoryPayment extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'order_history_payment';

    protected $fillable = [
        'user_id',
        'order_id',
        'payment_expected',
        'payment_money',
        'payment_date_plan',
        'payment_date_expected',
        'note',
    ];

    public $timestamps = true;
}
