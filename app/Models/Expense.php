<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expense extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'expense';

    protected $fillable = [
        'expense_code',
        'expense_type',
        'reason',
        'date_excute',
        'date_accounting',
        'money',
        'person_charge'
    ];

    public $timestamps = true;
    public function user(){
        return $this->belongsTo(User::class, 'person_charge', 'id');
    }

}
