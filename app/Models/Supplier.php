<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $table = 'supplier';

    protected $fillable = [
        'user_id',
        'name',
        'code',
        'address',
        'mobile_phone',
        'email_address',
        'tax_code',
        'bank_code',
        'description',
        'field_work'
    ];

    public $timestamps = true;
}
