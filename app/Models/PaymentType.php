<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentType extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'payment_type';

    protected $fillable = [
        'user_id',
        'parent_id',
        'name',
        'money',
        'payment_type',
        'payment_expiry',
        'payment_expiry_type',
        'payment_expiry_by',
    ];

    public $timestamps = true;
}
