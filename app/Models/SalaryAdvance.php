<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryAdvance extends Model
{
    protected $fillable=[
      "user_id",
        "partner_id",
        "amount",
        "action",
        "action_name",
        "note"
    ];
    //

    public function employee(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class,'partner_id');
    }
}
