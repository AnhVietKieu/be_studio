<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\OrderJob;

class PayRoll extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'payroll';


    protected $fillable = [
        'id',
        'user_id',
        'supplie_id',
        'department_id',
        'basic_salary',
        'work_day',
        'allowance',
        'start_date',
        'end_date',
        'note',
        'total',
        'status',
        'tax_salary',
        'config_id'
    ];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class,'supplie_id','id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class,'department_id','id');
    }

    static function revenue($start_date,$end_date,$user_id='',$author_id='',$supplier_id='',$department_id)
    {
        $revenue =0;
        $revenue_contract =0;
        $work_day = 0;

        $arr =[];
        $order_job =OrderJob::where('department_id',$department_id)->where('status','4')
            ->where('start_date','>=',$start_date)
            ->where('end_date','<',$end_date);

        $name_department = Department::find($department_id);

        switch ($name_department['name']){
            case"PHOTOSHOP";
                $order_photoshop =$order_job->where('recipient_id',$user_id)->where('user_author_id',$author_id)->get();

                $arr = array(
                    'work_day'=>isset($order_photoshop)?count($order_photoshop):'0'
                );
                break;
            case 'PHOTOGRAPHER';

            $order_photographer = $order_job->where('supplier_id',$supplier_id)->get();
            $arr = array(
                'work_day'=>isset($order_photographer)?count($order_photographer):'0'
            );
                break;
            default:
                return $arr=array(
                    'revenue'=>0,
                    'work_day'=>0
                ) ;

        }


    }

    static function revenue_sale($start_date,$end_date,$user_id,$customer_id,$type)
    {
        $total = 0;
        $order =Orders::where('status','1')
            ->where('created_at','>=',$start_date)
            ->where('created_at','<',$end_date);
        if(isset($order))
        {
            if($type =='CONTRACT')
            {
                if($user_id == $customer_id)
                {
                    $order_sale_contract = $order->where('user_id',$user_id)->where('customer_id',$customer_id)->get();

                    foreach ($order_sale_contract as $key)
                    {
                        $total+=$key['total_price'];
                    }

                    return array(
                        'revenue' =>$total,
                        'work_day' =>count($order_sale_contract)
                     );
                }
            }
            if($type=='NORMAL')
            {
                $order_sale = $order->where('customer_id',$customer_id)->where('user_id','!=',$user_id)->get();

                foreach ($order_sale as $key)
                {
                    $total+=$key['total_price'];
                }

                return array(
                    'revenue' =>$total,
                    'work_day' =>count($order_sale)
                );
            }
        }else{
            return array(
                'revenue' =>0,
                'work_day' =>0
            );
        }


    }

    static function payroll_arr($user_id,$supplie_id,$department_id,$basic_salary,$work_day,$allowance,$start_date,$end_date,$note,$total,$tax_salary)
    {

        $arr = array(
            'user_id'=>$user_id,
            'supplie_id'=>$supplie_id,
            'department_id'=>$department_id,
            'basic_salary'=>$basic_salary,
            'work_day'=>$work_day,
            'allowance'=>$allowance,
            'start_date'=>$start_date,
            'end_date'=>$end_date,
            'note'=>$note,
            'total'=>$total,
            'tax_salary'=>$tax_salary
        );

        return $arr;

    }

}
