<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderJob extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'order_job';

    protected $fillable = [
        'order_id',
        'combo_id',
        'service_id',
        'order_detail_id',
        'department_id',
        'user_author_id',
        'supplier_id',
        'name_job',
        'location',
        'start_date_plan',
        'end_date_plan',
        'start_date',
        'end_date',
        'status',
        'url_file',
        'link',
        'process',
        'recipient_id',
        'confirm_date'
    ];

    public $timestamps = true;

    public function user_author(){
        return $this->belongsTo(User::class, 'user_author_id', 'id');
    }

    public function recipient(){
        return $this->belongsTo(User::class, 'recipient_id', 'id');
    }

    public function order_detail(){
        return $this->belongsTo(OrderDetail::class, 'order_detail_id', 'id');
    }

    public function order(){
        return $this->belongsTo(Orders::class, 'order_id', 'id');
    }

    public function combo(){
        return $this->belongsTo(Combo::class, 'combo_id', 'id');
    }

    public function service(){
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id', 'id');
    }
}
