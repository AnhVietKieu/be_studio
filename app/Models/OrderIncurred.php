<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderIncurred extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'order_incurred';

    protected $fillable = ['incurred_id', 'order_id', 'date','create_by', 'payer', 'money', 'note','created_at'];

    public $timestamps = true;

    public function createBy(){
            return $this->belongsTo(User::class, 'create_by', 'id');
    }
     public function Payer(){
            return $this->belongsTo(User::class, 'payer', 'id');
    } 
    public function Orders(){
            return $this->belongsTo(Orders::class, 'order_id', 'id');
     }
     public function Incurred(){
            return $this->belongsTo(Incurred::class, 'incurred_id', 'id');
     }
    }
