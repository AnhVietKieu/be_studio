<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComboService extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $table = 'combo_service';

    protected $fillable = ['service_id', 'combo_id', 'number'];

    public $timestamps = true;

    public function service(){
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }
}
