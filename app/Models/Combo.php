<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Combo extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'combo';

    protected $fillable = [
        'user_id',
        'name',
        'code',
        'category_id',
        'price',
        'time_limit',
        'description',
        'time_shift',
        'max_sale',
        'cost_price'
    ];

    public $timestamps = true;

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function services(){
        return $this->belongsToMany(Service::class,'combo_service')
            ->withPivot('number');
    }
}
