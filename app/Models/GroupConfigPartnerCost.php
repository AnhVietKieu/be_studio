<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupConfigPartnerCost extends Model
{
    protected $fillable=[
        'partner_id',
        'excu_date',
        'basic_salary',
        'incurred_costs'
    ];

    protected $casts=[
        'incurred_costs'=>'array'
    ];

    public function ConfigPartnerCost(){
        return $this->hasMany(ConfigPartnerCost::class,'group_config_id','id');
    }
}
