<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    const UNIT = 0.5;

    public $filterBuilder = false;
    /**
     * The Model name.
     *
     * @var \Illuminate\Database\Eloquent\Model;
     */
    protected $model;

    /**
     * Paginate the given query.
     *
     * @param int $n The number of models to return for pagination
     *
     * @return mixed
     */
    public function getPaginate($n)
    {
        return $this->model->paginate($n);
    }

    /**
     * Create a new model and return the instance.
     *
     * @param array $inputs
     *
     * @return Model instance
     */
    public function store(array $inputs)
    {
        return $this->model->create($inputs);
    }

    /**
     * @return bool
     */
    public function isFilterBuilder(): bool
    {
        return $this->filterBuilder;
    }

    /**
     * delete multi rows
     *
     * @param array|int $ids
     * @param int $dem
     * @return bool|int
     */
    public function delete($ids){
        if(is_array($ids)){
            return $this->model->whereIn('id', $ids)->delete();
        }

        $this->destroy($ids);
    }

    /**
     * FindOrFail Model and return the instance.
     *
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Update the model in the database.
     *
     * @param int $id
     * @param array $inputs
     * @return bool|integer
     */
    public function update($id, array $inputs)
    {
        return $this->model->where('id', $id)->update($inputs);
    }

    /**
     * Delete the model from the database.
     *
     * @param int $id
     * @return boolean
     * @throws \Exception
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * @param string|array $fields
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function getAll($fields)
    {
        return $this->model->all($fields);
    }

    /**
     * @param $array
     * @param $arrayUnset
     * @return mixed
     */
    public function unsetValue($array, $arrayUnset)
    {
        foreach ($array as $key => $value){
            foreach ($arrayUnset as $k => $v){
                unset($array[$key][$v]);
            }
        }

        return $array;
    }

    /**
     * @param array $conditions
     * @param int $limit
     * @return mixed
     */
    public function paginateCustom($conditions, $limit = 20)
    {
        if (!empty($conditions['filter'])) {
            $this->filterBuilder = $this->whereBuilder($conditions['filter']);
        }

        if(!empty($conditions['search'])){
            $this->filterBuilder = $this->whereSearch($conditions['search']);
        }


        if(!empty($conditions['where'])){
            $this->filterBuilder = $this->whereType($conditions['where']);
        }


        if (!empty($conditions['sort'])) {
            $this->orderBuilder($conditions['sort']);
        }

        if (isset($conditions['per_page'])) {
            $limit = $conditions['per_page'];
        }

        if (isset($conditions['groupBy'])) {
            $this->filterBuilder = $this->groupByBuilder($conditions['groupBy']);
        }

        return $this->filterBuilder->paginate(
            $limit,
            $conditions['fields'],
            isset($conditions['namePages']) ? $conditions['namePages'] : "page");
    }

    public function whereType($conditions)
    {
        if (!$this->filterBuilder) {
            $this->filterBuilder = $this->model;
        }
        if (empty($conditions)) {
            return $this->filterBuilder;
        }
        foreach($conditions as $key => $value)
        {
            $this->filterBuilder = $this->filterBuilder->where($key,$value);
        }

       return $this->filterBuilder;


    }

    /**
     * @param array $conditions
     * @return mixed
     */
    public function queryWithoutPaginate($conditions)
    {
        if (!empty($conditions['filter'])) {
            $this->filterBuilder = $this->whereBuilder($conditions['filter']);
        }

        if (!empty($conditions['sort'])) {
            $this->orderBuilder($conditions['sort']);
        }

        if (isset($conditions['groupBy'])) {
            $this->filterBuilder = $this->groupByBuilder($conditions['groupBy']);
        }

        return $this->filterBuilder->select($conditions['fields'])->get();
    }

    /**
    */
    /**
     * @param array $conditions
     * @return bool
     */
    public function whereBuilder($conditions)
    {
        if (!$this->filterBuilder) {
            $this->filterBuilder = $this->model;
        }
        if (empty($conditions)) {
            return $this->filterBuilder;
        }

        foreach ($conditions as $k => $v) {
            if ($k == 'LIKE') {
                foreach ($v  as $column =>$value){
                    $a = $column;
                    $b = $value;
                    if($b !=''){
                        $this->filterBuilder = $this->filterBuilder->where(function ($query) use ($a, $b){
                            $query->orWhere($a, 'like', '%' . $b . '%');
                        });
                    }

                }
                        continue;
            }

            if($k == 'TYPE')
            {
                foreach ($v  as $column =>$value){
                    $a = $column;
                    $b = $value;
                    if($b !=''){
                        $this->filterBuilder = $this->filterBuilder->where(function ($query) use ($a, $b){
                            $query->orWhere($a,$b);

                        });
                    }

                }
                continue;
            }
//                $a = $v['column'];
//                $b = $v['value'];
//                    $this->filterBuilder = $this->filterBuilder->where(function ($query) use ($a, $b){
//                        foreach ($a as $key => $col) {
//                            $query->orWhere($col, 'like', '%' . $b . '%');
//                        }
//                    });
//                continue;

            if ($k == 'EXACT') {
                $this->filterBuilder = $this->filterBuilder->where(function ($query) use ($v){
                    foreach ($v as $key => $value) {
                        if(!empty($value)) {
                            $query->WhereIn($key, $value);
                        }
                    }
                });
                continue;
            }

        }

        return $this->filterBuilder;
    }

    /**
     * @param array $conditions
     * @return bool
     */
    public function groupByBuilder($conditions)
    {
        if (!$this->filterBuilder) {
            $this->filterBuilder = $this->model;
        }
        if (empty($conditions)) {
            return $this->filterBuilder;
        }
        foreach ($conditions as $k => $v) {
            $this->filterBuilder = $this->filterBuilder->groupBy($v);
        }

        $this->filterBuilder = $this->filterBuilder->orderBy('date_created');

        return $this->filterBuilder;
    }

    /**
     * @param array $sorts
     * @return bool
     */
    public function orderBuilder($sorts)
    {
        if (!empty($sorts)) {
            foreach ($sorts as $k => $v) {
                $this->filterBuilder = $this->filterBuilder->orderBy($k, $v);
            }
        }

        return $this->filterBuilder;
    }

    public function pluckModel($conditions, $key, $val)
    {
        if (!empty($conditions['filter'])) {
            $this->filterBuilder = $this->whereBuilder($conditions['filter']);
        }

        if (!empty($conditions['sort'])) {
            $this->orderBuilder($conditions['sort']);
        }

        if (isset($conditions['per_page'])) {
            $limit = $conditions['per_page'];
        }

        if (isset($conditions['groupBy'])) {
            $this->filterBuilder = $this->groupByBuilder($conditions['groupBy']);
        }

        return $this->filterBuilder->pluck($key, $val);
    }

    protected function returnErrors($message)
    {
        return [
            'error' => true,
            'messages' => $message,
        ];
    }

    protected function getOther($postId = NULL)
    {
        $posts = $this->model->find($postId);
        $list = $this->model->where('ID', '!=', $postId)
            ->where('created_by_id', $posts->created_by_id)
            ->orderBy('date_created', 'DESC')
            ->get();

        return $list;
    }

}
