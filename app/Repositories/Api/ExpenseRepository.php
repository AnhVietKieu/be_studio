<?php
namespace App\Repositories\Api;

use App\Helpers\Helpers;
use App\Models\Expense;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;


class ExpenseRepository extends BaseRepository
{
    /**
     * The Model name.
     *
     * @var Expense;
     */
    public function __construct(Expense $model)
    {
        $this->model = $model;
    }

    public function getList($request){
        $conditions['fields'] = [
            'id',
            'expense_code',
            'expense_type',
            'date_excute',
            'date_accounting',
            'reason',
            'money',
            'person_charge'
        ];
        $conditions['sort'] = [
            'id' => 'desc',
        ];
        $per_page = $request->per_page;
        if(!isset($request->person_charge)){
            $this->filterBuilder = $this->model
                ->with(['user' => function ($query) {
                    $query->select('id', 'name');
                }]);
        }


        return $this->paginateCustom($conditions, $per_page);
    }

    public function saveData(Request $request){
        $data = $request->all();
        $data['expense_code'] = $this->getSrtCode();
        $expense = $this->model;
        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel
            $expense->fill($data)->save();
            DB::commit();
            return $expense;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    /**
     * tạo mã chi tieu
     * @return string
     */
    private function getSrtCode()
    {
        $date = Carbon::now();

        $expense = $this->model
            ->select('expense_code')
            ->withTrashed()
            ->orderBy('id', 'DESC')
            ->first();

        $cnt = 1;
        if($expense){
            $cnt = substr(
                $expense->expense_code,
                strpos($expense->expense_code, "_")+1);
            $cnt += 1;
        }

        $code = "PC";
        $code .= $date->format("m/y");
        $code .= "_";
        $code .= Helpers::padLeft($cnt, 4, '0');
        return $code;
    }

    public  function updateData($id , $request){
        $data = $request->all();
        $expense = $this->model->find($id);
        DB::beginTransaction();
        try {
            //update vào bảng category bbằng câu lệnh update của laravel
            $expense->fill($data)->update();
            DB::commit();
            return $expense;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
        }
    }

    public  function deleteExpense($ids){
        DB::beginTransaction();
        try {
            $delete = $this->delete($ids);
            DB::commit();
            return $delete;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return false;
        }

    }

    public function getDetail($request, $id)
    {
        $fields = [
            'id',
            'expense_code',
            'expense_type',
            'date_excute',
            'date_accounting',
            'reason',
            'money',
            'person_charge'
        ];
        $expense = $this->model->select($fields)
            ->from('expense')
            ->with(['user' => function ($query) {
                $query->select('id', 'name');
            }])
            ->where('id', $id)->first();

        return $expense;
    }

    public function getCostAllMonth()
    {
        $data = $this->model->select('created_at', 'money')->whereYear('created_at', '=', date('Y'));

        return $data->get()
            ->groupBy(function ($expense) {
                return $expense->created_at->format('m/Y');
            });
    }

}
