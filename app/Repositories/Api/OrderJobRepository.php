<?php

namespace App\Repositories\Api;

use App\Models\OrderDetail;
use App\Models\OrderJob;
use App\Models\Orders;
use App\Models\Service;
use App\Models\User;
use App\Models\ComboService;
use App\Models\OrdersService;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;
use App\Helpers\Helpers;
use Illuminate\Http\Request;

class OrderJobRepository extends BaseRepository
{
    //Department ID map to database,
    const department = [
        'SALE'=>2,
        'PHOTOGRAPHER' => 3,
        'MAKEUP' => 4,
        'PHOTOSHOP' => 5,
        'IN' => 6
    ];

    public function __construct(OrderJob $model)
    {
        $this->model = $model;
    }

    // danh sách công việc trong tab danh sach
    public function getListJob(Request $request)
    {
        $array_data = [];
        $date_now = Carbon::now();
        $per_page = 15;
        $conditions['sort'] = [
            'id' => 'desc',
        ];
        $conditions['fields'] = [
            'id',
            'order_id',
            'combo_id',
            'supplier_id',
            'service_id',
            'recipient_id',
            'user_author_id',
            'order_detail_id',
            'name_job',
            'department_id',
            'start_date_plan',
            'end_date_plan',
            'end_date',
            'process',
            'status'
        ];

//        $checkRole = Auth::user()->authorizeRoles(['ASSIGN_JOB']);
        $this->filterBuilder = $this->model
            ->with(['order' => function ($query) {
                $query->select('id', 'code', 'customer_id')->with(['customer' => function ($q2) {
                    $q2->select('id', 'name');
                }]);
            }])
            ->with(['order_detail' => function ($query) {
                $query->select('id', 'date');
            }])
            ->with(['combo' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['user_author' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['service' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['supplier' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['recipient' => function ($query) {
                $query->select('id', 'name');
            }]);

        $data = $this->paginateCustom($conditions, $per_page)->toArray();
        foreach ($data['data'] as $key => $value) {
            $data['data'][$key]['user_author_name'] = $value['user_author']['name'] ?? '';
            $data['data'][$key]['order_code'] = $value['order']['code'] ?? '';
            $data['data'][$key]['customer_name'] = $value['order']['customer']['name'] ?? '';
            $data['data'][$key]['order_date'] = $value['order_detail']['date'] ?? '';
            $data['data'][$key]['combo_name'] = $value['combo']['name'] ?? '';
            $data['data'][$key]['service_name'] = $value['service']['name'] ?? '';
            $data['data'][$key]['supplier_name'] = $value['supplier']['name'] ?? '';
            $data['data'][$key]['recipient_name'] = $value['recipient']['name'] ?? '';
        }
        $data['data'] = $this->unsetValue($data['data'], [
            'order_id', 'combo_id', 'service_id', 'department_id', 'user_author', 'supplier_id', 'recipient_id',
            'order', 'order_detail', 'combo', 'service', 'order_detail_id', 'supplier', 'recipient']);
        $data['data'] = collect($data['data'])->groupBy('order_date');
        $data['data'] = array_values($data['data']->toArray());

        foreach ($data['data'] as $key => $value) {
            foreach ($value as $k => $v) {
                $checkEndDate = $date_now->diffInDays(Carbon::create($v['end_date_plan']), false);
                if ($checkEndDate < 0) {
                    if ($v['process'] < 100) {
                        $value[$k]['color'] = 'error';
                    } else {
                        $value[$k]['color'] = 'success';
                    }
                } elseif ($checkEndDate <= 2) {
                    if ($v['process'] < 100) {
                        $value[$k]['color'] = 'warning';
                    } else {
                        $value[$k]['color'] = 'success';
                    }
                } else {
                    $value[$k]['color'] = 'success';
                }
            }
            $array_data[$key]['header']['customer_name'] = $value[0]['customer_name'];
            $array_data[$key]['header']['order_code'] = $value[0]['order_code'];
            $array_data[$key]['header']['order_date'] = $value[0]['order_date'];
            $array_data[$key]['body'] = $value;
        }

        $data['data'] = $array_data;

        return $data;
    }

    public function getListJobOfMonth($month)
    {
        $query = $this->model
            ->with(['order', 'recipient', 'supplier','service'])
            ->where(Db::raw('MONTH(start_date_plan)'), '=', $month)
            ->whereHas('order', function($q){
                //get order job with order has payment
                $q->where('status', '=', 1);
            })
            ->get();
        return $query;
    }

    // xem chi tiết công việc
    public function detailJob($id)
    {
        $detaiJob = $this->model->select(
            'id',
            'name_job',
            'user_author_id',
            'recipient_id',
            'location',
            'start_date_plan',
            'end_date_plan',
            'status',
            'url_file',
            'link',
            'process',
            'supplier_id',
            'confirm_date'
        )
            ->with(['user_author' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['supplier' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['recipient' => function ($query) {
                $query->select('id', 'name');
            }])
            ->find($id);
        if ($detaiJob == null) return false;
        $detaiJob->user_author_name = $detaiJob->user_author->name ?? '';
        if (!empty($detaiJob->supplier)) {
            $detaiJob->supplier_name = $detaiJob->supplier->name ?? '';
        }
        if (!empty($detaiJob->recipient)) {
            $detaiJob->recipient_name = $detaiJob->recipient->name ?? '';
        }
        if (!empty($detaiJob['confirm_date'])) {
            $today = Carbon::now();
            $updated_at = Carbon::create($detaiJob['confirm_date']);
            $check_ten_minute = $updated_at->diffInMinutes($today, false);
            if ($check_ten_minute > 10) {
                $detaiJob['check_time_update'] = 1;
            }
        }
        $detaiJob = collect($detaiJob)->except([
            'recipient',
            'supplier',
            'user_author',
        ]);

        return $detaiJob;
    }

    // tạo mới công việc theo order
    public static function saveData($id)
    {
        $order = OrdersService::select(
            'id',
            'order_id',
            'combo_id',
            'service_ids',
            'add_services'
        )->where('order_id', $id)->get()->toArray();
        DB::beginTransaction();
        try {
            foreach ($order as $key => $value) {
                //thông tin service
                $all_services = [];
                if ($value['combo_id']) {
                    //get all service id and number in combo
                    $query=ComboService::with('service')
                        ->select('service_id','number')
                        ->where('combo_id', $value['combo_id'])
                        ->whereIn('service_id', explode(',', $value['service_ids']))
                        ->where('service_id','!=', 2) //ignore Đồ thường service
                        ->get();
                    $all_services=$query->map(function($item){
                            $all_services['service_id']=$item->service_id;
                            $all_services['service_name']=$item->service->name;
                            $all_services['department_id']=$item->service->department_id;
                            $all_services['number']=$item->number;
                            return $all_services;
                        });
                }
                if ($value['add_services']) {
                    //get all retail service
                    $extend_service=[];
                    $serviceIds = json_decode($value['add_services'], true);
                    $query = Service::whereIn('id',$serviceIds)
                        ->where('service_id','!=', 2) //ignore Đồ thường service
                        ->get();
                    $extend_service=$query->map(function($item){
                        $extend_service['service_id']=$item->id;
                        $extend_service['service_name']=$item->name;
                        $extend_service['department_id']=$item->department_id;
                        $extend_service['number']=1; //default 1
                        return $extend_service;
                    });

                    //merge
                    $all_services=array_merge($all_services,$extend_service);
                }

                //danh sách ngày chụp của combo
                $comboDate = OrderDetail::select('id', 'date', 'shift', 'time')
                    ->where('order_service_id', $value['id'])
                    ->get();
                $checkDate = $comboDate->count();
                //kiểm tra xem có ngày chụp chưa
                if ($checkDate != 0) {
                    $orderJob['order_id'] = $value['order_id'];
                    $orderJob['combo_id'] = $value['combo_id'];
                    $orderJob['user_author_id'] = Auth::id();
                    //for ngày chụp
                    foreach ($comboDate as $k => $v) {
                        $orderJob['order_detail_id'] = $v['id'];
                        $arrayJobGenerate = [];
                        $existsPhotoshopJob=false; //check if exists job for photoshoper

                        if ($v['shift']) {
                            $shiftOrTime = json_decode($v['shift'], 1);
                        } elseif ($v['time']) {
                            $shiftOrTime = json_decode($v['time'], 1);
                        }

                        //for các dịch vụ của combo trong ngày chụp
                        foreach ($all_services as $key => $value) {
                            //check exists job for Photoshoper
                            if($value['department_id']==self::department['PHOTOSHOP'])
                                $existsPhotoshopJob=true;

                            //generate job follow by department id
                                $jobList=self::generateOrderJobFollowTimeline(
                                    $value['department_id'],
                                    $orderJob,
                                    $value,
                                    $v
                                );
                                //merge to arrayjob
                            $arrayJobGenerate=array_merge($arrayJobGenerate,$jobList);
                        }

                        //if exists job for photoshoper'll generate extend job for photoshop
                        //example: send raw file job, confirm job, hand over job, design ....etc
                        if($existsPhotoshopJob){
                            $extendJob=self::generateOrderJobExtendOfPhotoshop(
                                $orderJob,
                                $value,
                                $v);
                            $arrayJobGenerate=array_merge($arrayJobGenerate,$extendJob);

                        }

                        //delete previous job if not yet process
                        OrderJob::where('order_id', $orderJob['order_id'])->delete();

                        //insert database
                        foreach ($arrayJobGenerate as $key => $value) {
                            $newJob = app(OrderJob::class);
                            $newJob->fill($value)->save();
                        }
                        DB::commit();
                        return true;
                    }
                } else {
                    return
                        [
                            'messages' => 'Chưa đặt ngày chụp, không thể giao việc tự động!',
                        ];
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
        }

        return true;
    }

    // lấy sô lượng công việc theo status
    public function getListNumber(Request $request)
    {
        $departmentUser = Auth::user()->department_id;
        if ($departmentUser == 1 || $departmentUser == 2) {
            $all = OrderJob::count();
            $confirm = OrderJob::where('status', 0)->count();
            $agree = OrderJob::where('status', 1)->count();
            $refuse = OrderJob::where('status', 2)->count();
            $processing = OrderJob::where('status', 3)->count();
            $finish = OrderJob::where('status', 4)->count();

            $array = array(
                '0' => [
                    "id" => 5,
                    "name" => 'Tất cả',
                    "number" => $all
                ],
                '1' => [
                    "id" => 0,
                    "name" => 'Chờ xác nhận',
                    "number" => $confirm
                ],
                '2' => [
                    "id" => 1,
                    "name" => 'Đồng ý',
                    "number" => $agree
                ],
                '3' => [
                    "id" => 2,
                    "name" => 'Từ chối',
                    "number" => $refuse
                ],
                '4' => [
                    "id" => 3,
                    "name" => 'Đang xử lí',
                    "number" => $processing
                ],
                '5' => [
                    "id" => 4,
                    "name" => 'Kết thúc',
                    "number" => $finish
                ],
            );
            return $array;
        } else {

            $result = OrderJob::where([
                ['user_id', '=', Auth::id()],
                ['status', '!=', 2],
            ])->get();

            $agree = 0;
            $confirm = 0;
            $processing = 0;
            $finish = 0;
            foreach ($result as $value) {
                if ($value['status'] == 0) {
                    $confirm += 1;
                }
                if ($value['status'] == 1) {
                    $agree += 1;
                }
                if ($value['status'] == 3) {
                    $processing += 1;
                }
                if ($value['status'] == 4) {
                    $finish += 1;
                }
            }
            $array = array(
                '0' => [
                    "id" => 5,
                    "name" => 'Tất cả',
                    "number" => $agree + $confirm + $processing + $finish
                ],
                '1' => [
                    "id" => 0,
                    "name" => 'Chờ xác nhận',
                    "number" => $confirm
                ],
                '2' => [
                    "id" => 1,
                    "name" => 'Đồng ý',
                    "number" => $agree
                ],
                '3' => [
                    "id" => 3,
                    "name" => 'Đang xử lí',
                    "number" => $processing
                ],
                '4' => [
                    "id" => 4,
                    "name" => 'Kết thúc',
                    "number" => $finish
                ],

            );

            return $array;
        }

    }

    // update từng công việc
    public function updateData($request)
    {
        $checkRole = Auth::user()->authorizeRoles(['ASSIGN_JOB']);
        $today = Carbon::now();
        $data = $request->all();
        $order_job = $this->model->find($request->id);
        DB::beginTransaction();
        try {
            // người nhận việc update công việc của mình
            if ($checkRole) {
                // nhân viên update tiến độ = 100 thì status chuyển sang kết thúc và lưu tgian kết thúc thực tế
                if (!empty($data['process'])) {
                    if ($data['process'] == 100) {
                        try {
                            $order_job['status'] = 4;
                            $order_job['end_date'] = $today;
                            $order_job->fill($order_job->toArray())->update();
                            $this->updateStatusOrder($order_job['order_id']);
                        } catch (\Exception $e) {
                            Log::info($e->getMessage());
                            DB::rollback();
                            return [
                                'error' => true,
                                'messages' => 'Cập nhật tiến độ thất bại',
                            ];
                        }
                    } else {
                        try {
                            $order_job['process'] = $data['process'];
                            $order_job->fill($order_job->toArray())->update();
                        } catch (\Exception $e) {
                            Log::info($e->getMessage());
                            DB::rollback();
                            return [
                                'error' => true,
                                'messages' => 'Cập nhật tiến độ thất bại',
                            ];
                        }
                    }
                }
                // nhân viên nhận việc thì cập nhật thời gian bắt đầu thực tế
                if (!empty($data['status'])) {
                    if (!empty($order_job['recipient_id']) || !empty($order_job['supplier_id'])) {
                        if ($data['status'] == 1 || $data['status'] == 2) {
                            if (!empty($order_job['confirm_date'])) {
                                $updated_at = Carbon::create($order_job['confirm_date']);
                                $check_ten_minute = $updated_at->diffInMinutes($today, false);
                                if ($check_ten_minute < 10) {
                                    $this->saveStatusJob($data, $order_job, $today);
                                } else {
                                    return [
                                        'error' => true,
                                        'messages' => 'Thời gian cập nhật đã hết hạn',
                                    ];
                                }
                            } else {
                                $this->saveStatusJob($data, $order_job, $today);
                            }
                        }
                    } else {
                        return [
                            'error' => true,
                            'messages' => 'Chưa có người thực hiện hoặc nhà cung cấp nào',
                        ];
                    }

                }
                //cập nhật ảnh chứng minh tin nhắn
                if (!empty($data['url_file'])) {
                    try {
                        $image = $data['url_file'];
                        $fileName = Auth::id() . '_' . time() . '_' . $image->getClientOriginalName();
                        $path = $request->file('url_file')->move(public_path("/image/job"), $fileName);
                        $photoURL = url('/image/job/' . $fileName);
                        if (!empty($photoURL)) {
                            $order_job['url_file'] = $photoURL;
                        }
                        $order_job->fill($order_job->toArray())->update();
                        $this->updateStatusOrder($order_job['order_id']);
                    } catch (\Exception $e) {
                        Log::info($e->getMessage());
                        DB::rollback();
                        return [
                            'error' => true,
                            'messages' => 'Cập nhật ảnh tin nhắn thất bại',
                        ];
                    }
                }
                //cập nhật link ảnh gốc gửi khách
                if (!empty($data['link'])) {
                    try {
                        $order_job['link'] = $data['link'];
                        $order_job->fill($order_job->toArray())->update();
                    } catch (\Exception $e) {
                        Log::info($e->getMessage());
                        DB::rollback();
                        return [
                            'error' => true,
                            'messages' => 'Cập nhật link ảnh gốc thất bại',
                        ];
                    }
                }
            }

            DB::commit();
            return $order_job;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
        }
    }

    public function assign($request)
    {
        $data = $request->all();
        $order_job = $this->model->find($request->id);
        DB::beginTransaction();
        try {
            $order_job->recipient_id = $data['recipient_id'];
            $order_job->supplier_id = $data['supplier_id'];
            $order_job->location = $data['location'];
            $order_job->note = $data['note'];
            $order_job->status = 0;//change status to wait approve
            $order_job->save();
            DB::commit();
            return $order_job;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
        }
    }

    protected function saveStatusJob($data, $order_job, $today)
    {
        if ($data['status'] == 1) {
            try {
                // nhân viên update trạng thái nhận việc
                $order_job['start_date'] = $today;
                $order_job['confirm_date'] = $today;
                $order_job['process'] = 10;
                $order_job['status'] = 1;
                $order_job->fill($order_job->toArray())->update();
                return $order_job;
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                DB::rollback();
                return [
                    'error' => true,
                    'messages' => 'Cập nhật nhận việc - từ chối thất bại',
                ];
            }
        } elseif ($data['status'] == 2) {
            try {
                // nhân viên update trạng thái từ chối
                $order_job['confirm_date'] = $today;
                $order_job['url_file'] = null;
                $order_job['process'] = 0;
                $order_job['status'] = 2;
                $order_job['start_date'] = null;
                $order_job['end_date'] = null;
                $order_job->fill($order_job->toArray())->update();
                return $order_job;
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                DB::rollback();
                return [
                    'error' => true,
                    'messages' => 'Cập nhật nhận việc - từ chối thất bại',
                ];
            }
        }
    }

    // update status order where order job
    public function updateStatusOrder($orderId)
    {
        $fields = [
            'id',
            'user_author_id',
            'status',
        ];
        $order = $this->model->select($fields)
            ->where('order_id', $orderId)
            ->get();
        $checkStatus = true;
        $checkUser = true;
        foreach ($order as $key => $value) {
            !isset($value->user_id) ? $checkUser = false : $checkUser = true;
            $value->status != 4 ? $checkStatus = false : $checkStatus = true;
        }
        if ($checkStatus) {
            Orders::find($orderId)->update(['status' => '3']);
        } elseif ($checkUser) {
            Orders::find($orderId)->update(['status' => '2']);
        }
    }

    public function getJobFollowCurrentUser()
    {
        return $this->model
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->with(['user_author' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['service' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['supplier' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['recipient' => function ($query) {
                $query->select('id', 'name');
            }])
            ->get();
    }

    public function validateInput($action, $request)
    {
        $checkRole = Auth::user()->authorizeRoles(['ASSIGN_JOB']);
        $errors = [
            'error' => false,
            'messages' => ''
        ];
        switch ($action) {
            case 'assign':
                $order_job = $this->model->find($request->id);
                //check exists
                if (!$order_job)
                    $errors = $this->makeError('Không tìm thấy bản ghi');

                //check status can assign is [2,5]
                //2: reject - 5:new
                $arrStatusCanAssign = [2, 5];
                if (!in_array($order_job->status, $arrStatusCanAssign))
                    $errors = $this->makeError('Công việc đang ở trạng thái không thể thay đổi');

                //check employee and supplier
                if ($request->recipient_id == null && $request->supplier_id == null)
                    $errors = $this->makeError('Không có thông tin người nhận việc');
                break;
        }
        return $errors;
    }

    /**
     * return list order job follow by timeline
     * @param $departmentId
     * @param $orderInfo
     * @param $serviceInfo
     * @param $orderDetail
     */
    private static function generateOrderJobFollowTimeline($departmentId,$orderInfo,$serviceInfo,$orderDetail)
    {
        switch ($departmentId) {
            case self::department['MAKEUP']:
            case self::department['SALE']:
                $jobName=$serviceInfo['service_name'];
                $dateAdd=0;
                $job=self::generateJobTemplate($jobName,$dateAdd,$orderInfo,$serviceInfo,$orderDetail);
                return [$job];
                break;
            case self::department['PHOTOGRAPHER']:
                $jobs=[];
                //Step 1: take photo
                $jobName="Chụp ảnh";
                $dateAdd=0;
                $job=self::generateJobTemplate($jobName,$dateAdd,$orderInfo,$serviceInfo,$orderDetail);
                //Create job number correspond to the service number
                for ($i = 0; $i < $serviceInfo['number']; $i++) {
                    array_push($jobs,$job);
                }
                return $jobs;
                break;

            case self::department['PHOTOSHOP']:
                $jobs=[];
                $jobName=$serviceInfo['service_name'];
                $dateAdd=20;
                $job=self::generateJobTemplate($jobName,$dateAdd,$orderInfo,$serviceInfo,$orderDetail);
                array_push($jobs,$job);
                return $jobs;
                break;
            case self::department['IN']:
                $jobName=$serviceInfo['service_name'];
                $dateAdd=21;
                $job=self::generateJobTemplate($jobName,$dateAdd,$orderInfo,$serviceInfo,$orderDetail);
                return $job;
                break;
        }
    }

    /**
     * Generate job Send raw file| Confirm
     * @param $orderInfo
     * @param $serviceInfo
     * @param $orderDetail
     */
    private static function generateOrderJobExtendOfPhotoshop($orderInfo,$serviceInfo,$orderDetail){
        $jobs=[];
        //Step 1: send raw file
        $jobName="Gửi ảnh gốc";
        $dateAdd=2;
        $job=self::generateJobTemplate($jobName,$dateAdd,$orderInfo,$serviceInfo,$orderDetail);
        array_push($jobs,$job);

        //Step 2: confirm
        $jobName="Xác nhận chọn ảnh";
        $dateAdd=5;
        $job=self::generateJobTemplate($jobName,$dateAdd,$orderInfo,$serviceInfo,$orderDetail);
        array_push($jobs,$job);

        //Step 3: design album
        $jobName="Thiết kế Album và In ấn";
        $dateAdd=21;
        $job=self::generateJobTemplate($jobName,$dateAdd,$orderInfo,$serviceInfo,$orderDetail);
        array_push($jobs,$job);

        //Step 4: hand over
        $jobName="Bàn giao";
        $dateAdd=22;
        $job=self::generateJobTemplate($jobName,$dateAdd,$orderInfo,$serviceInfo,$orderDetail);
        array_push($jobs,$job);

        return $jobs;
    }

    /**
     * generate Job follow by template of timeline
     * @param string $jobName
     * @param int $dateAdd
     * @param array $orderInfo
     * @param array $orderService
     * @param array $orderDetail
     * @param string $dateStart
     * @return array
     */
    private static function generateJobTemplate($jobName,$dateAdd,$orderInfo,$orderService,$orderDetail){
        if ($orderDetail['shift']) {
            $shiftOrTime = json_decode($orderDetail['shift'], 1);
        } elseif ($orderDetail['time']) {
            $shiftOrTime = json_decode($orderDetail['time'], 1);
        }
        $orderId=$orderInfo['order_id'];
        $comboId=$orderInfo['combo_id'];
        $authorId=$orderInfo['user_author_id'];
        $orderDetailId=$orderInfo['order_detail_id'];
        $serviceId=$orderService['service_id'];
        $departmentId=$orderService['department_id'];
        $dateStart=$orderDetail['date'];
        $startTime=self::generateTimeString($dateStart,$dateAdd,$shiftOrTime['timeBf']);
        $endTime=self::generateTimeString($dateStart,$dateAdd,$shiftOrTime['timeAt']);
        $job=[
            'order_id'=>$orderId,
            'combo_id'=>$comboId,
            'user_author_id'=>$authorId,
            'order_detail_id'=>$orderDetailId,
            'name_job'=>$jobName,
            'service_id'=>$serviceId,
            'department_id'=>$departmentId,
            'start_date_plan'=>$startTime,
            'end_date_plan'=>$endTime
        ];
        return $job;
    }

    /**
     * return date string format
     * @param $date
     * @param $dateAdd
     * @param $timeAdd
     * @return string
     * @throws \Exception
     */
    private static function generateTimeString($date,$dateAdd,$timeAdd){
       $timeStr= Carbon::parse($date)
            ->addDays($dateAdd)
            ->addHours($timeAdd['HH'])
            ->addMinutes($timeAdd['mm'])
            ->format('Y-m-d H:i:s');
       return $timeStr;
    }

    private function makeError($message)
    {
        return
            [
                'error' => true,
                'messages' => $message
            ];
    }
}
