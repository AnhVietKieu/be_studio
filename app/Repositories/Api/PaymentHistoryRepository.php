<?php

namespace App\Repositories\Api;

use App\Models\Customer;
use App\Models\Orders;
use App\Models\PaymentType;
use App\Models\OrdersService;
use App\Models\PaymentHistoryDetail;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;

class PaymentHistoryRepository extends BaseRepository
{
    public function __construct(PaymentHistoryDetail $model)
    {
        $this->model = $model;
    }

    public function saveData($request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $paymentHistory = new PaymentHistoryDetail();

        DB::beginTransaction();
        try {
            $paymentHistory->fill($data)->save();

            $vip = OrdersService::where('order_id', $request->order_id)
                ->with(['combo' => function ($query) {
                    $query->Where('name', 'like', '%vip%')
                        ->select('id', 'name');
                }])->select('combo_id')->get()->toArray();

            $arrayOrderId = Orders::where('customer_id', $request->customer_id)
                ->get(['id'])
                ->toArray();
            $arrayPaymentDetail = $this->model->whereIn('order_id', $arrayOrderId)
                ->get()
                ->groupBy('order_id')
                ->toArray();
            if (!empty($vip[0]['combo'])) {
                Customer::find($request->customer_id)->update(['level' => 'VIP']);
            } elseif (count($arrayPaymentDetail) >= 3) {
                Customer::find($request->customer_id)->update(['level' => 'VIP']);
            } else {
                Customer::find($request->customer_id)->update(['level' => 'LATCH']);
            }
            // thanh toán lần đầu thì update status order = 1
            $order_update = Orders::find($data['order_id']);
            //tạo mới công việc khi thanh toán
            if ($order_update->status == 0) {
                $result = $order_update->update(['status' => 1]);
                if ($result) {
                    OrderJobRepository::saveData($data['order_id']);
                }
            }

            DB::commit();
            return $paymentHistory;

        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
        }

    }

    public function getHistory($request, $id)
    {
        $paymentHistory = Orders::where('id', $id)->with([
            'orderHistoryPayment' => function ($query) {
                $query->select(
                    'id',
                    'user_id',
                    'order_id',
                    'payment_expected',
                    'payment_money',
                    'payment_date_plan',
                    'payment_date_expected',
                    'note'
                );
            }
        ])
            ->select('id', 'payment_type_id')
            ->get()->toArray();
        $paymentHistoryDetail = $this->model->where('order_id', $id)
            ->sum('payment_money');

        foreach ($paymentHistory as $key => $item) {
            $paymentHistory = $paymentHistory[0]['order_history_payment'];
        }

        $sum = 0;
        foreach ($paymentHistory as $key => $item) {
            if ($paymentHistory[$key]['payment_expected'] > $paymentHistoryDetail) {
                $paymentHistory[$key]['type_payment'] = 0;
            } elseif
            ($paymentHistory[$key]['payment_expected'] <= ($paymentHistoryDetail - $sum)) {
                $paymentHistory[$key]['type_payment'] = 1;
                $sum = $item['payment_expected'];
            }
        }

        return $paymentHistory;
    }

    public function getPaymentByOrder($order_id)
    {
        $fields = [
            'id',
            'user_id',
            'payment_money',
            'date_payment',
            'note',
            'code_bank'
        ];
        $paymentByOrder = $this->model
            ->with(['createBy' => function ($query) {
                $query->select('id', 'name');
            }])
            ->where('order_id', $order_id)->get()->toArray();

        $paymentByOrder = Helpers::addColumnArray($paymentByOrder, 'user_name', '', 'create_by', '');
        return $paymentByOrder;
    }

    public function getPaymentHistory($order_id)
    {
        $paymentTotalMoney = Orders::where('id', $order_id)
            ->select('id', 'total_price', 'payment_type_id')
            ->first();
        $checkPayment = PaymentType::select('payment_type')->where('id', $paymentTotalMoney->payment_type_id)->first();

        $payment = PaymentType::select('name', 'money')
            ->where('payment_type', $checkPayment->payment_type)
            ->where('parent_id', $paymentTotalMoney->payment_type_id)
            ->get();

        if ($checkPayment->payment_type == 0) {
            foreach ($payment as $key => $item) {
                $payment[$key]->money = ($item->money * $paymentTotalMoney->total_price)/100;
            }
        }

        return $payment;
    }

    public function updateData($id, $request)
    {
        $data = $request->all();
        $paymentHistory = $this->model->find($id);
        DB::beginTransaction();
        try {
            //update vào bảng paymenthistory bằng câu lệnh update của laravel
            $paymentHistory->fill($data)->update();
            DB::commit();
            return $paymentHistory;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
            DB::rollback();
        }
    }
    public function deletePayment($ids, $dem = 0)
    {
        if(!empty($ids))
        {
            return $this->delete($ids, $dem);
        }
}
}
