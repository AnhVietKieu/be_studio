<?php

namespace App\Repositories\Api;

use App\Models\Combo;
use App\Models\ConfigSaleSalary;
use App\Models\Service;
use App\Repositories\BaseRepository;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ConfigSaleSalaryRepository extends BaseRepository
{
    const configType = [
        'Saler' => 0,
        'Photoshop' => 1
    ];

    public function __construct(ConfigSaleSalary $model)
    {
        $this->model = $model;
    }

    public function getList($request)
    {
        $conditions['fields'] = [
            'id',
            'user_id',
            'basic_salary',
            'percentage_of_sales',
            'takecare',
            'sale_seasonal',
            'combo',
            'service',
            'cate_path',
            'bonus_money',
            'type',
            'excu_date',
            'allowance_lunch',
            'allowance_telephone'
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];

        if (!empty($request->filters)) {
            $conditions['filter'] = $request->filters;
        }

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }
        if (isset($request->type)) {
            if ($request->type == 1) {
                $conditions['where'] = [
                    'type' => 1
                ];
            } else {
                $conditions['where'] = [
                    'type' => 0
                ];
            }
        }

        $per_page = $request->per_page;

        $this->filterBuilder = $this->model->with(['user' => function ($query) {
            $query->select('name');
        }]);


        $data = $this->paginateCustom($conditions, $per_page);


        foreach ($data as $key => $value) {
            //format data return
            $value->sale_seasonal = json_decode($value->sale_seasonal, true);
            $value->takecare = json_decode($value->takecare, true);
            $value->combo = json_decode($value->combo, true);
            $value->service = json_decode($value->service, true);
            $value->percentage_of_sales = json_decode($value->percentage_of_sales, true);
        }
        return $data;
    }


    public function getDetail($id)
    {
        $fields = [
            'id',
            'user_id',
            'basic_salary',
            'percentage_of_sales',
            'takecare',
            'sale_seasonal',
            'combo',
            'service',
            'cate_path',
            'bonus_money',
            'type',
            'excu_date',
        ];
        $config = $this->model->select($fields)
            ->from('config_sale_salary')
            ->where('id', $id)->first();

        $user = User::find($config->user_id);

        $config['user_id'] = [
            'id' => $user->id,
            'name' => $user->name
        ];


        return $config;
    }

    public function saveData($request)
    {
        $data = $request->all();

        switch ($data['type']) {
            case self::configType['Saler']:
                $data = $this->mapConfigForSaler($data);
                break;
            case self::configType['Photoshop']:
                $data = $this->mapConfigForPhotoshop($data);
                break;
        }
        $config_sale = $this->model;

        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel
            $config_sale->fill($data)->save();
            DB::commit();
            return $config_sale;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            dd($e->getMessage());
            return false;
        }
    }

    public function updateData($request, $id)
    {
        $data = $request->all();

        switch ($data['type']) {
            case self::configType['Saler']:
                $data = $this->mapConfigForSaler($data);
                break;
            case self::configType['Photoshop']:
                $data = $this->mapConfigForPhotoshop($data);
                break;
        }

        $config_sale = $this->model->find($id);

        DB::beginTransaction();
        try {
            //gọi đến hàm save của laravel
            $config_sale->fill($data)->update();

            DB::commit();
            return $config_sale;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    public function deleteData($ids)
    {
        DB::beginTransaction();
        try {

            $delete = $this->delete($ids);

            DB::commit();
            return $delete;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return false;
        }
    }

    public function getConfigFieldForPhotoshop()
    {
        $data = [
            'combo' => $this->getComboForPhotoshop()->toArray(),
            'service' => $this->getServiceForPhotoshop()->toArray()
        ];
        return $data;
    }

    /***
     * return list combo contain at least one service [ảnh or album]
     */
    private function getComboForPhotoshop()
    {
        return Combo::select('id', 'name')
            ->with(['services' => function ($query) {
                $query->select('service.id', 'service.name')
                    ->where('name', 'like', '%' . 'ảnh' . '%')->orWhere('name', 'like', '%' . 'album' . '%');
            }])
            ->whereHas('services', function ($q) {
                $q->where('name', 'like', '%' . 'ảnh' . '%')->orWhere('name', 'like', '%' . 'album' . '%');
            })->get();
    }

    /***
     * return list service contain [ảnh or album]
     */
    private function getServiceForPhotoshop()
    {
        return Service::select('id', 'name')->where('name', 'like', '%' . 'ảnh' . '%')->orWhere('name', 'like', '%' . 'album' . '%')->get();
    }

    private function mapConfigForSaler($data)
    {
        $data['percentage_of_sales'] = json_encode($data['percentage_of_sales']);
        $data['takecare']= json_encode($data['takecare']);
        $data['sale_seasonal'] = json_encode($data['sale_seasonal']);
        $data['bonus_money'] = 0;
        $data['combo'] = '{}';
        $data['service'] = '{}';
        return $data;
    }

    private function mapConfigForPhotoshop($data)
    {
        $data['percentage_of_sales'] = '{}';
        $data['takecare'] = '{}';
        $data['sale_seasonal'] = '{}';
        $data['bonus_money'] = 0;
        $data['combo'] = json_encode($data['combo']);
        $data['service'] = json_encode($data['service']);
        return $data;
    }


    public function validateInputData($request,$id=null){
        $data=$request->all();
        $query=ConfigSaleSalary::
        where('excu_date','=',$data['excu_date'])
            ->where('user_id','=',$data['user_id']);
        //if is update
        if($id&&$id!=0)
            $query->where('id','!=',$id);
        $recordExist=$query->first();
        return $recordExist==null;
    }


}
