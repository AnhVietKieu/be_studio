<?php

namespace App\Repositories\Api;


use App\Models\OrderJob;
use App\Models\Orders;
use App\Models\OrdersService;
use App\Models\User;
use App\Models\OrderIncurred;
use App\Models\ComboService;
use App\Models\OrderDetail;
use App\Repositories\BaseRepository;
use DateInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;
use Illuminate\Support\Carbon;

class OrderRepository extends BaseRepository
{
    public function __construct(Orders $model)
    {
        $this->model = $model;
    }

    // danh sách hóa đơn
    public function getList(Request $request)
    {
        $conditions['fields'] = [
            'id',
            'code',
            'user_id',
            'customer_id',
            'status',
            'total_price',
            'note',
            'vat',
            'sale_off',
            'created_at',
            'take_care_id'
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }

        $per_page = $request->per_page;
        if (!isset($request->status)) {
            $this->filterBuilder = $this->model
                ->with(['customer' => function ($query) {
                    $query->select('id', 'name');
                }])
                ->with(['user' => function ($query) {
                    $query->select('id', 'name');
                }])
                ->with(['takeCareUser' => function ($query) {
                    $query->select('id', 'name');
                }]);
        } else {
            $this->filterBuilder = $this->model
                ->with(['customer' => function ($query) {
                    $query->select('id', 'name');
                }])
                ->with(['user' => function ($query) {
                    $query->select('id', 'name');
                }])
                ->with(['takeCareUser' => function ($query) {
                    $query->select('id', 'name');
                }])
                ->where('status', $request->status);
        }

        return $this->paginateCustom($conditions, $per_page);
    }

    // chi tiết hóa đơn
    public function getDetail($request, $id)
    {
        $fields = [
            'id',
            'code',
            'user_id',
            'customer_id',
            'shooting_location',
            'status',
            'total_price',
            'note',
            'payment_method',
            'payment_type_id',
            'vat',
            'sale_off',
            'take_care_id'
        ];

        $order = $this->model->select($fields)->with('user')
            ->where('id', $id)->first();
        if($order['take_care_id']){
            $order['take_care_name'] = User::where('id', $order['take_care_id'])->first()->name;
        }

        $order->user_name = $order->user->name;
        $order->customer_name = $order->customer->name;
        $order = collect($order)->except(['user', 'customer']);

        $order['order_services'] = OrdersService::select(
            'id',
            'combo_id',
            'service_ids',
            'add_services',
            'total_price',
            'sale_off',
            'combo_category_id'
        )
            ->with([
                'combo' => function ($query) {
                    $query->select('id', 'name', 'time_shift', 'time_limit');
                },
                'category' => function ($query) {
                    $query->select('id', 'name');
                }
            ])
            ->where('order_id', $id)
            ->get();

        foreach ($order['order_services'] as $key => $value) {
            // lấy danh sách dịch vụ trong combo nếu có
            $order['order_services'][$key]['list_service'] = [];
            $order['order_services'][$key]['list_service'] = ComboService::select(
                'id',
                'combo_id',
                'service_id',
                'number'
            )->with(['service' => function ($query) {
                $query->select('id', 'name', 'category_id')
                    ->with(['category' => function ($cate) {
                        $cate->select('id', 'name');
                    }]);
            }])
                ->where('combo_id', $value['combo_id'])
                ->get();

            if (!empty($value['service_ids'])) {
                $value['service_ids'] = explode(',', $value['service_ids']);
            }


            foreach ($order['order_services'][$key]['list_service'] as $k => $v) {
                if (!in_array($v['service_id'], $value['service_ids'])) {
                    unset($order['order_services'][$key]['list_service'][$k]);
                }
            }

            $order['order_services'][$key]['order_detail'] = [];
            $orderDetail = OrderDetail::where('order_service_id', $value['id'])
                ->orderBy('created_at', 'desc')
                ->first();
            $order['order_services'][$key]['order_details'] = $orderDetail;
            $order['order_services'][$key]['add_services'] = json_decode($value['add_services'], true);

            //calculator time schedule
            $timeSchedule = $this->getOrderTimeSchedule($orderDetail);

            $order['order_services'][$key]['start_time'] = $timeSchedule['start_time'];
            $order['order_services'][$key]['end_time'] = $timeSchedule['end_time'];
        }

        $orderData[] = $order->toArray();
        return $orderData;
    }

    // tạo mới đơn hàng
    public function saveData($request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['status'] = 0;
        $data['payment_type_id'] = 1;
        $data['code'] = $this->getSrtCode();
        $order = $this->model;

        DB::beginTransaction();
        try {
            $order->fill($data)->save();
            foreach ($data['order_service'] as $key => $value) {
                $this->filterService($value, $order->id);
            }
            DB::commit();
            return $order;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    // lưu dữ liệu trong bảng order service khi thêm hóa đơn hoặc sửa hóa đơn
    public function filterService($orderInputs, $orderId)
    {
        $data = [];
        // kiểm tra có combo hay không
        if (!empty($orderInputs['service_ids'])) {
            $service_ids = array_map(
                function ($array) {
                    return $array['service_id'];
                }, $orderInputs['service_ids']
            );
            // lấy id của cac service
            $data['service_ids'] = implode(",", $service_ids); // chuyển sang dạng chuỗi
        }

        if (!empty($orderInputs['add_service'])) {
            $data['add_services'] = json_encode($orderInputs['add_service'], 1);
        } else {
            $data['add_services'] = null;
        }
        $data['combo_id'] = $orderInputs['combo_id'];
        $data['combo_category_id'] = $orderInputs['combo_category_id'];
        $data['order_id'] = $orderId;
        $data['total_price'] = $orderInputs['total_price'];
        $data['sale_off'] = $orderInputs['sale_off'];
        $ordersService = app(OrdersService::class);
        $ordersService->fill($data)->save();
    }

    // cập nhật hóa đơn
    public function updateData($request, $id)
    {
        $data = $request->all();
        $order = $this->model->find($id);
        DB::beginTransaction();
        try {
            //gọi đến hàm save của laravel
            $order->fill($data)->update();

            if(isset($data['order_service']) && (count($data['order_service']) > 0)) {
                OrdersService::where('order_id', $id)->delete();
                foreach ($data['order_service'] as $key => $value) {
                    $this->filterService($value, $id);
                }
            }
            DB::commit();
            return $order;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    // xóa hóa đơn
    public function deleteOrders($ids, $dem = 0)
    {
        $status = Orders::select('id', 'status')
            ->whereIn('id', $ids)->get()->toArray();

        $flag = false;

        foreach ($status as $value) {
            if ($value['status'] != 0) {
                $flag = true;
                if ($value['status'] == 1) {
                    $messages = 2;
                }
                if ($value['status'] == 2) {
                    $messages = 3;
                }
                if ($value['status'] == 3) {
                    $messages = 4;
                }
            }

        }
        if ($flag == true) {
            return $messages;
        } else {
            DB::beginTransaction();

            try {
                $delete = $this->delete($ids, $dem);

                if ($delete == true) {
                    $idsOrdersService = OrdersService::select('id')->whereIn('order_id', $ids)->get()->toArray();
                    $idsOrdersService = array_map(
                        function ($array) {
                            return $array['id'];
                        }, $idsOrdersService
                    );
                }
                OrderDetail::whereIn('order_service_id', $idsOrdersService)->delete();
                OrdersService::whereIn('order_id', $ids)->delete();
                OrderIncurred::whereIn('order_id', $ids)->delete();
                DB::commit();
                return $delete;

            } catch (\Exception $e) {
                DB::rollBack();
                Log::info($e->getMessage());
                return false;
            }
        }

        return $delete;
    }

    // lấy danh sách hóa đơn
    public function getListSelect($request)
    {
        if ($request->status == 1) {
            $data = $this->model->select('id', 'code')->where('status', '>=', 1)->get();
        } else {
            $data = $this->model->select('id', 'code')->get();
        }

        return $data;
    }

    // tạo mã hóa đơn
    protected function getSrtCode()
    {
        $year = date("Y");
        $month = date("m");
        $id = $this->model->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->count();
        $date = date("m_y");
        if (empty($id)) {
            $code = '1_' . $date;
        } else {
            $code = ($id + 1) . '_' . $date;
        }

        return Helpers::padLeft($code, 13, 'HD:000');
    }

    // tạo timeline dự kiến
    public function getTimeline($id)
    {
        $order = $this->model->select(
            'id',
            'customer_id'
        )
            ->with([
                'orderServices' => function ($query) {
                    $query->select(
                        'id',
                        'order_id',
                        'combo_id',
                        'start_time',
                        'end_time',
                        'type_timeline'

                    )->with([
                        'combo' => function ($query) {
                            $query->select(
                                'id',
                                'name',
                                'time_shift',
                                'time_limit'
                            );
                        },
                        'order_details' => function ($query) {
                            $query->select(
                                'id',
                                'name',
                                'order_service_id',
                                'date'
                            );
                        }
                    ]);
                }
            ])
            ->find($id);
        // type_timeline == 1 thì hiển thị timeline dự kiến 1
        // type_timeline == 2 thì hiển thị timeline dự kiến 2
        $data = [];
        foreach ($order->orderServices as $key => $value) {
            $timelineName = $value->combo ? $value->combo->name : ("Gói lẻ " . ($key + 1));
            switch ($value->type_timeline) {
                case 1:
                    $data[$key] = $this->rendTimeline($id, $value->combo_id, $timelineName, $value->type_timeline, $value->start_time);
                    break;
                case 2:
                    if (!empty($value->time_shift)) {
                        $data[$key] = $this->rendTimeline($id, $value->combo_id, $timelineName, $value->type_timeline, $value->start_time);
                    }
                    break;
                default:
                    if (!empty($value->order_details)) {
                        foreach ($value->order_details as $k => $v) {
                            $data = array_merge($data, [$this->rendTimeline($id, $value->combo_id, $timelineName . ': ' . $v->name, $value->type_timeline, $v->date)]);
                        }
                    }
                    break;
            }
        }
        return $data;
    }

    protected function rendTimeline($orderId, $comboId, $name, $typeTimeline, $startTime)
    {
        $settings = [];
        if ($comboId) {
            switch ($typeTimeline) {
                case 1:
                    $settings = $this->renderSettingTimeLineStyle1($orderId, $comboId, $name, $startTime);
                    break;
                case 2:
                    $settings = $this->renderSettingTimeLineStyle2($name, $startTime);
                    break;
            }
        } else {
            $settings = $this->renderSettingTimeLineRetail($name, $startTime);
        }

        $array = $settings['array'];
        $timeline = $settings['timeline'];

        foreach ($array as $key => $value) {
            $timeline['detail'] = array_merge($timeline['detail'], [$value]);
        }
        $timeline['detail'] = $this->rendMargin($timeline['detail']);

        return $timeline;
    }


    private function renderSettingTimeLineStyle1($orderId, $comboId, $name, $startTime)
    {
        $timeline = [];
        $timeline['name'] = $name;
        $timeline['detailReal'] = $this->realTimeline($orderId, $comboId);
        $timeline['detail'][] = array(
            "title" => 'chụp ảnh',
            "text" => [''],
            "time" => date('Y-m-d', strtotime('+0 day', strtotime($startTime))),
            "class" => 'mt-0',
            "color" => 'grey'
        );
        $array = array(
            '0' => [
                "title" => 'Thanh toán Lần 2',
                "text" => [
                    'Thanh toán đợt 2'
                ],
                "time" => date('Y-m-d', strtotime('+1 day', strtotime($startTime))),
                "class" => 'mt-2',
                "color" => 'grey'
            ],
            '1' => [
                "title" => 'Gửi file ảnh gốc',
                "text" => [
                    'Bokeh Lah sẽ biên tập lại các ảnh chụp được trước khi gửi Khách',
                    'Ảnh lúc này là ảnh gốc, chưa qua chỉnh sửa, file JPEG preview',
                    'Khách sẽ nhận được email gửi đường link ảnh gốc để tải về máy tính.'
                ],
                "time" => date('Y-m-d', strtotime('+2 day', strtotime($startTime))),
                "class" => 'mt-2',
                "color" => 'grey'
            ],
            '2' => [
                "title" => 'Xác nhận chọn ảnh',
                "text" => [
                    'Sau khi xem ảnh, Khách phải chọn ảnh để chỉnh sửa và cho vào album.',
                    'Khách chọn những ảnh mình ưng ý nhất, ưu tiên ảnh có thần thái tốt, ánh sáng tốt, bố cục tốt.',
                    'Tất cả các khuyết điểm trên cơ thể đều có thể xử lý được bằng Photoshop (bóp mặt v line, bóp tay chân eo, làm mịn da, kéo dài chân, v.v…)'
                ],
                "time" => date('Y-m-d', strtotime('+5 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '3' => [
                "title" => 'Xử lý ảnh',
                "text" => [
                    'Bokeh Lah sẽ Photoshop kỹ lưỡng ảnh đã chọn của Khách.',
                    'Bokeh Lah sẽ thiết kế và lên dàn trang Album cho Khách duyệt.',
                    'Khách sẽ nhận được email gửi đường link dàn trang để tải về máy tính để xem và backup.'
                ],
                "time" => date('Y-m-d', strtotime('+20 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '4' => [
                "title" => 'Thiết kế Album và in ấn',
                "text" => [
                    'Khách duyệt thiết kế Album và bìa',
                    'Khách duyệt ảnh phóng',
                    'Khách duyệt chất liệu in ấn'
                ],
                "time" => date('Y-m-d', strtotime('+21 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '5' => [
                "title" => 'Bàn giao',
                "text" => [
                    'Tất cả sản phẩm in ấn hoàn thiện sẽ được để tại Studio để Khách tới nhận.',
                    'Nếu vì lý do nào đó Khách không thể tới nhận được, Bokeh Lah sẽ hỗ trợ gọi Shipper hộ Khách.'
                ],
                "time" => date('Y-m-d', strtotime('+23 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '6' => [
                "title" => 'Thanh toán Lần 3',
                "text" => [
                    'Thanh toán đợt 3'
                ],
                "time" => date('Y-m-d', strtotime('+24 day', strtotime($startTime))),
                "class" => 'mt-2',
                "color" => 'grey'
            ],
        );
        return [
            'array' => $array,
            'timeline' => $timeline
        ];
    }

    private function renderSettingTimeLineStyle2($name, $startTime)
    {
        $timeline = [];
        $timeline['name'] = $name;
        $timeline['detail'][] = array(
            "title" => 'chụp ảnh',
            "text" => [''],
            "time" => date('Y-m-d', strtotime('+0 day', strtotime($startTime))),
            "class" => 'mt-0',
            "color" => 'grey'
        );
        $array = array(
            '0' => [
                "title" => 'Thanh toán đợt 2',
                "text" => [
                    'Thanh toán đợt 2'
                ],
                "time" => date('Y-m-d', strtotime('+0 day', strtotime($startTime))),
                "class" => 'mt-2',
                "color" => 'grey'
            ],
            '1' => [
                "title" => 'Gửi file ảnh gốc',
                "text" => [
                    'Bokeh Lah tập hợp lại toàn bộ các ảnh chụp được tại sự kiện.',
                    'Bokeh Lah tiến hành xử lý hậu kỳ ảnh (bao gồm chọn lọc ảnh, crop ảnh, cân chỉnh ánh sáng và màu sắc… theo phong cách của Studio)',
                    'Khách sẽ nhận được email gửi đường link ảnh hoàn thiện để tải về máy tính xem và backup.'
                ],
                "time" => date('Y-m-d', strtotime('+2 day', strtotime($startTime))),
                "class" => 'mt-2',
                "color" => 'grey'
            ],
            '2' => [
                "title" => 'Xác nhận chọn ảnh',
                "text" => [
                    'Sau khi xem ảnh, Khách sẽ chọn ảnh mình thích nhất để làm Photobook.',
                    'Khách chọn những ảnh mình ưng ý nhất, ưu tiên ảnh có thần thái tốt, ánh sáng tốt, bố cục tốt.',
                    'Sau khi chọn xong, Khách email cskh@bokehlah.com ảnh đã chọn. Khách có thể gửi ảnh chụp màn hình của folder đã ảnh chọn để gửi cho Bokeh Lah.'
                ],
                "time" => date('Y-m-d', strtotime('+5 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '3' => [
                "title" => 'Xử lý ảnh',
                "text" => [
                    'Bokeh Lah sẽ thiết kế và lên dàn trang Album cho Khách duyệt',
                    'Khách sẽ nhận được email gửi đường link dàn trang để tải về máy tính xem và backup.'
                ],
                "time" => date('Y-m-d', strtotime('+20 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '4' => [
                "title" => 'Thiết kế Album và in ấn',
                "text" => [
                    'Khách duyệt thiết kế Album và bìa',
                    'Khách duyệt chất liệu in ấn',
                    'Khách có thể gửi thêm yêu cầu in ấn phát sinh nếu có ++'
                ],
                "time" => date('Y-m-d', strtotime('+21 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '5' => [
                "title" => 'Bàn giao',
                "text" => [
                    'Tất cả sản phẩm in ấn hoàn thiện sẽ được để tại Studio để Khách tới nhận.',
                    'Nếu vì lý do nào đó Khách không thể tới nhận được, Bokeh Lah sẽ hỗ trợ gọi Shipper hộ Khách.'
                ],
                "time" => date('Y-m-d', strtotime('+23 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '6' => [
                "title" => 'Thanh toán đợt 3',
                "text" => [
                    'Thanh toán đợt 2'
                ],
                "time" => date('Y-m-d', strtotime('+24 day', strtotime($startTime))),
                "class" => 'mt-2',
                "color" => 'grey'
            ],
        );
        return [
            'array' => $array,
            'timeline' => $timeline
        ];
    }

    private function renderSettingTimeLineRetail($name, $startTime)
    {
        $timeline = [];
        $timeline['name'] = $name;
        $timeline['detailReal'] = [];
        $timeline['detail'][] = array(
            "title" => 'chụp ảnh',
            "text" => [''],
            "time" => date('Y-m-d', strtotime('+0 day', strtotime($startTime))),
            "class" => 'mt-0',
            "color" => 'grey'
        );
        $array = array(
            '0' => [
                "title" => 'Thanh toán đợt 2',
                "text" => [
                    'Thanh toán đợt 2'
                ],
                "time" => date('Y-m-d', strtotime('+1 day', strtotime($startTime))),
                "class" => 'mt-2',
                "color" => 'grey'
            ],
            '1' => [
                "title" => 'Gửi file ảnh gốc',
                "text" => [
                    'Bokeh Lah sẽ biên tập lại các ảnh chụp được trước khi gửi Khách',
                    'Ảnh lúc này là ảnh gốc, chưa qua chỉnh sửa, file JPEG preview',
                    'Khách sẽ nhận được email gửi đường link ảnh gốc để tải về máy tính.'
                ],
                "time" => date('Y-m-d', strtotime('+2 day', strtotime($startTime))),
                "class" => 'mt-2',
                "color" => 'grey'
            ],
            '2' => [
                "title" => 'Xác nhận chọn ảnh',
                "text" => [
                    'Sau khi xem ảnh, Khách phải chọn ảnh để chỉnh sửa và cho vào album.',
                    'Khách chọn những ảnh mình ưng ý nhất, ưu tiên ảnh có thần thái tốt, ánh sáng tốt, bố cục tốt.',
                    'Tất cả các khuyết điểm trên cơ thể đều có thể xử lý được bằng Photoshop (bóp mặt v line, bóp tay chân eo, làm mịn da, kéo dài chân, v.v…)'
                ],
                "time" => date('Y-m-d', strtotime('+5 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '3' => [
                "title" => 'Xử lý ảnh',
                "text" => [
                    'Bokeh Lah sẽ Photoshop kỹ lưỡng ảnh đã chọn của Khách.',
                    'Bokeh Lah sẽ thiết kế và lên dàn trang Album cho Khách duyệt.',
                    'Khách sẽ nhận được email gửi đường link dàn trang để tải về máy tính để xem và backup.'
                ],
                "time" => date('Y-m-d', strtotime('+20 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '4' => [
                "title" => 'Thiết kế Album và in ấn',
                "text" => [
                    'Khách duyệt thiết kế Album và bìa',
                    'Khách duyệt ảnh phóng',
                    'Khách duyệt chất liệu in ấn'
                ],
                "time" => date('Y-m-d', strtotime('+21 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '5' => [
                "title" => 'Bàn giao',
                "text" => [
                    'Tất cả sản phẩm in ấn hoàn thiện sẽ được để tại Studio để Khách tới nhận.',
                    'Nếu vì lý do nào đó Khách không thể tới nhận được, Bokeh Lah sẽ hỗ trợ gọi Shipper hộ Khách.'
                ],
                "time" => date('Y-m-d', strtotime('+23 day', strtotime($startTime))),
                "class" => 'mt-5',
                "color" => 'grey'
            ],
            '6' => [
                "title" => 'Thanh toán đợt 3',
                "text" => [
                    'Thanh toán đợt 3'
                ],
                "time" => date('Y-m-d', strtotime('+24 day', strtotime($startTime))),
                "class" => 'mt-2',
                "color" => 'grey'
            ],
        );
        return [
            'array' => $array,
            'timeline' => $timeline
        ];
    }

    protected function rendMargin($timeline)
    {
        for ($i = 0; $i < (count($timeline) - 1); $i++) {
            for ($j = $i + 1; $j < count($timeline); $j++) // lặp các phần tử phía sau
            {
                $checkTime = (strtotime($timeline[$j]['time']) - strtotime($timeline[$i]['time'])) / (60 * 60 * 24);
                $checkTime < 3 ? $timeline[$j]['class'] = 'mt-1' :
                    ($checkTime < 6 ? $timeline[$j]['class'] = 'mt-2' :
                        ($checkTime < 9 ? $timeline[$j]['class'] = 'mt-3' :
                            ($checkTime < 12 ? $timeline[$j]['class'] = 'mt-4' : $timeline[$j]['class'] = 'mt-5')));
            }
        }

        return $timeline;
    }

    // lấy chi tiết hóa đơn để update
    public function getListEdit($id)
    {
        $fields = [
            'id',
            'customer_id',
            'total_price',
            'note',
            'payment_method',
            'payment_type_id',
            'vat',
            'sale_off'
        ];

        $order = $this->model->select($fields)
            ->where('id', $id)->first();

        $order['order_service'] = OrdersService::select(
            'id',
            'combo_id',
            'service_ids',
            'add_services',
            'total_price',
            'sale_off',
            'combo_category_id'
        )->with(['combo' => function ($query) {
            $query->select(
                'id',
                'name',
                'max_sale'
            );
        }])
            ->where('order_id', $id)
            ->get();
        foreach ($order['order_service'] as $key => $value) {
            // lấy danh sách dịch vụ trong combo nếu có
            $order['order_service'][$key]['list_service'] = [];
            $order['order_service'][$key]['price_combo'] = 0;
            $order['order_service'][$key]['menu1'] = false;
            $order['order_service'][$key]['dialog'] = false;
            if (!empty($value['service_ids'])) {
                $value['service_ids'] = explode(',', $value['service_ids']);
            }else{
                $value['service_ids']=[];
            }
            $order['order_service'][$key]['list_service'] = ComboService::select(
                'id',
                'service_id',
                'number'
            )->with(['service' => function ($query) {
                $query->select(
                    'id',
                    'name',
                    'sale_price'
                );
            }])
                ->where('combo_id', $value['combo_id'])
                ->whereIn('service_id', $value['service_ids'])
                ->get()->toArray();

            $order['order_service'][$key]['list_service'] = Helpers::addColumnArray($order['order_service'][$key]['list_service'], 'service_name', 'sale_price', 'service', 'sale_price');
            $order['order_service'][$key]['add_service'] = [];

            if (!empty($value['add_services'])) {
                $order['order_service'][$key]['add_service'] = json_decode($value['add_services'], 1);
                unset($order['order_service'][$key]['add_services']);
            }

            foreach ($order['order_service'][$key]['list_service'] as $k => $v) {
                $order['order_service'][$key]['price_combo'] += $v['number'] * $v['sale_price'];
            }
            $order['order_service'][$key]['service_ids'] = $order['order_service'][$key]['list_service'];
            unset($order['order_service'][$key]['list_service']);
        }
        $order['order_service'] = Helpers::addColumnArray($order['order_service'], 'combo_name', 'max_sale', 'combo', 'max_sale');

        return $order;
    }

    public function saveDatePhoto($request)
    {
        $data = $request->all();
        DB::beginTransaction();
        try {
            if ($data['shift']) {
                $data['shift'] = json_encode($data['shift'], 1);
            } else {
                $data['time'] = json_encode($data['time'], 1);
            }

            //find or update
            $orderDetail = OrderDetail::where('order_service_id', $data['order_service_id'])->first();
            if (!$orderDetail) {
                //save order detail
                $orderDetail = new OrderDetail;
                $orderDetail->fill($data)->save();
            } else {
                $orderDetail->date = $data['date'];
                $orderDetail->shift = $data['shift'];
                $orderDetail->time = $data['time'];
                $orderDetail->save();
            }

            //update time order service
            OrdersService::where('id', $data['order_service_id'])->update([
                'start_time' => $data['date']
            ]);
            DB::commit();
            return $orderDetail;

        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return false;
        }
    }

    public function realTimeline($orderId, $comboId)
    {
        $fields = [
            'id',
            'start_date as time',
            'end_date',
            'start_date',
            'service_id',
            'start_date_plan',
            'end_date_plan'
        ];
        $orderJob = OrderJob::select($fields)
            ->with(['service' => function ($query) {
                $query->select('name', 'id');
            }])
            ->where([
                ['order_id', '=', $orderId],
                ['combo_id', '=', $comboId]
            ])->get();

        $realTimeline = [];
        foreach ($orderJob as $key => $value) {
            $realTimeline[$key] = array(
                "title" => '',
                "text" => [
                    $value->start_date ? date('H:i:s', strtotime($value->start_date)) : date('H:i:s', strtotime($value->start_date_plan)) . ' ' . $value->service->name,
                ],
                "time" => $value->start_date ? date('Y-m-d', strtotime($value->start_date)) : date('Y-m-d', strtotime($value->start_date_plan)),
                "class" => 'mt-0',
                "color" => 'grey'
            );
        }
        $realTimeline = $this->rendMargin($realTimeline);
        for ($i = 0; $i < (count($realTimeline) - 1); $i++) {
            for ($j = $i + 1; $j < count($realTimeline); $j++) // lặp các phần tử phía sau
            {
                if ($realTimeline[$i]['time'] == $realTimeline[$j]['time']) {
                    $realTimeline[$i]['text'] = array_merge($realTimeline[$i]['text'], $realTimeline[$j]['text']);
                }
            }
        }
        $realTimeline = Helpers::superUnique($realTimeline, 'time');
        return $realTimeline;
    }

    public function getOrderForDashboard()
    {
        $revenueFollowSaleInMonth = $this->model
            ->whereMonth('created_at', '=', date('m'))
            ->whereYear('created_at', '=', date('Y'))
            ->where('status', '!=', 0)
            ->select('total_price', 'user_id')
            ->get()
            ->groupBy(function ($order) {
                return User::find($order->user_id)->name;
            });

        $totalPriceArr = $this->model
            ->whereMonth('created_at', '=', date('m'))
            ->whereYear('created_at', '=', date('Y'))
            ->where('status', '!=', 0)
            ->pluck('total_price')
            ->toArray();
        $orderUnpaid = $this->model
            ->with(['customer', 'user'])
            ->where('status', 0)
            ->get();

        return [
            'revenue' => $revenueFollowSaleInMonth,
            'total_price' => array_sum($totalPriceArr),
            'unpaid' => $orderUnpaid,
        ];
    }

    public function getRevenueAllMonthFollowSale($search)
    {
        $data = $this->model->select('created_at', 'user_id', 'total_price')->whereYear('created_at', '=', date('Y'))->where('status', '!=', 0);
        if (is_numeric($search->revenue->userId)) {
            $data = $data->where('user_id', $search->revenue->userId);
        }

        return $data->get()
            ->groupBy(function ($order) {
                return $order->created_at->format('m/Y');
            });
    }

    public function getOrdersAllMonthFollowSale($search)
    {
        $data = $this->model->select('created_at', 'user_id')->whereYear('created_at', '=', date('Y'));
        if (is_numeric($search->totalOrders->status)) {
            $data = $data->where('status', $search->totalOrders->status);
        }

        if (is_numeric($search->totalOrders->userId)) {
            $data = $data->where('user_id', $search->totalOrders->userId);
        }

        return $data->get()
            ->groupBy(function ($order) {
                return $order->created_at->format('m/Y');
            });
    }

    public function getProfitAllMonth()
    {
        $data = $this->model->select('id', 'created_at', 'total_price')
            ->with('orderIncurred')
            ->where('status', '!=', 0)
            ->whereYear('created_at', '=', date('Y'));

        return $data->get()
            ->groupBy(function ($order) {
                return $order->created_at->format('m/Y');
            });
    }

    public function getListNumber()
    {

        $all = $this->model->count();
        $confirm = $this->model::where('status', 0)->count();
        $processing = $this->model::where('status', 1)->count();
        $sprint = $this->model::where('status', 2)->count();
        $finish = $this->model::where('status', 3)->count();

        $array = array(
            '0' => [
                "id" => 5,
                "name" => 'Tất cả',
                "number" => $all
            ],
            '1' => [
                "id" => 0,
                "name" => 'Chờ thanh toán',
                "number" => $confirm
            ],
            '2' => [
                "id" => 1,
                "name" => 'Đang xử lý',
                "number" => $processing
            ],
            '3' => [
                "id" => 2,
                "name" => 'Đang thực hiện',
                "number" => $sprint
            ],
            '4' => [
                "id" => 3,
                "name" => 'Kết thúc',
                "number" => $finish
            ],
        );
        return $array;
    }


    private function getOrderTimeSchedule($orderDetail)
    {
        $result = [
            'start_time' => '',
            'end_time' => ''
        ];
        if ($orderDetail && $orderDetail->date && ($orderDetail->time || $orderDetail->shift)) {
            $time = $orderDetail->time ? json_decode($orderDetail->time) : json_decode($orderDetail->shift);
            $startTime = $orderDetail->date->add(new DateInterval('PT' . (($time->timeBf->HH * 60) + $time->timeBf->mm) . 'M'));
            $endTime = $orderDetail->date->add(new DateInterval('PT' . (($time->timeAt->HH * 60) + $time->timeAt->mm) . 'M'));
            $result = [
                'start_time' => $startTime->format('Y-m-d H:i'),
                'end_time' => $endTime->format('Y-m-d H:i')
            ];
        }
        return $result;
    }

    /**
     * validate can change date schedule if not has every job is running state
     * @param $request
     */
    public function canChangeDateSchedule($request)
    {
        $orderService = OrdersService::find($request->order_service_id);
        if ($orderService) {
            $jobRunning = OrderJob::where('order_id', $orderService->order_id)
                ->where('status', '!=', 2) //reject job
                ->where('status', '!=', 5) //new job
                ->get();
            return $jobRunning->count() == 0;
        }
        return false;
    }
}
