<?php

namespace App\Repositories\Api;

use App\Models\Combo;
use App\Models\ComboService;
use App\Models\OrdersService;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helpers;

class ComboRepository extends BaseRepository
{
    /**
     * The Model name.
     *
     * @var Combo;
     */
    public function __construct(Combo $model)
    {
        $this->model = $model;
    }

    public function saveData($request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $combo = $this->model;
        if (!empty($data['time_shift'])) {
            $data['time_shift'] = json_encode($data['time_shift'], 1);
        } else {
            $data['time_shift'] = null;
        }
        DB::beginTransaction();
        try {
            //gọi đến hàm save của laravel
            $combo->fill($data)->save();
            $id = $combo->id;
            if (!empty($data['combo_service'])) {
                foreach ($data['combo_service'] as $a) {
                    $a['combo_id'] = $id;
                    (new ComboService())->fill($a)->save();
                }
            }
            DB::commit();

            return $combo;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return false;
        }
    }

    public function getList(Request $request)
    {
        $conditions['fields'] = [
            'id',
            'user_id',
            'name',
            'code',
            'category_id',
            'price',
            'time_limit',
            'description',
            'created_at',
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }

        $per_page = $request->per_page;
        $this->filterBuilder = $this->model;

        return $this->paginateCustom($conditions, $per_page);
    }

    public function getDetail($id, $check = 0)
    {
        $fields = [
            'id',
            'user_id',
            'name',
            'code',
            'category_id',
            'price',
            'time_limit',
            'description',
            'time_shift',
            'max_sale',
            'cost_price'
        ];

        $combo = $this->model->select($fields)->with('category')->where('id', $id)->first();
        $combo->category_name = $combo->category->name;
        $combo = collect($combo)->except('category');

        $combo['combo_service'] = ComboService::with(['service' => function ($query) {
            $query->select('id', 'name', 'sale_price', 'category_id')->with(['category' => function ($q) {
                $q->select('id', 'name');
            }]);
        }])
            ->select('service_id', 'combo_id', 'number')
            ->where('combo_id', $id)
            ->get();
        foreach ($combo['combo_service'] as $key => $value) {
            $combo['combo_service'][$key]->category_service_name = $value->service->category->name;
            $combo['combo_service'][$key]->sale_price = $value->service->sale_price;
            $combo['combo_service'][$key]->service_name = $value->service->name;
            $combo['combo_service'][$key] = collect($value)->except('service');
        }
        // lấy service theo category cho chi tiết combo
        if ($check == 0) {
            $combo['combo_service'] = Helpers::groupArray($combo['combo_service'], 'category_service_name');
        } else {
            if (empty($combo['time_shift'])) {
//                $getTime = json_decode($combo['time_limit'], true);
//                $combo['time_limit2'] = array();
//                foreach ($getTime as $key => $value){
//                    $getTime[$key]['timeBf']['HH'] = substr($value['time1'],0,2);
//                    $getTime[$key]['timeBf']['mm'] = substr($value['time1'],3,5);
//                    $getTime[$key]['timeAt']['HH'] = substr($value['time2'],0,2);
//                    $getTime[$key]['timeAt']['mm'] = substr($value['time2'],3,5);
//                    unset($getTime[$key]['time1']);
//                    unset($getTime[$key]['time2']);
//                }
//                $combo['time_limit2'] = $getTime;
            }
        }
        return $combo;
    }

    public function updateData($id, $request)
    {

        $data = $request->all();
        if (!empty($data['time_shift'])) {
            $data['time_shift'] = json_encode($data['time_shift'], 1);
        } else {
            $data['time_shift'] = null;
        }
        $combo = $this->model->find($id);

        DB::beginTransaction();
        try {
            //update vào bảng category bbằng câu lệnh update của laravel
            $combo->fill($data)->update();
            ComboService::where('combo_id', $id)->delete();

            foreach ($data['combo_service'] as $a) {
                $a['combo_id'] = $id;
                (new ComboService())->fill($a)->save();
            }
            DB::commit();
            return $combo;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
        }
    }

    public function deleteCombo($ids, $dem = 0)
    {
        $combodId = OrdersService::select('id')->where('deleted_at', '=', null)->whereIn('combo_id', $ids)->get()->toArray();
        if (!empty($combodId)) {
            return $messages = " Không xóa được";
        } else {

            DB::beginTransaction();
            try {
                $delete = $this->delete($ids, $dem);

                if ($delete == true) {
                    ComboService::whereIn('combo_id', $ids)->delete();
                }
                DB::commit();
                return $delete;
            } catch (\Exception $e) {
                DB::rollBack();
                Log::info($e->getMessage());
                return false;
            }
        }
    }

    public function getListSelect()
    {
        $data = $this->model
            ->select('id', 'user_id', 'name', 'code', 'category_id', 'price', 'time_limit', 'description')
            ->get();

        return $data;
    }

}
