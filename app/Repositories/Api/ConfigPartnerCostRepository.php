<?php

namespace App\Repositories\Api;

use App\Models\Category;
use App\Models\Combo;
use App\Models\ConfigPartnerCost;
use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ConfigPartnerCostRepository extends BaseRepository
{

    public function __construct(ConfigPartnerCost $model)
    {
        $this->model=  $model;
    }

    public function getList($request)
    {
        $conditions['fields'] = [
            'id',
            'partner_id',
            'combo_id',
            'money_of_combo',
            'half_day',
            'full_day',
            'morning',
            'evening',
            'afterparty',
            'out_of_province_half_day',
            'out_of_province_full_day',
            'excu_date',
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];

        if (!empty($request->filters)) {
            $conditions['filter'] = $request->filters;
        }

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }

        $per_page = $request->per_page;

        $this->filterBuilder = $this->model->with(['combo' => function($query){
            $query->select('name');
        }]);

        $data = $this->paginateCustom($conditions, $per_page);

        return $data;

    }

    public function getDetail($id)
    {
        $fields = [
            'id',
            'partner_id',
            'combo_id',
            'money_of_combo',
            'half_day',
            'full_day',
            'morning',
            'evening',
            'afterparty',
            'out_of_province_half_day',
            'out_of_province_full_day',
            'excu_date',
        ];
        $config = $this->model->select($fields)
            ->from('config_partner_cost')
            ->where('id', $id)->first();

        $user = User::find($config->partner_id);

        $combo = Combo::find($config->combo_id);


       $config['partner_id'] = [
           'id' =>$user->id,
           'name' =>$user->name
       ];

       $config['combo_id'] = [
           'id' =>$config->combo_id,
           'name' =>$combo->name
       ];

        return $config;
    }


    public function saveData($request)
    {
        $data = $request->all();

        $config_sale = $this->model;

        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel
            $config_sale->fill($data)->save();
            DB::commit();
            return $config_sale;
        } catch (\Exception $e) {
            Log::info($e->getMessage());

            dd($e->getMessage());
            return false;
        }
    }
    public function updateData($request,$id)
    {
        $data = $request->all();

        $config_cost = $this->model->find($id);
        DB::beginTransaction();
        try {
            //gọi đến hàm save của laravel
            $config_cost->fill($data)->update();

            DB::commit();
            return $config_cost;
        } catch (\Exception $e) {
            Log::info($e->getMessage());

            return false;
        }
    }

    public function deleteData($ids)
    {
        DB::beginTransaction();
        try {
            $delete = $this->delete($ids);

            DB::commit();
            return $delete;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return false;
        }
    }

    /***
     * return list config for photographer
     */


    public function getConfigFieldPhotographer()
    {
        $data = [
            'combo' => $this->getComboForPhotographer(),
            'incurred_costs' => $this->getIncurredCostsForPhotographer()
        ];
        return $data;
    }

    /***
     * return list combo contain at least one service [ảnh or album]
     */
    private function getComboForPhotographer()
    {
        $combos=[];
        $query=Combo::all();
        $combos=$query->map(function($item){
            $combos['combo_id']=$item->id;
            $combos['combo_name']=$item->name;
            return $combos;
        });
        return $combos;
    }

    /***
     * return incurred_costs of photographer
     */
    private function getIncurredCostsForPhotographer()
    {
        return [
            ['name'=>'Ngoài giờ','code'=>'extra_time'],
            ['name'=>'Đi xa 30-90KM','code'=>'km90'],
            ['name'=>'Đi xa 90-150KM','code'=>'km150'],
            ['name'=>'Đi xa trên 150KM','code'=>'kmover150'],
            ['name'=>'Qua đêm','code'=>'overnight'],
            ['name'=>'Đi xa cả ngày','code'=>'fullday'],
        ];

    }
}
