<?php

namespace App\Repositories\Api;

use App\Models\PayRoll;
use App\Models\SalaryAdvance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class PayRollRepository extends BaseRepository
{
    const payrollType = [
        'employee' => 'employee',
        'ctv' => 'ctv'
    ];
    protected $advanceSalaryModel;

    public function __construct(
        PayRoll $model,
    SalaryAdvance $advanceSalaryModel
    )
    {
        $this->model = $model;
        $this->advanceSalaryModel=$advanceSalaryModel;
    }

    public function listAll($request)
    {
        $conditions['fields'] = [
            'id',
            'user_id',
            'supplie_id',
            'department_id',
            'basic_salary',
            'work_day',
            'allowance',
            'start_date',
            'end_date',
            'note',
            'total',
            'status',
            'tax_salary',
            'config_id'
        ];

        $conditions['sorts'] = [
            'id' => 'desc',
        ];

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }
        $per_page = $request->per_page;

        $this->filterBuilder = $this->model->with([
            'user' => function ($query) {
                $query->select('id', 'name');
            },
            'supplier' => function ($query) {
                $query->select('id', 'name');
            },
            'department' => function ($query) {
                $query->select('id', 'name');
            }
            ]);

        //filter by type payroll
        switch ($request->payroll_type) {
            case self::payrollType['employee']:
                $this->filterBuilder->whereNotNull('user_id');
                break;
            case self::payrollType['ctv']:
                $this->filterBuilder->whereNotNull('supplie_id');
                break;
        }

        $data = $this->paginateCustom($conditions, $per_page);
        return $data;
    }

    public function listAdvanceSalaryInMonth($month){
        return $this->advanceSalaryModel->with('employee.department','supplier')->whereMonth('created_at', $month)->get();
    }

    public function addAdvanceSalary($data){
      $newRecord=  $this->advanceSalaryModel->fill($data)->save();
      return $newRecord;
    }

    public function updateAdvanceSalary($id,$data){
        $record=$this->advanceSalaryModel->find($id);
        if($record)
            $record->fill($data)->save();
        return $record;
    }
}
