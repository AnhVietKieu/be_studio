<?php

namespace App\Repositories\Api;
use App\Models\Orders;
use App\Models\OrderComment;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;

class OrderCommentRepository extends BaseRepository
{
    /**
     * The Model name.
     *
     * @var OrderComment;
     */
    protected $model;

    public function __construct(OrderComment $model)
    {
        $this->model = $model;
    }

    public function saveData($request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['customer_id'] = Orders::select('customer_id')
            ->where('id', $data['order_id'])
            ->first()->customer_id;

        $orderComment = $this->model;

        DB::beginTransaction();
        try {
            //gọi đến hàm save của laravel
            $orderComment->fill($data)->save();
            DB::commit();
            return $orderComment;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return false;
        }
    }


    public function getListByOrderId($request, $id)
    {
        $fields = [
            'id',
            'order_id',
            'customer_id',
            'content_detail',
            'user_id',
            'created_at',
        ];

        $orderComment = $this->model->select($fields)
        ->with(['user' => function ($query) {
            $query->select('id', 'name','avatar');
        }])->where('order_id', $id)->get();

        $today = Carbon::now();
        foreach ($orderComment as $key => $value){
            $firstDate = strtotime($value->created_at);
            $secondDate = $today->timestamp;
            $diff = abs($secondDate - $firstDate);
            $time = [];

            $time['years'] = floor($diff / (365*60*60*24));
            if($time['years']>0){
                $orderComment[$key]['time'] = $time['years'].' năm trước';
            }else{
                $time['months'] = floor(($diff - $time['years'] * 365*60*60*24) / (30*60*60*24));
                if($time['months']>0){
                    $orderComment[$key]['time'] = $time['months'].' tháng trước';
                }else{
                    $time['days'] = floor(($diff - $time['years'] * 365*60*60*24 - $time['months']*30*60*60*24) / (60*60*24));
                    if($time['days']>0){
                        $orderComment[$key]['time'] = $time['days'].' ngày trước';
                    }else{
                        $time['hours'] = floor(($diff - $time['years'] * 365*60*60*24 - $time['months']*30*60*60*24 - $time['days']*60*60*24) / (60*60));
                        if($time['hours']>0){
                            $orderComment[$key]['time'] = $time['hours'].' giờ trước';
                        }else{
                            $time['minutes'] = floor(($diff - $time['years'] * 365*60*60*24 - $time['months']*30*60*60*24 - $time['days']*60*60*24 - $time['hours']*60*60) / 60);
                            $orderComment[$key]['time'] = $time['minutes'].' phút trước';
                        }
                    }
                }
            }
        }

        return $orderComment;
    }

    public function updateData($id, $request)
    {
        $data = $request->all();
        $orderComment = $this->model->find($id);

        DB::beginTransaction();
        try {
            //update vào bảng category bbằng câu lệnh update của laravel
            $orderComment->fill($data)->update();

            DB::commit();
            return $orderComment;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
        }
    }

    public function deleteOrder($id, $dem = 0)
    {
        $ids = [$id];
        $delete = $this->delete($ids, $dem);

        return $delete;
    }


}
