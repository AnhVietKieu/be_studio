<?php


namespace App\Repositories\Api;


use App\Models\GroupConfigPartnerCost;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GroupConfigPartnerCostRepository extends BaseRepository
{
    public function __construct(
        GroupConfigPartnerCost $model
    )
    {
        $this->model=$model;
    }

    public function getList($request){
         $conditions['fields'] = [
            'id',
            'partner_id',
            'excu_date',
             'basic_salary',
             'incurred_costs'
        ];
        $conditions['sort'] = [
            'id' => 'desc',
        ];

        if (!empty($request->filters)) {
            $conditions['filter'] = $request->filters;
        }

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }

        $per_page = $request->per_page;

        $this->filterBuilder = $this->model->with('ConfigPartnerCost.Combo');

        $data = $this->paginateCustom($conditions, $per_page);
        return $data;
    }

    public function findOne($id){
        $data=$this->model->with('ConfigPartnerCost')->find($id);
        return $data;
    }

    public function saveData($request){
        $data = $request->all();
        $group = $this->model;
        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel
            $newRecord=$group->fill($data);
            $newRecord->save();
            $newRecord->ConfigPartnerCost()->createMany($data['combo']);
            DB::commit();
            return $newRecord;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    public function updateData($request,$id){
        $data = $request->all();
        $group = $this->model->find($id);
        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel

            $updateRecord=$group->fill($data);
            $updateRecord->update();
            foreach ($data['combo'] as $item){
                $updateRecord->ConfigPartnerCost()->where('id',$item['id'])->update($item);
            }
            DB::commit();
            return $updateRecord;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    public function deleteData($ids){
        DB::beginTransaction();
        try {
            $delete = $this->delete($ids);
            DB::commit();
            return $delete;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return false;
        }
    }

    public function validateInputData($request,$id=null){
        $data=$request->all();
        $query=GroupConfigPartnerCost::
            where('excu_date','=',$data['excu_date'])
            ->where('partner_id','=',$data['partner_id']);
        //if is update
        if($id&&$id!=0)
            $query->where('id','!=',$id);
        $recordExist=$query->first();
        return $recordExist==null;
    }
}
