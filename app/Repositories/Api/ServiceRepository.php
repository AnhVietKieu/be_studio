<?php

namespace App\Repositories\Api;

use App\Models\Service;
use App\Models\OrdersService;
use App\Models\ComboService;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ServiceRepository extends BaseRepository
{
    public function __construct(Service $model)
    {
        $this->model = $model;
    }

    public function saveData($request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        // $service = $this->model;
        DB::beginTransaction();
        try {
            //gọi đến hàm save của laravel
            // $service->fill($data)->save();
            $service = $this->model->create($data);
            DB::commit();
            return $service;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
            DB::rollback();
        }
    }

    public function getList(Request $request)
    {
        $conditions['fields'] = [
            'id',
            'user_id',
            'name',
            'code',
            'category_id',
            'cost_price',
            'sale_price',
            'minus_price',
            'description',
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];


        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'category_id' => $request->groupBy,
            ];
        }

        $per_page = $request->per_page;
        $this->filterBuilder = $this->model;
        return $this->paginateCustom($conditions, $per_page);
    }

    public function getDetail($request, $id)
    {
        $fields = [
            'id',
            'user_id',
            'name',
            'code',
            'category_id',
            'cost_price',
            'sale_price',
            'minus_price',
            'description',
            'department_id'
        ];

        $service = $this->model->select($fields)
            ->from('service')
            ->where('id', $id)->first();

        return $service;
    }

    public function updateData($id, $request)
    {
        $data = $request->all();
        $service = $this->model->find($id);
        DB::beginTransaction();
        try {
            //update vào bảng category bằng câu lệnh update của laravel
            $service->fill($data)->update();
            DB::commit();
            return $service;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
            DB::rollback();
        }
    }

    public function deleteComboService($ids)
    {
        $addService = OrdersService::select('add_services')
            ->where([
                ['add_services', '!=', null],
                ['deleted_at', '=', null],
            ])
            ->get()
            ->toArray();
        $checkService = false; // biến kiểm tra service đã có trong các đơn hàng chưa
        foreach ($addService as $key => $value) {
            $addService[$key]['add_services'] = json_decode($value['add_services'],1);
            foreach ($addService[$key]['add_services'] as $k => $v) {
                $checkService = in_array($v['service_id'], $ids) ?  true :  false;
            }
        }
        $servicedIds = ComboService::where('deleted_at','=', null)
            ->whereIn('service_id', $ids)
            ->count();

        if(($servicedIds != 0) || $checkService)
        {
            return $messages = " Không xóa được";
        }
        else {
                DB::beginTransaction();
                try {
                    $delete = $this->delete($ids);
                    DB::commit();
                    return $delete;
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::info($e->getMessage());
                    return false;
                }
            }
    }

    public function getListSelect()
    {
        $data = $this->model->select('id', 'user_id', 'name', 'category_id', 'sale_price', 'department_id')
            ->with(['department','category' => function ($query) {
            $query->select('id', 'name', 'type')->where('type', '=', 0);
        }])
            ->orderBy('category_id')
            ->get()
            ->toArray();

        $list = [];
        foreach ($data as $key => $value) {
            $data[$key]['number'] = 1;
            $data[$key]['service_id'] = $value['category_id'];
            if (!empty($value['category'])) {
                $list[] = $value;
            }
        }
        foreach ($list as $key => $value) {
            $list[$key]['number'] = 1;
            $list[$key]['service_id'] = $value['id'];

        }
        return $list;
    }
}
