<?php

namespace App\Repositories\Api;

use App\Models\ComboService;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ComboServiceRepository extends BaseRepository
{
    public function __construct(ComboService $model)
    {
        $this->model = $model;
    }

    public function saveData($request)
    {
        $data = $request->all();
        $comboservice = $this->model;
        DB::beginTransaction();
        try {
            //gọi đến hàm save của laravel
            $comboservice->fill($data)->save();
            DB::commit();
            return $comboservice;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
            return false;
        }
    }

    public function getList(Request $request)
    {
       $data = $this->model->all();

        return $data;
    }



    public function getDetail($request, $id)
    {
        $fields = ['id','service_id','combo_id','number'];
        $comboservice = $this->model->select($fields)
            ->from('combo_service')
            ->where('id', $id)->first();

        return $comboservice;
    }

   
    public function updateData($id, $request)
    {
        $data = $request->all();
        $comboservice = $this->model->find($id);
        DB::beginTransaction();
        try {
            //update vào bảng category bằng câu lệnh update của laravel
            $comboservice->fill($data)->update();
            DB::commit();
            return $comboservice;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
            DB::rollback();
        }
    }

    public function deleteComboService($ids, $dem = 0)
    {
        return $this->delete($ids, $dem);
    }

    public function getListSelect()
    {
        $data = $this->model->select('id','service_id','combo_id','number')->get();

        return $data;
    }

}
