<?php

namespace App\Repositories\Api;

use App\Models\OrderIncurred;
use App\Models\Orders;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;

class OrderIncurredRepository extends BaseRepository
{
    public function __construct(OrderIncurred $model)
    {
        $this->model = $model;
    }

    public function saveData($request)
    {
        $data = $request->all();
        $orderIncurred = $this->model;
        $data['create_by'] = Auth::id();
        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel
            $order = Orders::select('id', 'status')->where('id', $data['order_id'])->first();
            if ($order->status != 3) {
                $orderIncurred->fill($data)->save();
            } else {
                return [
                    'error' => true,
                    'messages' => 'thực hiện thất bại thất bại',
                ];
            }
            DB::commit();
            return $orderIncurred;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return false;
        }
    }


    public function getDetailIncurred(Request $request, $id)
    {
        $orderIncurred = OrderIncurred::select(
            'id',
            'incurred_id',
            'order_id',
            'note',
            'create_by',
            'payer',
            'date',
            'money',
            'created_at'
        )
            ->with(['createBy' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['Payer' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['Orders' => function ($query) {
                $query->select('id', 'code');
            }])
            ->with(['Incurred' => function ($query) {
                $query->select('id', 'name');
            }])
            ->where('order_id', $id)->get()->toArray();

        $orderIncurred = Helpers::addColumnArray($orderIncurred, 'create_by_name', '', 'create_by', '');
        $orderIncurred = Helpers::addColumnArray($orderIncurred, 'payer_name', '', 'payer', '');
        $orderIncurred = Helpers::addColumnArray($orderIncurred, 'incurred_name', '', 'incurred', '');

        return $orderIncurred;
    }

    public function getDetail($request, $id)
    {
        $orderIncurred = OrderIncurred::select('id', 'incurred_id', 'order_id', 'note', 'money', 'date', 'payer')
            ->find($id);

        return $orderIncurred;
    }


    public function updateData($id, $request)
    {
        $data = $request->all();
        $orderIncurred = $this->model->find($id);
        DB::beginTransaction();
        try {
            $order = Orders::select('id', 'status')->where('id', $orderIncurred['order_id'])->first();
            if ($order->status != 3) {
                $orderIncurred->fill($data)->update();
            } else {
                return [
                    'error' => true,
                    'messages' => 'thực hiện cập nhật thất bại',
                ];
            }
            DB::commit();
            return $orderIncurred;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
        }
    }

    public function deleteOrderIncurred($ids)
    {
        $orderIncurred = $this->model->find($ids[0]);
        $order = Orders::select('id', 'status')->find($orderIncurred['order_id']);
        if ($order->status != 3) {
            DB::beginTransaction();
            try {
                $orderIncurred->delete();
                DB::commit();
                return $orderIncurred;
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                DB::rollBack();
                return [
                    'error' => true,
                    'messages' => 'Xóa thất bại',
                ];
            }
        }
        return false;
    }

    public function getListSelect()
    {
        $data = $this->model->select('id', 'incurred_id', 'order_id', 'note', 'money')->get();

        return $data;
    }

}
