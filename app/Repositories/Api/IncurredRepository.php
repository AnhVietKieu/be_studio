<?php

namespace App\Repositories\Api;

use App\Models\Incurred;
use App\Models\OrderIncurred;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IncurredRepository extends BaseRepository
{
    public function __construct(Incurred $model)
    {
        $this->model = $model;
    }

    public function saveData($request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $incurred = $this->model;
        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel
            $incurred->fill($data)->save();
            DB::commit();
            return $incurred;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
            DB::rollback();
        }
    }

    public function getList(Request $request)
    {
        $conditions['fields'] = [
            'id',
            'user_id',
            'name',
            'description',
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];


        if(!empty($request->groupBy)){
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }

        $per_page = $request->per_page;
        $this->filterBuilder = $this->model;

        return $this->paginateCustom($conditions, $per_page);
    }

    public function getDetail($request, $id)
    {
        $fields = ['id', 'user_id', 'name', 'description'];
        $incurred = $this->model->select($fields)
            ->from('incurred')
            ->where('id', $id)->first();

        return $incurred;
    }


    public function updateData($id, $request)
    {
        $data = $request->all();
        $incurred = $this->model->find($id);
        DB::beginTransaction();
        try {
            //update vào bảng incurred bbằng câu lệnh update của laravel
            $incurred->fill($data)->update();
            DB::commit();
            return $incurred;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
            DB::rollback();
        }
    }

    public function deleteIncurred($ids, $dem = 0)
    {
        $incurredId = OrderIncurred::select('id')->where('deleted_at','=',null)->whereIn('incurred_id',$ids)->get()->toArray();
        $messages = '';
        if(!empty($incurredId))
        {
            return $messages = " Không xóa được";
        }
        else {

            return $this->delete($ids, $dem);
        }
    }

    public function getListSelect()
    {
        $data = $this->model->select('id','name','description')->get();

        return $data;
    }

}
