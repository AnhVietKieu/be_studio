<?php

namespace App\Repositories\Api;

use App\Models\Department;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getList($request)
    {
        $conditions['fields'] = [
            'id',
            'name',
            'email',
            'password',
            'department_id',
            'avatar',
            'basic_salary',
            'percentage_of_sales',
            'phone_number',
            'lock',
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }

        $per_page = $request->per_page;

        $this->filterBuilder = $this->model->with(['department' => function ($query) {
            $query->select('id', 'name');
        }]);

        $this->filterBuilder = $this->model->with(['roles' => function ($query) {
            $query->select('name');
        }]);

        $data = $this->paginateCustom($conditions, $per_page);

        foreach ($data as $key => $value) {
            //format data return
            $value['department_name'] = $value->department->name;
            $value->percentage_of_sales = json_decode($value->percentage_of_sales, true);
            unset($value->department);

//            //format role
            $roles = [];
            foreach ($value->roles as $valueKey => $role) {
                $roles[] = $role->pivot->roles_id;
            }
            unset($value->roles);
            $value['roles'] = $roles;
        }

        return $data;
    }
    public function listUser($request)
    {
        $conditions['fields'] = [
            'id',
            'name',
            'email',
            'password',
            'department_id',
            'avatar',
            'basic_salary',
            'percentage_of_sales',
            'phone_number',
            'lock',
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }

        $per_page = $request->per_page;

        $this->filterBuilder = $this->model->with(['department' => function($query){
            $query->select('id', 'name');
        }]);

        $this->filterBuilder = $this->model->with(['roles' => function($query){
            $query->select('name');
        }]);


       if(isset($request))
       {
           $conditions['filter'] = [
               'LIKE' =>[
                   'name' =>isset($request->name)?$request->name:'',
                   'email' =>isset($request->email)?$request->email:'',
                   'basic_salary' =>isset($request->basic_salary)?$request->basic_salary:'',
                   'phone_number' =>isset($request->phone_number)?$request->phone_number:'',

               ],
               'TYPE' =>[
                   'department_id' =>isset($request->department_id)?$request->department_id:''
                ]
           ];
       }

        $data = $this->paginateCustom($conditions, $per_page);

        return $data;

    }
    public function updateUser($request)
    {
        //validate input
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'phone_number' => 'required',
                'email' => 'required|email',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'error' => $validator->errors()
                ], 400);
        }

        $data = $request->all();
        $user_id = Auth::user()->id;
        $user = $this->model->find($user_id);

        //kiểm tra xem có upload avatar không
        if (!empty($data['image'])) {
            $image = $data['image'];
            $fileName = Auth::id() . '_' . time() . '_' . $image->getClientOriginalName();
            $path = $request->file('image')->move(public_path("/image/avatar"), $fileName);
            $photoURL = url('/image/avatar/' . $fileName);
        }
        if (!empty($photoURL)) {
            $user->avatar = $photoURL;
        } else {
            return response()->json(
                [
                    'error' => 'Không upload được ảnh'
                ], 400);
        }

        //Lưu db
        DB::beginTransaction();
        try {
            $user->fill($data)->update();
            DB::commit();
            return $user;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];

        }
    }

    public function lockUsers($id, $request)
    {
        DB::beginTransaction();
        try {
            $user = $this->model->find($id);
            if ($user->lock == 0) {
                $user->update(['lock' => 1]);
            } else {
                $user->update(['lock' => 0]);
            }
            //update vào bảng category bbằng câu lệnh update của laravel
            DB::commit();
            return $user;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];

        }
    }

    public function updateData($id, $request)
    {
        $data = $request->all();
        $user = $this->model->find($id);
        DB::beginTransaction();
        try {
            $user->fill($data)->update();

            if(isset($data['roles'])){
                $user->roles()->sync($data['roles']);
            }
            DB::commit();
            return $user;
        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::info($e->getMessage());
            DB::rollback();
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];

        }
    }

    public function listDepartment($request)
    {
        $fields = [
            'id',
            'name',
            'description',
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];

        $department = Department::select($fields)->get();
        return $department;
    }

    public function getListSelect($request)
    {
        $list_user = '';
        $fields = [
            'id',
            'name',
            'department_id',
            'email',
            'phone_number'
        ];

        // type == 0 => lấy danh sách user
        if ($request->type == 0) {
            $list_user = $this->model->select($fields)->get();
        }
        // type == 1 => lấy danh sách user theo department
        elseif ($request->type == 1) {
           $filder = [
               'EXCU' =>[
                   'id' =>isset($request->department_id)?$request->department_id:''
               ],
               'LIKE' =>[
                   'email' =>isset($request->email)?$request->email:'',
                   'phone_number' =>isset($request->phone)?$request->phone:'',
                   'name' =>isset($request->name)?$request->name:''
               ]
           ];

            $list_us = Department::select('id','name')
            ->whereNotIn('id',[4]);//ignore 4:Make up - 5:Photoshop  this field will get in supplier;

            foreach ($filder as $column =>$value)
            {
                if($column =='LIKE')
                {
                    foreach ($value as $key =>$value1)
                    {

                        if($value1!=''){
                            $list_us->where($key,'like','%'.$value1.'%');
                        }

                    }
                }
                if($column=='EXCU')
                {
                    foreach ($value as $key =>$value1)
                    {
                        if($value1!=''){
                            $list_us->where($key,$value1);
                        }

                    }
                }

            }

            $list_user = $list_us->get();
            foreach ($list_user as $key => $value){
                $list_user[$key]['user'] = $this->model->select($fields)
                    ->where('department_id',$value['id'])->get();
                }

        }
        return $list_user;
    }

    /**
     * Return User Info with full detail
     * @param $id
     * @return User
     */
    public function getUserDetail($id)
    {
        $user = User::with('roles', 'department')->find($id);
        if ($user)
            return $this->formatUserDataResponse($user);
        else return [];
    }

    private function formatUserDataResponse($user)
    {
        $result = clone $user;
        //format data return
        $result['department_name'] = is_null($user->department) ? "" : $user->department->name;
        $result->percentage_of_sales = json_decode($user->percentage_of_sales, true);
        $result->basic_salary=(int)$result->basic_salary;
        unset($result->department);

//       //format role
        $roles = [];
        foreach ($user->roles as $valueKey => $role) {
            $roles[] = $role->pivot->roles_id;
        }
        unset($result->roles);
        $result['roles'] = $roles;
        return $result;
    }
}
