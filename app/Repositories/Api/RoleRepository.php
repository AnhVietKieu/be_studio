<?php

namespace App\Repositories\Api;

use App\Models\Roles;
use App\Repositories\BaseRepository;

class RoleRepository extends BaseRepository
{
    public function __construct(Roles $model)
    {
        $this->model = $model;
    }

    public function getList($request)
    {
        $fields = [
            'id',
            'group',
            'name',
            'permissions',
        ];

        $dataRoles = $this->model->select($fields)->get()->groupBy('group');

        $arrayRoles = [];
        foreach ($dataRoles as $key => $value){
            $arrayFormat['name'] = $key;
            $arrayFormat['items'] = $value;
            $arrayFormat['selected'] =[];
            $arrayRoles[] = $arrayFormat;
        }

        return $arrayRoles;
    }

}
