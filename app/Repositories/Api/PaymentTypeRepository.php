<?php

namespace App\Repositories\Api;

use App\Models\PaymentType;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PaymentTypeRepository extends BaseRepository
{
    public function __construct(PaymentType $model)
    {
        $this->model = $model;
    }
    public function getList(Request $request)
    {
        $data = $this->model::where('parent_id', '<>' , 0)->get()->toArray();
        $list = array();
        foreach ($data as $key => $value){
            if($value['payment_expiry_type'] ==0){
                    $list[$key] = $value['name'].': '.$value['money'].' % tổng giá trị hợp đồng, hạn thanh toán: '.$value['payment_expiry'].' ngày từ khi kí hợp đồng (không được hoàn trả).';
            }else{
                    $list[$key] = $value['name'].': '.$value['money'].' % tổng giá trị hợp đồng, hạn thanh toán: '.$value['payment_expiry'].' tháng từ khi kí hợp đồng (không được hoàn trả).';
            }
        }

        return $list;
    }

}
