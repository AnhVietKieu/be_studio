<?php

namespace App\Repositories\Api;

use App\Helpers\helpers;
use App\Models\Customer;
use App\Models\Orders;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CustomerRepository extends BaseRepository
{
    public function __construct(Customer $model)
    {
        $this->model = $model;
    }

    public function getList(Request $request)
    {
        $conditions['fields'] = [
            'id',
            'user_id',
            'name',
            'code',
            'address',
            'mobile_phone',
            'email_address',
            'tax_code',
            'bank_code',
            'description',
            'field_work',
            'married_date',
            'childrens',
            'date_of_birth',
            'contact_name',
            'contact_email',
            'contact_mobile_phone',
            'contact_address',
            'level',
            'CMT',
            'contact_CMT'
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }

        if (isset($request->value_search)) {
            $conditions['filter']['LIKE'] = [
                'column' => [
                    'name',
                ],
                'value' => $request->value_search
            ];
        }

        $per_page = $request->per_page;
        if(isset($request->customer_id)){
            $this->filterBuilder = $this->model->where('id',$request->customer_id);
        }else{
            $this->filterBuilder = $this->model;
        }
        return $this->paginateCustom($conditions, $per_page);
    }

    public function getListSelect()
    {
        $data = $this->model->select()->get();

        return $data;
    }

    public function getListSearch($request = null)
    {
        $conditions['fields'] = [
            'id',
            'user_id',
            'name',
            'code',
            'address',
            'mobile_phone',
            'email_address',
            'tax_code',
            'bank_code',
            'description',
            'field_work',
            'married_date',
            'childrens',
            'date_of_birth',
            'contact_name',
            'contact_email',
            'contact_mobile_phone',
            'contact_address',
            'level',
            'CMT',
            'contact_CMT'
        ];

        $param = $request->all();
        $data = $this->model;
        foreach ($param as $key => $value) {
            if ($key == 'customer_name' && !empty($request->customer_name)) {
                $data = $data->where('name', 'like', '%' . $request->customer_name . '%');
                continue;
            }
            if ($key == 'email_address' && !empty($request->email_address)) {
                $data = $data->where('email_address', 'like', '%' . $request->email_address . '%');
                continue;
            }
            if ($key == 'mobile_phone' && !empty($request->mobile_phone)) {
                $data = $data->where('mobile_phone', 'like', '%' . $request->mobile_phone . '%');
                continue;
            }
            if ($key == 'married_date' && !empty($request->married_date)) {
                $data = $data->where('married_date', $request->married_date);
                continue;
            }
            if ($key == 'date_of_birth' && !empty($request->date_of_birth)) {
                $data = $data->where('date_of_birth', $request->date_of_birth);
                continue;
            }
            if ($key == 'level' && !empty($request->level)) {
                $data = $data->where('level', $request->level);
                continue;
            }
            if ($key == 'age') {
                $today = date('Y-m-d');
                if (!empty($value['age_start']) && empty($value['age_end'])) {
                    $year = $value['age_start'];
                    $year_start = strtotime(date("Y-m-d", strtotime($today)) . " -$year year");
                    $year_start = strftime("%Y", $year_start);
                    $data = $data->whereYear('date_of_birth', '<=', $year_start);
                } elseif (empty($value['age_start']) && !empty($value['age_end'])) {
                    $year = $value['age_end'];
                    $year_end = strtotime(date("Y-m-d", strtotime($today)) . " -$year year");
                    $year_end = strftime("%Y", $year_end);
                    $data = $data->whereYear('date_of_birth', '>=', $year_end);
                } elseif (!empty($value['age_start']) && !empty($value['age_end'])) {
                    $year_one = $value['age_start'];
                    $year_two = $value['age_end'];
                    $year_start = strtotime(date("Y-m-d", strtotime($today)) . " -$year_one year");
                    $year_start = strftime("%Y", $year_start);
                    $year_end = strtotime(date("Y-m-d", strtotime($today)) . " -$year_two year");
                    $year_end = strftime("%Y", $year_end);
                    $data = $data->whereYear('date_of_birth', '<=', $year_start)
                        ->whereYear('date_of_birth', '>=', $year_end);
                }
                continue;
            }
            if ($key == 'money') {
                $money_data = Orders::select('customer_id', 'total_price')->get()
                    ->groupBy('customer_id');
                $id = [];
                foreach ($money_data as $key => $item) {
                    $value_total_price = (collect($item)->sum('total_price'));
                    if (!empty($value['money_start'])
                        && empty($value['money_start'])
                        && $value_total_price >= $value['money_start']) {
                        $id[] = $key;
                    } elseif (!empty($value['money_end']
                        && empty($value['money_start'])
                        && $value_total_price <= $value['money_end'])) {
                        $id[] = $key;
                    } elseif (!empty($value['money_end'])
                        && !empty($value['money_start'])
                        && $value_total_price >= $value['money_start']
                        && $value_total_price <= $value['money_end']) {
                        $id[] = $key;
                    }
                }

                $data = $data->whereIn('id', $id);
                continue;
            }
        }

        $conditions['sort'] = [
            'id' => 'desc',
        ];
        $per_page = $request->per_page;
        $this->filterBuilder = $data;

        return $this->paginateCustom($conditions, $per_page);
    }

    public function getListOrder($id)
    {
        $listOrder = Orders::select('id', 'code', 'status', 'total_price', 'sale_off', 'vat')
            ->with(['orderServices' => function ($query) {
                $query->select('id', 'combo_id', 'order_id')->with(['combo' => function ($q) {
                    $q->select('id', 'name');
                }]);
            }])
            ->where('customer_id', $id)->get()->toArray();

        foreach ($listOrder as $key => $value) {
            $listOrder[$key]['list_combo'] = [];
            if (is_array($value['order_services']) || is_object($value['order_services'])) {
                foreach ($value['order_services'] as $k => $v) {
                    $listOrder[$key]['list_combo'][$k]['name'] = $v['combo']['name'];
                }
                unset($listOrder[$key]['order_services']);
            }
        }

        return $listOrder;
    }

    public function getDetail($request, $id)
    {
        return $this->model
            ->where('id', $id)->first();
    }

    public function saveData($request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['level'] = 'POTENTIAL';
        $data['code'] = $this->getCode();
        $customer = $this->model;
        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel
            $customer->fill($data)->save();
            DB::commit();
            return $customer;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    public function updateData($id, $request)
    {
        $data = $request->all();

        $customer = $this->model->find($id);
        DB::beginTransaction();
        try {
            //update vào bảng category bbằng câu lệnh update của laravel
            $customer->fill($data)->update();
            DB::commit();
            return $customer;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
        }
    }

    public function deleteCustomer($ids, $dem = 0)
    {
        $orderId = Orders::select('id')->where('deleted_at', '=', null)->whereIn('customer_id', $ids)->get()->toArray();
        if (!empty($orderId)) {
            return $messages = " Không xóa được";
        } else {

            DB::beginTransaction();
            try {
                $delete = $this->delete($ids, $dem);

                if ($delete == true) {
                    Orders::whereIn('customer_id', $ids)->delete();
                }
                DB::commit();
                return $delete;
            } catch (\Exception $e) {
                DB::rollBack();
                Log::info($e->getMessage());
                return false;
            }
        }
    }

    public function getCode()
    {
        $id = $this->model->count();
        if (empty($id)) {
            $code = '0';
        } else {
            $code = $id + 1;
        }

        return Helpers::padLeft($code, 7, 'KH:0000');
    }

    public function getDataForReport()
    {
        return $this->model
            ->whereYear('created_at', '=', date('Y'))
            ->get()
            ->groupBy(function ($customer) {
                return $customer->created_at->format('m/Y');
            });
    }
}
