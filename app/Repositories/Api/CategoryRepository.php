<?php

namespace App\Repositories\Api;

use App\Models\Category;
use App\Models\Combo;
use App\Models\Service;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CategoryRepository extends BaseRepository
{
    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function saveData($request)
    {
        $data = $request->all();
        //create default value;
        $data['enable_for_photographer_salary_config']=0;
        $data['photographer_salary_field_config']='[]';
        $data['user_id'] = 1;
        $category = $this->model;
        DB::beginTransaction();
        try {
            //gọi đến hàm update của laravel
            $category->fill($data)->save();
            DB::commit();
            return $category;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            DB::rollback();
            return false;

        }
    }

    public function getList(Request $request)
    {
        $fields = [
            'id',
            'name'
        ];

        $category = [];

        if($request->type == 1) {
            $category = $this->model->select($fields)
                ->with(['combos' => function($query){
                    $query->with('services')->select(
                        'id',
                        'name',
                        'code',
                        'category_id',
                        'time_limit',
                        'price',
                        'description');
                    }])
                ->where('type', 1)->get();
        }

        if($request->type == 0) {
            $category = $this->model->select($fields)
                ->with(['services' => function($query){
                    $query->select(
                        'id',
                        'name',
                        'code',
                        'category_id',
                        'cost_price',
                        'sale_price',
                        'minus_price',
                        'description'
                    );
                    }])
                ->where('type', 0)->get();
        }

        return $category;
    }

    public function getDetail($request, $id)
    {
        $fields = [
            'id',
            'user_id',
            'name',
            'type',
            'description',

        ];

        $category = $this->model->select($fields)
            ->from('category')
            ->where('id', $id)->first();

        return $category;
    }

    public function updateData($id, $request)
    {
        $data = $request->all();
        $category = $this->model->find($id);
        DB::beginTransaction();
        try {
            //update vào bảng category bbằng câu lệnh update của laravel
            $category->fill($data)->update();
            DB::commit();
            return $category;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
            DB::rollback();
        }
    }

    public function deleteCategory($ids, $dem = 0)
    {

        $categorycombo = Combo::select('id','name')->where('category_id',$ids)->get()->toArray();
        $categoryservice = Service::select('id','name')->where('category_id',$ids)->get()->toArray();
        if(!empty($categorycombo) || !empty($categoryservice))
        {
            return " Không xóa được";
        }
        else{
            return $this->delete($ids, $dem);
        }

    }

    public function getListSelect()
    {
        $data = $this->model->select('id','name','description')->where('type',0)->get();

        return $data;
    }

    public function getListCombo($request)
    {
        $data = $this->model->select('id','name','description')->where('type',1)->get();

        return $data;
    }

    public function getComboByCategory($categoryId)
    {
        $data = $this->model->select('name','id')->with(['combos'=> function($query){
            $query->select('name', 'id', 'category_id');
        }])->where('id',$categoryId)->first()->combos;

        return $data;
    }

}
