<?php

namespace App\Repositories\Api;

use App\Models\OrderJob;
use App\Models\Supplier;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SupplierRepository extends BaseRepository
{
    public function __construct(Supplier $model)
    {
        $this->model = $model;
    }

    public function getList(Request $request)
    {
        $conditions['fields'] = [
            'id',
            'name',
            'code',
            'mobile_phone'
        ];

        $conditions['sort'] = [
            'id' => 'desc',
        ];

        if (!empty($request->groupBy)) {
            $conditions['groupBy'] = [
                'id' => $request->groupBy,
            ];
        }

        if (isset($request->value_search)) {
            $conditions['filter']['LIKE'] = [
                'column' => [
                    'name',
                ],
                'value' => $request->value_search
            ];
        }

        $per_page = $request->per_page;
        $this->filterBuilder = $this->model;

        return $this->paginateCustom($conditions, $per_page);
    }



    public function getListSearch($request = null)
    {
        $conditions['fields'] = [
            'id',
            'name',
            'code',
            'mobile_phone'
        ];

        $param = $request->all();
        $data = $this->model;
        foreach ($param as $key => $value){
            if($key == 'supplier_name' && !empty($request->supplier_name)){
                $data = $this->model->where('name', 'like', '%'.$request->supplier_name.'%');
                continue;
            }
            if($key == 'email_address' && !empty($request->email_address)){
                $data = $data->where('email_address', 'like', '%'.$request->email_address.'%');
                continue;
            }
            if($key == 'mobile_phone' && !empty($request->mobile_phone)){
                $data = $data->where('mobile_phone', 'like', '%'.$request->mobile_phone.'%');
                continue;
            }
            if($key == 'field_work' && !empty($request->field_work)){
                $data = $data->where('field_work', 'like', '%'.$request->field_work.'%');
                continue;
            }
            if($key == 'date_create' && !empty($request->date_create)){
                $array_id = OrderJob::where('created_at', $request->date_create)->select('supplier_id')->get();
                $data = $data->whereIn('id', $array_id);
                continue;
            }
//            if($key == 'order_supplier' && !empty($request->order_supplier)){
//                $array_id = OrderJob::where('order_id', $request->order_supplier)->select('supplier_id')->get();
//                $data = $data->whereIn('id', $array_id);
//                continue;
//            }
        }

        $conditions['sort'] = [
            'id' => 'desc',
        ];
        $per_page = $request->per_page;
        $this->filterBuilder = $data;

        //non paging
        $non_paging=$request->non_paging??false;
        if($non_paging)
            return $this->filterBuilder->get();

        return $this->paginateCustom($conditions, $per_page);
    }

    public function getListSupplierByWorkField(){
        $fieldWorks= $this->model->select('field_work')->groupBy('field_work')->pluck('field_work');
        $dataReturn=[];
        foreach ($fieldWorks as $index=>$field){
            $suppliers=$this->model->where('field_work',$field)->get();
            $newClass=new \stdClass();
            $newClass->id=$index+1;
            $newClass->name=$field;
            $newClass->user=$suppliers;
            $dataReturn[]=$newClass;
        }
        return $dataReturn;
//
//        foreach ($suppliers as $supplier)
//        {
//            $dataReturn[$supplier->field_work][]=$supplier;
//        }
//        return $dataReturn;
    }

    public function getDetail($request, $id)
    {
        $fields = [
            'id',
            'user_id',
            'name',
            'code',
            'address',
            'mobile_phone',
            'email_address',
            'tax_code',
            'bank_code',
            'description',
            'field_work'
        ];
        $supplier = $this->model->select($fields)
            ->from('supplier')
            ->where('id', $id)->first();

        return $supplier;
    }

    public function saveData($request)
    {
        $data = $request->all();
        $data['user_id'] = 1;
        DB::beginTransaction();
        try {

            //gọi đến hàm update của laravel
            $supplier = $this->model->fill($data)->save();
            DB::commit();
            return $supplier;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
            DB::rollback();
        }
    }

    public function updateData($id, $request)
    {
        $data = $request->all();
        $supplier = $this->model->find($id);
        DB::beginTransaction();
        try {
            //update vào bảng category bbằng câu lệnh update của laravel
            $supplier->fill($data)->update();
            DB::commit();
            return $supplier;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return [
                'error' => true,
                'messages' => 'Cập nhật thất bại',
            ];
        DB::rollback();
        }
    }

    public function deleteSupplier($ids, $dem = 0)
    {
        $supplierId = OrderJob::select('id')->where('deleted_at','=',null)->whereIn('supplier_id',$ids)->get()->toArray();
        $messages = '';
        if(!empty($supplierId))
        {
            return $messages = " Không xóa được";
        }
        else {

            return $this->delete($ids, $dem);
        }
    }

    public function getListSelect()
    {
        $data = $this->model->select('name','code','id')->get();

        return $data;
    }

    public function getListJob($id)
    {
        $listJob = OrderJob::select('id', 'service_id', 'start_date_plan', 'end_date_plan')
        ->with(['service' => function($query) {
            $query->select('cost_price', 'id', 'name');
        }])
        ->where('supplier_id',$id)->get();

        return $listJob;
    }


}
