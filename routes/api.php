<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('register', 'AuthController@register');
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::post('reset', 'PasswordResetController@reset');
        Route::post('change/password', 'PasswordResetController@changePassword');
        Route::post('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});


Route::middleware('auth:api')->group(function () {
    Route::prefix('user')->group(function () {
        Route::get('/list/select', 'UserController@listSelect');
        Route::post('/updateUser', 'UserController@updateUser');
        Route::post('/{id}/lock', 'UserController@lockUser')->middleware('scopes:LIST_USER');

        Route::get('/{id}/view', 'UserController@getDetail')->middleware('scopes:LIST_USER');
        Route::get('','UserController@index')->middleware('scopes:LIST_USER');
        Route::post('/{id}/edit', 'UserController@editUser')->middleware('scopes:LIST_USER');
        Route::get('/department', 'UserController@listDepartment');
        Route::get('/list','UserController@listParam');
        Route::get('/{id}/view', 'UserController@getDetail')->middleware('scopes:LIST_USER');
    });

    Route::prefix('roles')->group(function () {
        Route::get('', 'RoleController@index');
    });

    Route::prefix('dashboard')->group(function () {
        Route::get('', 'DashboardController@index');
    });

    Route::prefix('report')->group(function () {
        Route::get('', 'ReportController@index');
    });

    Route::group(['prefix' => 'customer'], function () {
        Route::get('', 'CustomerController@index')->middleware('scopes:LIST_CUSTOMER');
        Route::get('/{id}', 'CustomerController@view')->middleware('scopes:LIST_CUSTOMER');
        Route::get('/{id}/order', 'CustomerController@order')->middleware('scopes:LIST_CUSTOMER');
        Route::post('/create', 'CustomerController@create')->middleware('scopes:CREATE_CUSTOMER');
        Route::post('/{id}/update', 'CustomerController@update')->middleware('scopes:EDIT_CUSTOMER');
        Route::post('/delete', 'CustomerController@delete')->middleware('scopes:DELETE_CUSTOMER');
        Route::get('/list/select', 'CustomerController@listSelect')->middleware('scopes:CREATE_CUSTOMER');
        Route::post('/list/search', 'CustomerController@listSearch')->middleware('scopes:LIST_CUSTOMER');
    });

    Route::prefix('category')->group(function () {
        Route::get('/', 'CategoryController@index')->middleware('scopes:LIST_CATEGORY');
        Route::get('/{id}', 'CategoryController@view')->middleware('scopes:LIST_CATEGORY');
        Route::post('/create', 'CategoryController@create')->middleware('scopes:CREATE_CATEGORY');
        Route::post('/{id}/update', 'CategoryController@update')->middleware('scopes:EDIT_CATEGORY');
        Route::post('/delete', 'CategoryController@delete')->middleware('scopes:DELETE_CATEGORY');
        Route::get('/list/select', 'CategoryController@listSelect')->middleware('scopes:CREATE_CATEGORY');
        Route::get('/list/combo', 'CategoryController@listCombo')->middleware('scopes:LIST_CATEGORY');
        Route::get('/{id}/listcombo', 'CategoryController@getCombo')->middleware('scopes:LIST_CUSTOMER');
    });

    Route::prefix('incurred')->group(function () {
        Route::get('', 'IncurredController@index')->middleware('scopes:LIST_INCURRED');
        Route::get('/{id}', 'IncurredController@view')->middleware('scopes:LIST_INCURRED');
        Route::post('/create', 'IncurredController@create')->middleware('scopes:CREATE_INCURRED');
        Route::post('/{id}/update', 'IncurredController@update')->middleware('scopes:EDIT_INCURRED');
        Route::post('/delete', 'IncurredController@delete')->middleware('scopes:DELETE_INCURRED');
        Route::get('/list/select', 'IncurredController@listSelect')->middleware('scopes:CREATE_INCURRED');
    });

    Route::prefix('supplier')->group(function () {
        Route::get('', 'SupplierController@index')->middleware('scopes:LIST_SUPPLIER');
        Route::get('/{id}', 'SupplierController@view')->middleware('scopes:LIST_SUPPLIER');
        Route::post('/create', 'SupplierController@create')->middleware('scopes:CREATE_SUPPLIER');
        Route::post('/{id}/update', 'SupplierController@update')->middleware('scopes:EDIT_SUPPLIER');
        Route::post('/delete', 'SupplierController@delete')->middleware('scopes:DELETE_SUPPLIER');
        Route::get('/list/select', 'SupplierController@listSelect')->middleware('scopes:CREATE_SUPPLIER');
        Route::post('/list/search', 'SupplierController@listSearch');
        Route::get('/{id}/job', 'SupplierController@listJob');
        Route::post('/list/searchResponseFormatLikeUser', 'SupplierController@listSearchResponseFormatLikeUser')->middleware('scopes:LIST_SUPPLIER');
        Route::get('/list/groupFieldWork', 'SupplierController@listSupplierByWorkField');
    });

    Route::prefix('combo')->group(function () {
        Route::get('', 'ComboController@index')->middleware('scopes:LIST_COMBO');
        Route::get('/{id}', 'ComboController@view')->middleware('scopes:LIST_COMBO');
        Route::post('/create', 'ComboController@create')->middleware('scopes:CREATE_COMBO');
        Route::post('/{id}/update', 'ComboController@update')->middleware('scopes:EDIT_COMBO');
        Route::get('/{id}/order', 'ComboController@comboOrder')->middleware('scopes:EDIT_COMBO');
        Route::post('/delete', 'ComboController@delete')->middleware('scopes:DELETE_COMBO');
        Route::get('/list/select', 'ComboController@listSelect')->middleware('scopes:CREATE_COMBO');
    });

    Route::prefix('order')->group(function () {
        Route::get('', 'OrderController@index');
        Route::get('/{id}', 'OrderController@view');
        Route::post('/add_date_photo', 'OrderController@addDatePhoto');
        Route::post('/create', 'OrderController@create');
        Route::post('/{id}/update', 'OrderController@update');
        Route::post('/delete', 'OrderController@delete');
        Route::get('/list/select', 'OrderController@listSelect');
        Route::get('/{id}/expected-timeline', 'OrderController@expected');
        Route::get('/{id}/list-edit', 'OrderController@listEdit');
        Route::get('/list/number', 'OrderController@listNumber');
    });

    Route::prefix('order_incurred')->group(function () {
        Route::get('/{id}', 'OrderIncurredController@view');
        Route::post('/create', 'OrderIncurredController@create');
        Route::post('/{id}/update', 'OrderIncurredController@update');
        Route::post('/delete', 'OrderIncurredController@delete');
        Route::get('/list/select', 'OrderIncurredController@listSelect');
        Route::get('/{id}/listorder', 'OrderIncurredController@listOrderIncurred');
    });

    Route::prefix('payment-type')->group(function () {
        Route::get('', 'PaymentTypeController@index');
    });

    Route::prefix('combo-service')->group(function () {
        Route::get('', 'ComboServiceController@index');
        Route::get('/{id}', 'ComboServiceController@view');
        Route::post('/create', 'ComboServiceController@create');
        Route::post('/{id}/update', 'ComboServiceController@update');
        Route::post('/delete', 'ComboServiceController@delete');
        Route::get('/list/select', 'ComboServiceController@listSelect');
    });

    Route::prefix('service')->group(function () {
        Route::get('', 'ServiceController@index')->middleware('scopes:LIST_SERVICE');
        Route::get('/{id}', 'ServiceController@view')->middleware('scopes:LIST_SERVICE');
        Route::post('/create', 'ServiceController@create')->middleware('scopes:CREATE_SERVICE');
        Route::post('/{id}/update', 'ServiceController@update')->middleware('scopes:EDIT_SERVICE');
        Route::post('/delete', 'ServiceController@delete')->middleware('scopes:DELETE_SERVICE');
        Route::get('/list/select', 'ServiceController@listSelect')->middleware('scopes:CREATE_COMBO');
    });

    Route::prefix('payment-history')->group(function () {
        Route::get('/{id}/history', 'PaymentHistoryController@history');
        Route::get('/{order_id}/order', 'PaymentHistoryController@listByOrder');
        Route::post('/{id}/update', 'PaymentHistoryController@update');
        Route::get('/{order_id}/payment', 'PaymentHistoryController@getPayment');
        Route::post('/create', 'PaymentHistoryController@create');
        Route::post('/delete', 'PaymentHistoryController@delete');
    });

    Route::prefix('order-comment')->group(function () {
        Route::get('', 'OrderCommentController@index');
        Route::get('/{order_id}', 'OrderCommentController@view');
        Route::post('/create', 'OrderCommentController@create');
        Route::post('/{id}/update', 'OrderCommentController@update');
        Route::post('/delete', 'OrderCommentController@delete');
    });

    Route::prefix('order-job')->group(function () {
        Route::get('/listJob', 'OrderJobController@listJobs');

        Route::post('/{order_id}/create', 'OrderJobController@create');
        Route::get('{order_job_id}/view', 'OrderJobController@view');
        Route::post('/update', 'OrderJobController@update');
        Route::get('/{order_id}/list-job', 'OrderJobController@list');
        Route::post('/assign', 'OrderJobController@assign');
        Route::post('', 'OrderJobController@index');
        Route::get('/listJobOfMonth/{month}', 'OrderJobController@listJobOfMonth');
        Route::get('listnumber', 'OrderJobController@listNumbers');
    });

    Route::prefix('email')->group(function () {
        Route::get('/lists', 'MailController@getLists');
        Route::post('/send', 'MailController@store');
    });

    Route::prefix('expense')->group(function () {
        Route::get('', 'ExpenseController@index');
        Route::post('/create', 'ExpenseController@create');
        Route::post('{id}/update', 'ExpenseController@update');
        Route::post('/delete', 'ExpenseController@delete');
        Route::get('/{expense_id}', 'ExpenseController@view');
        Route::get('/list/select', 'ExpenseController@listSelect');
    });


    Route::prefix('config_salary')->group(function () {
        Route::get('', 'ConfigSaleSalaryController@index');

        Route::post('/create', 'ConfigSaleSalaryController@create');
        Route::post('{id}/update', 'ConfigSaleSalaryController@update');
//        Route::post('/delete', 'ConfigSaleSalaryController@delete');

        Route::get('{id}/show','ConfigSaleSalaryController@show');


    });

    Route::prefix('config_cost')->group(function () {
        Route::get('','ConfigPartnerCostController@index');
        Route::post('/create', 'ConfigPartnerCostController@create');
        Route::post('{id}/update', 'ConfigPartnerCostController@update');
        Route::get('{id}/show','ConfigPartnerCostController@show');
        Route::get('config_field/photographer','ConfigPartnerCostController@getConfigFieldPhotographer');
        Route::post('/delete', 'ConfigPartnerCostController@delete');
        Route::get('/config_field/photoshop','ConfigSaleSalaryController@getConfigFieldPhotoshop');
    });

    Route::prefix('advance_salary')->group(function () {
        Route::get('/listOfMonth/{month}','PayRollController@listAdvanceSalaryInMonth');
        Route::post('/create','PayRollController@addAdvanceSalary');
        Route::post('/{id}/update','PayRollController@updateAdvanceSalary');
    });
});

Route::get('/test', 'CategoryController@index');

Route::get('config','PayRollController@updateSalary');

Route::get('listSalary','PayRollController@index');




