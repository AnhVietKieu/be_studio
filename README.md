#I Setup Project
## 1. Chuẩn bị môi trường cho cài đặt lần đầu  
 * docker 
```
    ubuntu:
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04
```
 * docker-compose
```
    https://docs.docker.com/compose/install/
```

## 2. Khởi chạy lần đầu

  Chú ý: trước khi thực hiện hãy vào file code app/Providers/AuthServiceProvider.php, comment đoạn code sau l
  ại
  
  ```
    public function boot()
    {
        $dataRoles = Roles::select('name', 'permissions')->get();
        $scopes = [];
        foreach ($dataRoles as $value) {
            $scopes[$value->name] = $value->permissions;
        }

        $this->registerPolicies();
        Passport::routes(function ($router) {
            $router->forAccessTokens();
            $router->forPersonalAccessTokens();
            $router->forTransientTokens();
        });
        Passport::personalAccessTokensExpireIn(now()->addDays(1));
        Passport::refreshTokensExpireIn(now()->addDays(30));
        Passport::pruneRevokedTokens();
        Passport::tokensCan($scopes);
    }
  ```
Sau khi chạy xong các bước cài đặt lần đầu sẽ uncomment về ban đầu. 
  
 * step 0: Tạo file .env
 ```
    cp .env.example .env
```
 * step 1: Sau khi git pull project về chạy 
```
    sudo docker-compose build        :lệnh build docker image
    sudo docker-compose up -d        :lệnh khởi tạo container
```

 * step 2: Vào docker web = command 
 
 ```
    sudo docker exec -it bkl-backend bash 
 ```

 * step 3: Chạy tiếp các lệnh cài đặt khi vào trong container
  
 ```
    composer install 
    php artisan key:generate
    php artisan migrate --seed
    php artisan passport:install
 ```

 * step 4: 
 
    Uncomment đoạn code thực hiện ở phần chú ý phía trên
    
## 3: Cập nhật dữ liệu cho database;
 File dump trong thư mục database/dump/{file_name}
 Sử dụng comand tại root folder project
 ```
    mysql -h 173.0.0.2 -u root -p studio < database/dump/{file_name}
 ```
 Với: 
    - {file_name}   :file mới nhất trong đó 
    - password:     : root

## 4. Test
Web: 
```
    http://localhost:41780
```
Api:
```
    http://localhost:41780/api/test
```
   địa chỉ truy cập sẽ thêm port 41780 (do port 80 của container dc map ra qua 41780 - xem trong file docker-compose.yml)


## 6. Các lần sau chỉ chạy 

```
   sudo docker-compose start     : lệnh start container
   sudo docker-compose stop      : lệnh stop container
```
```
   sudo docker exec -it bkl-backend bash   : lệnh vào docker backend (note. các command sẽ thao tác trong docker này) 
    
```

#II. Document
## 1. API document
 Sử dụng Swagger lib
 ```
https://github.com/DarkaOnLine/L5-Swagger
Version 
```
Link document
```
http://localhost:41780/api/documentation
```
Đối với api không authent 
```
click [Try it out]-> [Execute]
```

Đối với api có authent
```
step1: click Button [Authorize]

step2: điền 4 thông tin user,pass, client_id, client_secret

=> client_id, client_secret lấy = cách chạy command
 php artisan passport:install  (lấy Password grant client)

```
